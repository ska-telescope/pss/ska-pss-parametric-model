.. skeleton documentation master file, modified


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.

.. toctree::
  :maxdepth: 4
  :caption: Home
  :hidden:
  
  


.. README =============================================================

.. This project most likely has it's own README. We include it here.

.. toctree::
   :maxdepth: 4
   :caption: Readme

   README.md

.. COMMUNITY SECTION ==================================================

..

.. toctree::
  :maxdepth: 4
  :caption: Package name
  :hidden:



Documentation of the POM -
==================================

These are all the packages, functions and scripts that form part of the project.



.. toctree::
   :maxdepth: 4
   :caption: Contents:
   
   modules   




Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



