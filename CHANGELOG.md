# Changelog
## 1.1.1
- bug fixes in SPS pipeline

## 1.1.0

- Source code setup with poetry
- SPS pipeline implemented
