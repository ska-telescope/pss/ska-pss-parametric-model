#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
auxillary plot routine that zooms into the standard plot AFTER the simulation finished 

example call:
python src/ska_pss_parametric_model/POM_plot_time1to2.py 100 500.2 /home/bposselt/POM/POM_PSS/pom_plot/output/run_modules_mostrecent.csv  /home/bposselt/POM/POM_PSS/pom_plot/output/ /home/bposselt/POM/POM_PSS/pom_plot/output/out_resources_mostrecent.csv testZ1

ends with
--- created ../output/POM_ModulesResources_testZ1_t100.0_to_t500.2.pdf
"""
import packages.POM_visualisation as vis

import pandas as pd

import argparse

#------------------------------------------------------------------------------
# read arguments
#------------------------------------------------------------------------------
parser=argparse.ArgumentParser()
parser.add_argument("time1",type=float,help="a starting time within the simulation time ")
parser.add_argument("time2",type=float,help="an ending time within the simulation time ")
parser.add_argument("file_mrun",type=str,help="path/output/run_modules_mostrecent.csv")
parser.add_argument("outdir",type=str,help="path/output/")
parser.add_argument("file_res_out",type=str,help="path/output/out_resources_mostrecent.csv")
parser.add_argument("filename_extra",type=str,help="plotZoom1")


args = parser.parse_args()
times = args.time1
timee = args.time2
fname_mr = args.file_mrun
df_POM_run_flags = pd.read_csv(fname_mr)
outdir=args.outdir
fname_or = args.file_res_out
#dfr = pd.read_csv('fname_or')
extra =args.filename_extra



vis.plot_output_time1to2(time1 = times,
                         time2 = timee,
                         df_POM_run_flags =df_POM_run_flags,
                         filedir = outdir,
                         fileres = fname_or,
                         extraname = extra )

