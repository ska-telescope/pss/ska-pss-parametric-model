#!/usr/bin/env python3                
# -*- coding: utf-8 -*-               
# 
"""
some class definitions and copy of main class code PSS_POM
"""
import numpy as np
import pandas as pd
import simpy
import logging
import warnings
from  time import sleep

logger = logging.getLogger("pomlog")

from . import POM_modules as pm

def do_at(strval):
    yr=pm.get_decimal_year()
    print(str(strval)+str(yr))
    

###############################################################################


class testpipe1(object):
    """
    This test pipeline class calls
        - the receptor, 
        - DDBC buffer, 
        - RFIM processing (of the DDBC buffer)
        
        RAM of raw data is released after RFIM modules is done
        
        CPU RAM gets steadily lower because RAM used by RFIM mask(s) are not released
        
    """
    def __init__(self,env,g,df_POM_run_flags,name,oo_init):
        """
        gets the setup of the simulation

        Parameters
        ----------
        env : a simpy simulation environment 
              The same simpy env is used for the whole simulation (of one beam at the moment)
        g : the class with general parameters of the simulation 
            It was compiled before and is passed on.
        df_POM_run_flags : a pandas DataFrame with Flags of the POM
            It is used to run the pipeline with the config parameters (e.g., Cheetah modules on or off)
        name : Name of the pipeline 
            When multiple pipelines (beams) are run, this will help to keep track

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        self.env = env
        self.name= name
        self.simtime = g.sim_duration
        self.N_chunks = g.N_chunks
          
        self.g =g
        self.df_POM_run_flags = df_POM_run_flags
        self.obs_counter = 0
        self.chunk_counter = 0
        
#        self.oo = out_monitor(self.env)
        self.oo = oo_init
       

    def run(self):
        """
        runs the pipeline until end time of simulation is reached

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        # later to be extended to 3 beams
        #b_id=1
#        self.env.process(self.do(b_id))

        self.env.process(self.do())
        self.env.run(until=self.simtime)
        logger.info(str(self.name)+' time end: '+ str(self.env.now)) 
        return(self.oo.out_data,self.oo.out_resources, self.oo.inter_data)
      
 
    def do(self):
        """
        The pipeline with individual module calls.

        Returns
        -------
        None.

        """
#    def do(self,b_id):
#        print(self.oo.out_data)
#        print(self.oo.out_resources)
        
        while True:
            tpip1_start=self.env.now
            # --- Create/get new observation
            # --- Increment the obs counter
            
            
            
            self.obs_counter += 1
            time_obsID_started = self.env.now
#            logger.info('- obs counter: '+str(self.obs_counter)+
#                        ', started at time '+str(time_obsID_started))
            
            obs = pm.Observation(self.obs_counter)
            # need to set 0 for each new observation
            self.chunk_counter = 0
            
            
            # ------ keeps generating data_chunks for one observation,  
            #        counting, until number-of-chunks is reached
            while self.chunk_counter < (self.N_chunks):

                  self.chunk_counter += 1
                  errflag=0
                  
                  chunk = pm.Chunk(obs.obs_id,self.chunk_counter)
#                  print(chunk.obs_id,chunk.chunk_id)
                  self.oo.init_od_newrow(obs.obs_id,self.chunk_counter)
                  #print(self.oo.out_data)
                                       
                  time_read_start=self.env.now 

                  readin_iters=(self.g.t_RCPT_i + self.g.chunk_buffer_length)

                  rusemem=self.g.D_RCPT+self.g.M_RCPT
                  rreturnmem=self.g.M_RCPT



                  rec1=pm.receptor_mod(self.env,
                                       '* RCPT',
                                       readin_iters,
                                       'CPU',
                                       self.g.cpu_RCPT,
                                       rusemem,
                                       self.g.cpu_RCPT,
                                       rreturnmem,
                                  #     b_id,
                                       chunk.obs_id,
                                       chunk.chunk_id,
                                       self.oo,
                                       'time_rcpt')

                  # run data receptor 
                  rec1_proc =self.env.process(rec1.run())
                  yield rec1_proc    

##############################                      

                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 1):


                      ddbc2 = pm.Buffer_Module_Useevent_Idata(env =self.env,
                                   name = 'DDBC',
                                   runtime = self.g.t_DDBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_DDBC,
                                   use_resource_mem = self.g.M_DDBC,
                                   return_resource_num = self.g.cpu_DDBC,
                                   return_resource_mem = self.g.M_DDBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rcpt',
                                   timecol_inoo  = 'time_ddbc',
                                   with_intermediate_data = 'M',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_DDBC',
                                   mod_size_interdata_perchunk = self.g.D_DDBC,
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   D_already_reserved= self.g.D_RCPT,
                                   sub_chunks = 1)

                      ddbcproc = self.env.process(ddbc2.reserve_buffer(rec1_proc,False))
                     

                      
                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 0):
                      logger.info('- NO DDBC done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))
##############################                      

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 1):

                      
                      
                      rfim1 = pm.Proc_Module_Useevent_Idata(env =self.env,
                                   name = 'RFIM',
                                   runtime = self.g.t_RFIM,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_RFIM,
                                   use_resource_mem = self.g.M_RFIM,
                                   return_resource_num = self.g.cpu_RFIM,
                                   return_resource_mem = self.g.M_RFIM,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ddbc',
                                   timecol_inoo  = 'time_rfim',
                                   with_intermediate_data = 'RM',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_rfim,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_DDBC',
                                   req_last_used_by ='DDBC_done',
                                   req_change_used_by = 'RFIM_req',
                                   mod_ID1_inoointer = 'D_RFIM',
                                   mod_size_interdata_perchunk = self.g.D_RFIM,
                                   mod_name_extra = '_msk',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False)



                      rfim1_proc = self.env.process(rfim1.run(ddbcproc, True))


##############################                       
# This releases the RAM resource occupied by the DBBC 
# 
                      ddbcrelease = self.env.process(ddbc2.release_buffer_resource(rfim1_proc, 'RFIM_done',False))   
##############################                      

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 0):
                      logger.info('- NO RFIM done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))

        tpip1_end=self.env.now

###############################################################################


class testpipe3(object):
    """
     
    The pipeline class where the receptor, buffer, processing, and output processes are called

         
    """
    def __init__(self,env,g,df_POM_run_flags,name,oo_init):
        """
        gets the setup of the simulation

        Parameters
        ----------
        env : a simpy simulation environment 
              The same simpy env is used for the whole simulation (of one beam at the moment)
        g : the class with general parameters of the simulation 
            It was compiled before and is passed on.
        df_POM_run_flags : a pandas DataFrame with Flags of the POM
            It is used to run the pipeline with the config parameters (e.g., Cheetah modules on or off)
        name : Name of the pipeline 
            When multiple pipelines (beams) are run, this will help to keep track

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        self.env = env
        self.name= name
        self.simtime = g.sim_duration
        self.N_chunks = g.N_chunks
          
        self.g =g
        self.df_POM_run_flags = df_POM_run_flags
        self.obs_counter = 0
        self.chunk_counter = 0
        
#        self.oo = out_monitor(self.env)
        self.oo = oo_init
       

    def run(self):
        """
        runs the pipeline until end time of simulation is reached

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        # later to be extended to 3 beams
        #b_id=1
#        self.env.process(self.do(b_id))

        self.env.process(self.do())
        self.env.run(until=self.simtime)
        logger.info(str(self.name)+' time end: '+ str(self.env.now)) 
        return(self.oo.out_data,self.oo.out_resources, self.oo.inter_data)
      
 
    def do(self):
        """
        The pipeline with individual module calls.

        Returns
        -------
        None.

        """
#    def do(self,b_id):
#        print(self.oo.out_data)
#        print(self.oo.out_resources)
        
        while True:
            tpip3_start=self.env.now
           # --- Create/get new observation
            # --- Increment the obs counter
            
            
            
            self.obs_counter += 1
            time_obsID_started = self.env.now
#            logger.info('- obs counter: '+str(self.obs_counter)+
#                        ', started at time '+str(time_obsID_started))
            
            obs = pm.Observation(self.obs_counter)
            # need to set 0 for each new observation
            self.chunk_counter = 0
            
            
            # ------ keeps generating data_chunks for one observation,  
            #        counting, until number-of-chunks is reached
            while self.chunk_counter < (self.N_chunks):

                  self.chunk_counter += 1
                  errflag=0
                  
                  chunk = pm.Chunk(obs.obs_id,self.chunk_counter)
#                  print(chunk.obs_id,chunk.chunk_id)
                  self.oo.init_od_newrow(obs.obs_id,self.chunk_counter)
                  #print(self.oo.out_data)
                                       
                  time_read_start=self.env.now 

                  readin_iters=(self.g.t_RCPT_i + self.g.chunk_buffer_length)

                  rusemem=self.g.D_RCPT+self.g.M_RCPT
                  rreturnmem=self.g.M_RCPT



                  rec1= pm.receptor_mod(self.env,
                                       '* RCPT',
                                       readin_iters,
                                       'CPU',
                                       self.g.cpu_RCPT,
                                       rusemem,
                                       self.g.cpu_RCPT,
                                       rreturnmem,
                                  #     b_id,
                                       chunk.obs_id,
                                       chunk.chunk_id,
                                       self.oo,
                                       'time_rcpt')

                  # run data receptor 
                  rec1_proc =self.env.process(rec1.run())
                  yield rec1_proc    

##############################                      


                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 1):


                      ddbc = pm.Buffer_Module_Useevent_Idata(env =self.env,
                                   name = 'DDBC',
                                   runtime = self.g.t_DDBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_DDBC,
                                   use_resource_mem = self.g.M_DDBC,
                                   return_resource_num = self.g.cpu_DDBC,
                                   return_resource_mem = self.g.M_DDBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rcpt',
                                   timecol_inoo  = 'time_ddbc',
                                   with_intermediate_data = 'M',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_DDBC',
                                   mod_size_interdata_perchunk = self.g.D_DDBC,
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   D_already_reserved= self.g.D_RCPT,
                                   sub_chunks = 1)

                      ddbcproc = self.env.process(ddbc.reserve_buffer(rec1_proc,False))
                     

                      
                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 0):
                      logger.info('- NO DDBC done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))
##############################                      

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 1):


                      rfim = pm.Proc_Module_Useevent_Idata(env =self.env,
                                   name = 'RFIM',
                                   runtime = self.g.t_RFIM,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_RFIM,
                                   use_resource_mem = self.g.M_RFIM,
                                   return_resource_num = self.g.cpu_RFIM,
                                   return_resource_mem = self.g.M_RFIM,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ddbc',
                                   timecol_inoo  = 'time_rfim',
                                   with_intermediate_data = 'RM',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_rfim,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_DDBC',
                                   req_last_used_by ='DDBC_done',
                                   req_change_used_by = 'RFIM_req',
                                   mod_ID1_inoointer = 'D_RFIM',
                                   mod_size_interdata_perchunk = self.g.D_RFIM,
                                   mod_name_extra = '_msk',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False)



                      rfim_proc = self.env.process(rfim.run(ddbcproc, True))

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 0):
                      logger.info('- NO RFIM done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))




########################################
#12345678912345678
                  if (self.df_POM_run_flags.run_ffbc.loc[0] == 1):
                      
                      ffbc = pm.Buffer_Module_Useevent_Idata(env =self.env,
                                   name = 'FFBC',
                                   runtime = self.g.t_FFBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_FFBC,
                                   use_resource_mem = self.g.M_FFBC,
                                   return_resource_num = self.g.cpu_FFBC,
                                   return_resource_mem = self.g.M_FFBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rfim',
                                   timecol_inoo  = 'time_ffbc',
                                   with_intermediate_data = 'M',
                                   num_chunks_for_intermediate_data = self.g.N_chunks,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_FFBC',
                                   mod_size_interdata_perchunk = self.g.D_FFBC,
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   D_already_reserved= self.g.D_RCPT, # reminder:  per data chunk
                                   sub_chunks = self.g.chunks_in_ddbc
                                   )

                      ffbc_proc = self.env.process(ffbc.reserve_buffer(rfim_proc,False))

##############################                       
#  DDBC no longer needed in THIS pipeline (but data is in the FFBC )
#                     the first argument is the event after which this is run 
#                     the second is the earliest entry in "last_used_by" for the last module that modified the row of B_DDBC (FFBC does not!)
#                     True - only inter_data is changed 
                      ddbcrelease = self.env.process(ddbc.release_buffer_resource(ffbc_proc, 'RFIM_req',True))   
##############################                      

                      
                  if (self.df_POM_run_flags.run_ffbc.loc[0] == 0):
                       logger.info('- NO FFBC done for obsID/chunk'+ str(chunk.obs_id)+
                                   '/'+str(self.chunk_counter))

            
            # # --- CDOS crucial for releasing resources, keep user informed if unreasonable flags
                  if ((self.df_POM_run_flags.run_cdos.loc[0] == 1) 
                      and (self.df_POM_run_flags.run_ffbc.loc[0] == 0)):
                       warnings.warn('- CDOS cannot work without FFBC \n'+
                                         '- run_CDOS=0, no CDOS for obsID ' 
                                         + str(self.obs_counter) + '\n'+
                                       '!!! Buffer used by Obs *not* out of scope, thus RAM stays occupied')
                
                  if (self.df_POM_run_flags.run_cdos.loc[0] == 0):
                           warnings.warn('- run_CDOS=0, no CDOS for obsID ' 
                                         +str(self.obs_counter) +'\n'+
                                       '!!! Buffer used by Obs *not* out of scope, thus RAM stays occupied')
            

                  if ((self.df_POM_run_flags.run_cdos.loc[0] == 1) 
                        and (self.df_POM_run_flags.run_ffbc.loc[0] == 1)):
                
#123456789123456789123                      
                      cdosA = pm.Proc_Module_Useevent_Idata(env =self.env,
                                   name = 'CDOSa',
                                   runtime = self.g.t_CDOS_all,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_CDOS,
                                   use_resource_mem = self.g.M_CDOS,
                                   return_resource_num = self.g.cpu_CDOS,
                                   return_resource_mem = self.g.M_CDOS,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ffbc',
                                   timecol_inoo  = 'time_cdosA',
                                   with_intermediate_data = 'RM',
                                   num_chunks_for_intermediate_data = self.g.N_chunks,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_FFBC',
                                   req_last_used_by ='FFBC_done',
                                   req_change_used_by = 'CDOS_req',
                                   mod_ID1_inoointer = 'D_CDOSa',
                                   mod_size_interdata_perchunk = self.g.D_CDOSa,
                                   mod_name_extra = '_lists',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False)


                      cdosA_proc = self.env.process(cdosA.run(ffbc_proc, True))
                      
#123456789123456789123

                   #-------- final resource release after the processing of one observation is finished
                      # release RAM memory of FFBC buffer False: not only inter_data, but also resource 
                      B_FFBC_release = self.env.process(ffbc.release_buffer_resource(cdosA_proc, 'CDOS_done',False))
                      # release RAM memory of data products (RFIM msk, CDOS outut lists,...) 
                      #  '' entry must be listed in check_done_withorder method
                      D_RFIM_msk_release = self.env.process(rfim.release_intermediate_data_products(cdosA_proc, 'RFIM_mskdone',True))
                      D_CDOSa_lists_release = self.env.process(cdosA.release_intermediate_data_products(cdosA_proc, 'CDOSa_listsdone',True))

        tpip3_end=self.env.now




###############################################################################
# routine to compare output data frames
###############################################################################

def compare_dataframes(cfile_outdata,cfile_out_resources,cfile_interdata,outdata_filename,outres_filename,interdata_filename):
    df1_comp_outd = pd.read_csv(cfile_outdata)
    df1_comp_outres = pd.read_csv(cfile_out_resources)
    df1_comp_interd = pd.read_csv(cfile_interdata)    
    
    outdf1 = pd.read_csv(outdata_filename)
    outresdf1= pd.read_csv(outres_filename)
    interdf1= pd.read_csv(interdata_filename)


    # make sure we compare only the same columns (in case we change the data frames in future develpment)
    # may need updating for later tests
    df_comp_outd = df1_comp_outd[['obs','chunk', 
                                  'time_rcpt', 'time_ddbc',
                                  'time_rfim', 'time_ffbc',  
                                  'time_cdosA']]
    
    
    outdf = outdf1[['obs','chunk', 
                                 'time_rcpt', 'time_ddbc',
                                 'time_rfim', 'time_ffbc',  
                                 'time_cdosA']]
    
    data_comp = outdf.compare(df_comp_outd,keep_shape=False,result_names=("this_run", "old"))
    
    if (len(data_comp) == 0):
        print('* 1 * The test reproduces the archived out_data DataFrame.')
    if (len(data_comp) != 0):
        print('* 1 * The test DOES NOT reproduce the archived out_data DataFrame. The differences are:')
        print(data_comp)
 
    df_comp_outres = df1_comp_outres[['time_now',
                                      'obs',     
                                      'chunk',   
                                      'module_caller',
                                      'cpu_ram_free',
                                      'cpu_cores_free', 
                                      'gpus',
                                      'mem_in_gpu']]
 
    outresdf = outresdf1[['time_now',
                                      'obs',     
                                      'chunk',   
                                      'module_caller',
                                      'cpu_ram_free',
                                      'cpu_cores_free', 
                                      'gpus',
                                      'mem_in_gpu']]

    res_comp = outresdf.compare(df_comp_outres,keep_shape=False,result_names=("this_run", "old"))

    if (len(res_comp) == 0):
        print('* 2 * The test reproduces the archived out_resources DataFrame.')
    if (len(res_comp) != 0):
        print('* 2 *  The test DOES NOT reproduce the archived out_resources DataFrame. The differences are:')
        print(res_comp)

    df_comp_interd = df1_comp_interd[['obs',                                      
                        'data_name_p1',
                        'ID_name_p2',
                        'time_created',
                        'time_last_access',
                        'time_deleted',  
                        'size_in_ram',
                        'last_used_by' ]]
    
    
    interdf = interdf1[['obs',                                      
                        'data_name_p1',
                        'ID_name_p2',
                        'time_created',
                        'time_last_access',
                        'time_deleted',  
                        'size_in_ram',
                        'last_used_by']]

    
    inter_comp = interdf.compare(df_comp_interd,keep_shape=False,result_names=("this_run", "old"))

    if (len(inter_comp) == 0):
        print('* 3 * The test reproduces the archived inter_data DataFrame.')
    if (len(inter_comp) != 0):
        print('* 3 *  The test DOES NOT reproduce the archived inter_data DataFrame. The differences are:')
        print(inter_comp)


    
    return(data_comp,res_comp,inter_comp)









