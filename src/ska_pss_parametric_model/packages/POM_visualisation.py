#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
visualise what the POM is doing

This is based on: 
    documentation of the rich package
    https://github.com/Textualize/rich/blob/master/examples/top_lite_simulator.py
    Lite simulation of the top linux command.

"""
import numpy as np
import pandas as pd
import re
import matplotlib.pyplot as plt

import simpy
import logging
import warnings
from time import sleep

logger = logging.getLogger("pomlog")
logging.getLogger('matplotlib').setLevel(logging.WARNING)

#import datetime
#import random
import sys
from dataclasses import dataclass

from rich import box
from rich.live import Live
from rich.table import Table,Column
from rich.layout import Layout
#from rich.panel import Panel

if sys.version_info >= (3, 8):
    from typing import Literal
else:
    from typing_extensions import Literal


@dataclass
class Row:
    """
    Defines the row entries of the top-like table
    """
    rid: int
    module_name: str
    processes_running: int
    need_cputhreads : int
    need_gpu : int
    need_M : int
    need_D : int

    @property
    def memory_str(self) -> str:
        if self.memory > 1e6:
            return f"{int(self.memory/1e6)}M"
        if self.memory > 1e3:
            return f"{int(self.memory/1e3)}K"
        return str(self.memory)



#console = Console()

class top_vis(object):
    def __init__(self,env,console,g,oo):
        """
        Initialise
        
        Parameters
        ----------
        env : a simpy environment
        console : rich parameter to know where output is directed to
        g : POM parameters
        oo : output data frames that are queried

        Returns
        -------
        run_top_vis actually runs the process
        """
        self.env = env
        self.console = console
        self.g = g
        self.oo = oo   
    
    def column_lookup(self,module:str):
        """
        checks model parameters of modules

        Parameters
        ----------
        module : str
            Name of the module for which to get the data

        Returns
        -------
        rid : row ID for output table
        col : time column name in oo.output_data
        res_col_useD : used data memory per running module
        res_col_useM : used processing memory  per running module
        res_col_usecpu : used number of CPUs/threads  per running module
        res_col_usegpu : used number of GPUs  per running module

        """
        col = 'time_rcpt' # "a value" in case of wrong input
        res_col_useD = self.g.D_RCPT
        res_col_useM = self.g.M_RCPT
        res_col_usecpu = self.g.cpu_RCPT
        res_col_usegpu = 0
        
        if( (module == 'rcpt') or (module == 'RCPT')):
            rid = 1
            col = 'time_rcpt'
            res_col_useD = self.g.D_RCPT
            res_col_useM = self.g.M_RCPT
            res_col_usecpu = self.g.cpu_RCPT
            res_col_usegpu = 0
            
        if ((module == 'ddbc') or (module == 'DDBC')):
            rid = 2
            col = 'time_ddbc'
            res_col_useD = 0
            res_col_useM = self.g.M_DDBC
            res_col_usecpu = self.g.cpu_DDBC
            res_col_usegpu = 0
            
        if ((module == 'rfim') or (module == 'RFIM')):
            rid = 3
            col = 'time_rfim'
            res_col_useD = self.g.D_RFIM
            res_col_useM = self.g.M_RFIM
            res_col_usecpu = self.g.cpu_RFIM
            res_col_usegpu = self.g.gpu_RFIM
            
        if ((module == 'ffbc') or (module == 'FFBC')):
            rid = 4
            col = 'time_ffbc'
            res_col_useD = 0
            res_col_useM = self.g.M_FFBC
            res_col_usecpu = self.g.cpu_FFBC
            res_col_usegpu = 0
            
        if ((module == 'cdosA') or (module == 'cdosa') or (module == 'CDOSA')):
            rid = 5
            col = 'time_cdosA'
            res_col_useD = 0
            res_col_useM = self.g.M_CDOS
            res_col_usecpu = self.g.cpu_CDOS
            res_col_usegpu = 0

        return(rid,col,res_col_useD,res_col_useM,res_col_usecpu,res_col_usegpu)    

          
    
    def get_row_numbers(self, module:str):
        """
        gets the number of how many proceses with the same name are started/running

        Parameters
        ----------
        module : str
                Name of the Cheetah module

        Returns
        -------
        rid : row ID for this module name
        howmany : how many parallel processes with the respective same module name  
        howmany*res_col_useD :  total data memory used by same-module processes
        howmany*res_col_useM :  total processing memory used by same-module processes
        howmany*res_col_usecpu : total number of CPUs/threads used by same-module processes
        howmany*res_col_usegpu : total number of GPUs used by same-module processes

        """
        rid,column,res_col_useD,res_col_useM,res_col_usecpu,res_col_usegpu = self.column_lookup(module)
        aus_oo = self.oo.out_data[(self.oo.out_data[column] == -1)]
        howmanyh = len(aus_oo)
        if (howmanyh >0):
            howmany = howmanyh
        else:
            howmany = 0
        return(rid,
               howmany, 
               howmany*res_col_useD,
               howmany*res_col_useM,
               howmany*res_col_usecpu,
               howmany*res_col_usegpu)

    def generate_row(self,name,rid,numpr,needcputhreads,needgpu,needM,needD) -> Row:
        """
        generate  a row entry for table

        Parameters
        ----------
        name : module name .
        rid : row ID
        numpr : how many parallel processes with the respective same module name
        needcputhreads : total number of CPUs/threads used by same-module processes
        needgpu : total number of GPUs used by same-module processes
        needM : total processing memory used by same-module processes        
        needD : total data memory used by same-module processes

        Returns
        -------
        Row instance with the same parameters

        """
        return Row(rid = rid,
                   module_name = name,
                   processes_running =numpr,
                   need_cputhreads = needcputhreads,
                   need_gpu = needgpu,
                   need_M = needM,
                   need_D = needD
                   )
    
    
    def create_table(self) -> Table:
        """
        create Table, sorted by row ID
        queries resource usage at current time for footer 

        Returns
        -------
        Table instance

        """
        module_list = ['RCPT','DDBC','RFIM','FFBC','CDOSA']
        for i,module in enumerate(module_list):
            rid,numproc,needD,needM,needcpu,needgpu = self.get_row_numbers(module)
            row_i = self.generate_row(module,rid,numproc,
                                      needcpu,needgpu, 
                                      needM,needD)
            if (i==0):
                rows=[row_i]
            if (i>0):
                rows.append(row_i)
        
        rows_sorted = sorted(rows,key=lambda r: r.rid,reverse=False)
        
        table = Table(Column(header="Module \n",
                             footer='\n'+'available',
                             justify='left'),
                      Column(header="#running \n",
                             footer='\n'+'now:',
                              justify='center'),
                      Column(header="#threads req \n ",
                             footer='\n'+str(self.env.cpu_cores.level),
                             justify='center'),
                      Column(header="ProcMemory req \n (bytes)",
                             footer='\n'+'both:',justify='center'),
                      Column(header="DataMemory req \n (bytes)",
                             footer='\n'+str(self.env.cpu_ram.level),
                             justify='center'),
                      Column(header="#gpu req \n",
                             footer='\n'+str(self.env.gpus.level),
                             justify='center'),
                      Column(header="gpu Memory req \n (bytes)",
                             footer='\n'+str(self.env.mempergpu.level),
                             justify='center'),
                      Column(header='Sim_time \n (sim unit)', 
                             footer=' ',
                             justify='right'),
                      box=box.MINIMAL,show_footer=True                      
                      )
        
        for row in rows_sorted:
            table.add_row(
                row.module_name,
                str(row.processes_running),
                str(row.need_cputhreads),
                str(row.need_M),
                str(row.need_D),
                str(row.need_gpu),
                str(0),
                str(self.env.now)
                )
        table.add_section()
        table.add_row(
            "used now",                
            "(in total):",                
            str(self.g.available_cpu_cores - self.env.cpu_cores.level),                
            "both:",                
            str(self.g.available_cpu_ram - self.env.cpu_ram.level),                
            str(self.g.available_gpus - self.env.gpus.level) ,                
            str(self.g.available_mem_pergpu - self.env.mempergpu.level),                
            str(self.env.now),style='red1'
                )
            
            
        return table
            
        
    def run_top_vis(self,sleeptime):
        """
        Is producing top-like output, slowed down if sleeptime not 0
   
        """
        layout = Layout()
        layout.split_column(Layout(name="upper"),
                            Layout(name="lower")
                            )
        layout["upper"].size = 20
        # lower panel planned to be used for graph output for example
        
        with Live(console=self.console, screen=True, auto_refresh=False) as live:
            while True:
                sleep(sleeptime)    # this stops everything (also progress of simpy time)

                layout["upper"].update(live.update(self.create_table(), refresh=True))
                yield self.env.timeout(1)  # this is simpy environemnt time !
                
                
###############################################################################
#       pdf plot routine(s)
###############################################################################

# helper routine, get filtered data frame for plotting
def get_seldf_module(dfr,modname,endsimtime):
    """
    Assembles start and end time of a module listing  from 
    the resource output dataframe together in a new dataframe

    Parameters
    ----------
    dfr : pandas dataframe
          This is the resource output dataframe from the simulation
    modname : which module as string
              e.g., 'RCPT', 'DDBC',...
    endsimtime : the total simulation time
                 from the POM confi parameters, 
                 it is needed to find unfinished modules 

    Returns
    -------
    a filtered pandas data frame that has in the left part all the columns
    associated with the 'start' of the module, the second part has all the 
    (renamed) columns associated with the 'end' of the module

    """
    mname=str(modname)
    dfh1= dfr[(dfr.module_caller.str.contains(mname, regex=False) & 
               dfr.module_caller.str.contains('start', regex=False,flags=re.IGNORECASE))]
    
    dfh2= dfr[(dfr.module_caller.str.contains(mname, regex=False) & 
               dfr.module_caller.str.contains('end', regex=False,flags=re.IGNORECASE))]
    
    dfh3=dfh2.rename(columns={'time_now'        : 'time_end', 
                         'module_caller'   : 'module_end', 
                         'cpu_ram_free'    : 'cpu_ram_free_end',
                         'cpu_ram_used'    : 'cpu_ram_used_end',
                         'DIF_cpu_ram_used': 'DIF_cpu_ram_used_end',
                         'cpu_cores_free'  : 'cpu_cores_free_end' ,
                         'cpu_cores_used'  : 'cpu_cores_used_end' ,
                         'gpus'            : 'gpus_end', 
                         'mem_in_gpu'      : 'mem_in_gpu_end'})

    
    aus_dfr=pd.merge(dfh1,dfh3, on=['obs','chunk'],how='left')
    
    mask= aus_dfr.time_end.isnull()
    aus_dfr['notfinished'] = 0
    aus_dfr.loc[mask,'time_end'] = endsimtime
    aus_dfr.loc[mask,'notfinished'] = 1


    aus_dfr.reset_index(inplace=True,drop=True)
    return(aus_dfr)


def get_run_modulenames(dfr):
    """
    produces list of the run module names

    Parameters
    ----------
    dfr : pandas dataframe
         output of simulation with the run modules

    Returns
    -------
    list of strings

    """
   
    df_run_flags= dfr.copy()
    if (df_run_flags.run_rcpt.iat[0] == 1): mlist=['RCPT']

    if (df_run_flags.run_ddbc.iat[0] == 1): mlist.append('DDBC')

    if (df_run_flags.run_rfim.iat[0] == 1): mlist.append('RFIM')

    if (df_run_flags.run_ddtr.iat[0] == 1): mlist.append('DDTR')

    if (df_run_flags.run_spdt.iat[0] == 1): mlist.append('SPDT')

    if (df_run_flags.run_spsift.iat[0] == 1):  mlist.append('SPSIFT')
    
    if (df_run_flags.run_spscluster[0] == 1):  mlist.append('SPCLUST')

    if (df_run_flags.run_ffbc.iat[0] == 1):  mlist.append('FFBC')

    if (df_run_flags.run_psbc.iat[0] == 1):  mlist.append('PSBC')

    if (df_run_flags.run_cxft.iat[0] == 1):  mlist.append('CXFT') 

    if (df_run_flags.run_brdz.iat[0] == 1): mlist.append('BRDZ') 

    if (df_run_flags.run_dred.iat[0] == 1): mlist.append('DRED') 

    if (df_run_flags.run_fdas.iat[0] == 1): mlist.append('FDAS') 

    if (df_run_flags.run_sift.iat[0] == 1): mlist.append('SIFT') 

    if (df_run_flags.run_fldo.iat[0] == 1): mlist.append('FLDO') 

# make sure to capture CDOS of SPS and of PSS
    if ((df_run_flags.run_cdos.iat[0] == 1) & (df_run_flags.run_ffbc.iat[0] == 1)): 
        mlist.append('CDOSa') 
    if ((df_run_flags.run_cdos.iat[0] == 1) & (df_run_flags.run_spdt.iat[0] == 1)): 
        mlist.append('CDOSs') 
    
    return(mlist)




#  actual plotting routine
    
def plot_output_frommain(simtime,df_POM_run_flags,filedir,fileres,extraname):
     """
     plots out_resources...csv as overview plot    

     Parameters
     ----------
     simtime : int
        end of simulation time
     df_POM_run_flags : pandas dataframe
           was an output from simulation 
           (based on a check of POM against Cheetah parameters and existing POM pipelines)
     filedir : str
        output dir path
     fileres : str
        out_resources...csv
     extraname : str
       extra string that will be added to plot file name

     Returns
     -------
     pdf file in output directory

     """
     endsimtime = simtime
     
     modulenames=get_run_modulenames(df_POM_run_flags)
     # need max 16 colors
     colorlist=['darkblue','darkorange','darkcyan','brown','darkviolet','black',
                'purple', 'blue','darkslategrey','crimson','gold','indigo',
                'magenta','olive','darkturquoise','dodgerblue','darkgrey']        
     
     indir = str(filedir)
     outdir = indir
     file_r = str(fileres)
     dfr=pd.read_csv(file_r)
     
     fileouti=outdir+'POM_ModulesResources_'+extraname+'.pdf'
     
     
     entries=dfr[[ 'module_caller']].drop_duplicates(inplace=False,keep='first',ignore_index=True)
     #print('unique entries in module_caller: ",entires.module_caller)

     # create start/stop data frame
     lm=len(modulenames)
     dfs=[]

     for i in range(0,lm):
         iname=modulenames[i]
         dfs.append(get_seldf_module(dfr,iname,endsimtime).copy())
     
     # for lower plot    
     time=dfr.time_now
     Fram=dfr.cpu_ram_free/dfr.cpu_ram_free.max()
     Fcpus=dfr.cpu_cores_free/dfr.cpu_cores_free.max()

     Uram= dfr.cpu_ram_used/dfr.cpu_ram_free.max()
     Ucpus=dfr.cpu_cores_used/dfr.cpu_cores_free.max()    
     
     plt.rcParams.update({'font.size':20})
     plt.rcParams.update({'axes.labelsize':20})
     plt.rcParams.update({'axes.titlesize':20})
     plt.rcParams.update({'xtick.labelsize':20})
     plt.rcParams.update({'ytick.labelsize':20})
     plt.rcParams.update({'legend.fontsize':20})
     
     fig, (ax,ax2) = plt.subplots(2, 1,  tight_layout=True,figsize=(20,14),sharex=True,gridspec_kw={'height_ratios': [2, 1]})
     fig.subplots_adjust(hspace=0.0, wspace=0.05, left=0.07, right=0.97)



     ax2.plot(time,Uram, lw=3,zorder=4,color='blue', 
              label='used CPU RAM, (available Max: '+str(dfr.cpu_ram_free.max())+' )')

     ax2.plot(time,Ucpus, lw=3,zorder=4,color='red', 
              label='used CPU threads, (availabe Max: '+str(dfr.cpu_cores_free.max())+' )')

     ax2.xaxis.set_label_position('top') 
     ax2.xaxis.tick_top()
     ax2.set(xlabel='simulation time')
     ax2.set(ylabel='normalised resources')

     ax2.set(xlim=(0.0,endsimtime))
     ax2.set(ylim=(0,1.05))

     ax2.grid(True)
     ax2.grid(which='minor',color='grey',ls='--',lw=1)
     ax2.grid(which='major',color='grey',lw=2)
     ax2.legend(loc='upper left')


     for i in range(0,lm): 
         iname=modulenames[i]
         dfu=dfs[i].copy()
         x1=np.asarray(dfu['time_now'])
         x2=np.asarray(dfu['time_end'])
         nf=np.asarray(dfu['notfinished'])
 
         secsize= 1.0 / (lm)
         usec =0.2 *secsize
         y1=1.0-(secsize*i + usec)- secsize*0.3*nf
         y2=1.0-(secsize*i + 4.0*usec)+ secsize*0.3*nf
    
         #first entry for labels  
         ax.plot([x1[0],x2[0]],[y1[0],y2[0]], 
                 lw=8,zorder=1,color=colorlist[i],alpha=0.5,label=str(iname)+' on')
         #other entries without labels
         for k in range(1,len(dfu)):          
             ax.plot([x1[k],x2[k]],[y1[k],y2[k]], 
                     lw=8,zorder=1,color=colorlist[i],alpha=0.5)


     ax.set(xlim=(0.0,endsimtime))
     ax.set(ylim=(0,1.2))
     ax.set_yticklabels([])
     ax.grid(True,axis='x',which='major',color='grey',lw=2,zorder=4)
     ax.legend(loc='upper center',ncol=6)


     fig.tight_layout(rect=[0, 0.03, 1, 0.95])
     plt.show()
     fig.savefig(fileouti, dpi=300, bbox_inches='tight')
     logger.info('--- created '+ fileouti)
     print('--- created '+ fileouti)
     print('--- you can zoom into this plot (from time1 to time2) via:')
     print('python POM_plot_time1to2.py time1 time2 ../../output/run_modules_mostrecent.csv ../../output/ ../../output/out_resources_mostrecent.csv testZ1')
     
     
def plot_output_frommain_more(simtime,df_POM_run_flags,filedir,fileres,extraname):
     """
     plots out_resources...csv as overview plot    

     Parameters
     ----------
     simtime : int
        end of simulation time
     df_POM_run_flags : pandas dataframe
           was an output from simulation 
           (based on a check of POM against Cheetah parameters and existing POM pipelines)
     filedir : str
        output dir path
     fileres : str
        out_resources...csv
     extraname : str
       extra string that will be added to plot file name

     Returns
     -------
     pdf file in output directory

     """
     endsimtime = simtime
     
     modulenames=get_run_modulenames(df_POM_run_flags)
     # need max 16 colors
     colorlist=['darkblue','darkorange','darkcyan','brown','darkviolet','black',
                'purple', 'blue','darkslategrey','crimson','gold','indigo',
                'magenta','olive','darkturquoise','dodgerblue','darkgrey']        
     
     indir = str(filedir)
     outdir = indir
     file_r = str(fileres)
     dfr=pd.read_csv(file_r)
     
     fileouti=outdir+'POM_ModulesResources_'+extraname+'.pdf'
     
     
     entries=dfr[[ 'module_caller']].drop_duplicates(inplace=False,keep='first',ignore_index=True)
     #print('unique entries in module_caller: ",entires.module_caller)

     # create start/stop data frame
     lm=len(modulenames)
     dfs=[]

     for i in range(0,lm):
         iname=modulenames[i]
         dfs.append(get_seldf_module(dfr,iname,endsimtime).copy())
     
     # for lower plot    
     time=dfr.time_now
     Fram=dfr.cpu_ram_free/dfr.cpu_ram_free.max()
     Fcpus=dfr.cpu_cores_free/dfr.cpu_cores_free.max()

     Uram= dfr.cpu_ram_used/dfr.cpu_ram_free.max()
     Ucpus=dfr.cpu_cores_used/dfr.cpu_cores_free.max()    
     
     Uram2 = dfr.cpu_ram_used/dfr.cpu_ram_used.max()
     
     plt.rcParams.update({'font.size':20})
     plt.rcParams.update({'axes.labelsize':20})
     plt.rcParams.update({'axes.titlesize':20})
     plt.rcParams.update({'xtick.labelsize':20})
     plt.rcParams.update({'ytick.labelsize':20})
     plt.rcParams.update({'legend.fontsize':20})
     
     fig, (ax,ax2,ax3) = plt.subplots(3, 1,  tight_layout=True,figsize=(20,18),
                                      sharex=True,gridspec_kw={'height_ratios': [2, 1,1]})
     fig.subplots_adjust(hspace=0.0, wspace=0.05, left=0.07, right=0.97)



     ax2.plot(time,Uram, lw=3,zorder=4,color='blue', 
              label='used CPU RAM, (available Max: '+str(dfr.cpu_ram_free.max())+' )')

     ax2.plot(time,Ucpus, lw=3,zorder=4,color='red', 
              label='used CPU threads, (availabe Max: '+str(dfr.cpu_cores_free.max())+' )')

     ax2.xaxis.set_label_position('top') 
     ax2.xaxis.tick_top()
     ax2.set(xlabel='simulation time')
     ax2.set(ylabel='normalised resources')

     ax2.set(xlim=(0.0,endsimtime))
     ax2.set(ylim=(0,1.05))

     ax2.grid(True)
     ax2.grid(which='minor',color='grey',ls='--',lw=1)
     ax2.grid(which='major',color='grey',lw=2)
     ax2.legend(loc='upper left')


     ax3.plot(time,Uram2, lw=3,zorder=4,color='blue', 
              label='ratio to -used- MAX (='+ str(int(dfr.cpu_ram_used.max())) + ')')

     ax3.xaxis.set_label_position('top') 
     ax3.xaxis.tick_top()
     ax3.set(xlabel='simulation time')
     ax3.set(ylabel='used CPU RAM / USED MAX')

     ax3.set(xlim=(0.0,endsimtime))
     ax3.set(ylim=(0,1.05))

     ax3.grid(True)
     ax3.grid(which='minor',color='grey',ls='--',lw=1)
     ax3.grid(which='major',color='grey',lw=2)
     ax3.legend(loc='lower center')

     for i in range(0,lm): 
         iname=modulenames[i]
         dfu=dfs[i].copy()
         x1=np.asarray(dfu['time_now'])
         x2=np.asarray(dfu['time_end'])
         nf=np.asarray(dfu['notfinished'])
 
         secsize= 1.0 / (lm)
         usec =0.2 *secsize
         y1=1.0-(secsize*i + usec)- secsize*0.3*nf
         y2=1.0-(secsize*i + 4.0*usec)+ secsize*0.3*nf
    
         #first entry for labels  
         ax.plot([x1[0],x2[0]],[y1[0],y2[0]], 
                 lw=8,zorder=1,color=colorlist[i],alpha=0.5,label=str(iname)+' on')
         #other entries without labels
         for k in range(1,len(dfu)):          
             ax.plot([x1[k],x2[k]],[y1[k],y2[k]], 
                     lw=8,zorder=1,color=colorlist[i],alpha=0.5)


     ax.set(xlim=(0.0,endsimtime))
     ax.set(ylim=(0,1.2))
     ax.set_yticklabels([])
     ax.grid(True,axis='x',which='major',color='grey',lw=2,zorder=4)
     ax.legend(loc='upper center',ncol=6)


     fig.tight_layout(rect=[0, 0.03, 1, 0.95])
     plt.show()
     fig.savefig(fileouti, dpi=300, bbox_inches='tight')
     logger.info('--- created '+ fileouti)
     print('--- created '+ fileouti)
     print('--- you can zoom into this plot (from time1 to time2) via:')
     print('python POM_plot_time1to2.py time1 time2 ../../output/run_modules_mostrecent.csv ../../output/ ../../output/out_resources_mostrecent.csv testZ1')
     
     
     
                
def plot_output_time1to2(time1,time2,df_POM_run_flags,filedir,fileres,extraname):
    
    """
    plots a time zoom-in window in simulation time

    Parameters
    ----------
    time1 : float
           starting time 
    time2 : float
           ending time
    df_POM_run_flags : pandas datafram
           was an output from simulation 
           (based on a check of POM against Cheetah parameters and existing POM pipelines)
    filedir : str
        directory for output file
    fileres : str
        out_resources...csv file (with path)
    extraname : str
        extra string that will be added to plot file name

    Returns
    -------
    pdf file in output directory

    """
    #endsimtime = simtime
    endsimtime = df_POM_run_flags.simtime.iat[0]
    astr1= '_t'+str(time1)+'_to_t'+str(time2)
     
     
    modulenames=get_run_modulenames(df_POM_run_flags)
     # need max 16 colors
    colorlist=['darkblue','darkorange','darkcyan','brown','darkviolet','black',
                'purple', 'blue','darkslategrey','crimson','gold','indigo',
                'magenta','olive','darkturquoise','dodgerblue','darkgrey']        
     
    outdir = str(filedir)
    file_r = str(fileres)
    dfr=pd.read_csv(file_r)
     
    fileouti=outdir+'POM_ModulesResources_'+extraname+astr1+'.pdf'
     
     
    entries=dfr[[ 'module_caller']].drop_duplicates(inplace=False,keep='first',ignore_index=True)
     #print('unique entries in module_caller: ",entires.module_caller)

    # create start/stop data frame
    lm=len(modulenames)
    dfs=[]

    for i in range(0,lm):
         iname=modulenames[i]
         dfs.append(get_seldf_module(dfr,iname,endsimtime).copy())
     
     # for lower plot    
    time=dfr.time_now
    Fram=dfr.cpu_ram_free/dfr.cpu_ram_free.max()
    Fcpus=dfr.cpu_cores_free/dfr.cpu_cores_free.max()

    Uram= dfr.cpu_ram_used/dfr.cpu_ram_free.max()
    Ucpus=dfr.cpu_cores_used/dfr.cpu_cores_free.max()    
     
    plt.rcParams.update({'font.size':20})
    plt.rcParams.update({'axes.labelsize':20})
    plt.rcParams.update({'axes.titlesize':20})
    plt.rcParams.update({'xtick.labelsize':20})
    plt.rcParams.update({'ytick.labelsize':20})
    plt.rcParams.update({'legend.fontsize':20})
     
    fig, (ax,ax2) = plt.subplots(2, 1,  tight_layout=True,figsize=(20,14),sharex=True,gridspec_kw={'height_ratios': [2, 1]})
    fig.subplots_adjust(hspace=0.0, wspace=0.05, left=0.07, right=0.97)



    ax2.plot(time,Uram, lw=3,zorder=4,color='blue', 
              label='used CPU RAM, (available Max: '+str(dfr.cpu_ram_free.max())+' )')

    ax2.plot(time,Ucpus, lw=3,zorder=4,color='red', 
              label='used CPU threads, (availabe Max: '+str(dfr.cpu_cores_free.max())+' )')

    ax2.xaxis.set_label_position('top') 
    ax2.xaxis.tick_top()
    ax2.set(xlabel='simulation time')
    ax2.set(ylabel='normalised resources')

#     ax2.set(xlim=(0.0,endsimtime))
    ax2.set(xlim=(time1,time2))
    ax2.set(ylim=(0,1.05))

    ax2.grid(True)
    ax2.grid(which='minor',color='grey',ls='--',lw=1)
    ax2.grid(which='major',color='grey',lw=2)
    ax2.legend(loc='upper left')


    for i in range(0,lm): 
         iname=modulenames[i]
         dfu=dfs[i].copy()
         x1=np.asarray(dfu['time_now'])
         x2=np.asarray(dfu['time_end'])
         nf=np.asarray(dfu['notfinished'])
 
         secsize= 1.0 / (lm)
         usec =0.2 *secsize
         y1=1.0-(secsize*i + usec)- secsize*0.3*nf
         y2=1.0-(secsize*i + 4.0*usec)+ secsize*0.3*nf
    
         #first entry for labels  
         ax.plot([x1[0],x2[0]],[y1[0],y2[0]], 
                 lw=8,zorder=1,color=colorlist[i],alpha=0.5,label=str(iname)+' on')
         #other entries without labels
         for k in range(1,len(dfu)):          
             ax.plot([x1[k],x2[k]],[y1[k],y2[k]], 
                     lw=8,zorder=1,color=colorlist[i],alpha=0.5)


     #ax.set(xlim=(0.0,endsimtime))
    ax.set(xlim=(time1,time2))
    ax.set(ylim=(0,1.2))
    ax.set_yticklabels([])
    ax.grid(True,axis='x',which='major',color='grey',lw=2,zorder=4)
    ax.legend(loc='upper center',ncol=6)


    fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.show()
    fig.savefig(fileouti, dpi=300, bbox_inches='tight')
    logger.info('--- created '+ fileouti)
    print('--- created '+ fileouti)
                
                
                