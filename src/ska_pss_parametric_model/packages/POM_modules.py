#!/usr/bin/env python3                
# -*- coding: utf-8 -*-               
# 
"""
some class definitions and copy of main class code PSS_POM

"""

import numpy as np
import pandas as pd
import simpy
import logging
import warnings
from  time import sleep

#import packages.POM_readin as pr

logger = logging.getLogger("pomlog")

#------------------------------------------------------------------------------
#  general class definitions
#------------------------------------------------------------------------------

# these classes can be outside the main routine / not visible for spyder
class glob:
    """
    Class glob stores the global parameters, 
    based on config parameter pandas dataframe df_POM_parT
    
    """
    def __init__(self,df_POM_parT):
        self.number_of_runs=int(df_POM_parT.number_of_runs.iloc[0])  # how many simulations
        self.sim_duration=df_POM_parT.sim_duration.iloc[0]      # run time of one sim

        self.obs_length= df_POM_parT.obs_length.iloc[0]         #  observation length in time
        self.chunk_buffer_length= df_POM_parT.chunk_length.iloc[0]  # chunk size in time
        self.N_chunks = np.round(self.obs_length/self.chunk_buffer_length)    # number chunks per obs
 #    chunk_buffer_size= chunk_buffer_length * 8 # in bits [for now time *8 bit]
        try: 
            self.time_resolution = df_POM_parT.time_resolution.iloc[0]
        except:
            self.time_resolution = None
    
# time of RCP,... module to process one chunk of data    
        try: 
            self.t_RCPT_i = df_POM_parT.t_RCP_i.iloc[0]        
        except:
            self.t_RCPT_i = None        

    
        try: 
            self.t_DDBC_i= df_POM_parT.t_DDBC_i.iloc[0]  
        except:
            self.t_DDBC_i= None 

        try: 
            self.t_RFIM= df_POM_parT.t_RFIM.iloc[0]  
        except:
            self.t_RFIM = None

        try: 
            self.t_DDTR= df_POM_parT.t_DDTR.iloc[0]  
        except:
            self.t_DDTR = None 

# real time to processing time ratio for DDTR
        try: 
            self.r_to_p_DDTR= df_POM_parT.r_to_p_DDTR.iloc[0]  
        except:
            self.r_to_p_DDTR = None

        try: 
            self.t_SPDT= df_POM_parT.t_SPDT.iloc[0]  
        except:
            self.t_SPDT = None
# real time to processing time ratio for SPDT
        try: 
            self.r_to_p_SPDT= df_POM_parT.r_to_p_SPDT.iloc[0]  
        except:
            self.r_to_p_SPDT = None  

# processing time (for x candidate per min) SPS sifting, clustering
        try: 
            self.t_SPSIFT= df_POM_parT.t_SPSIFT.iloc[0]  
        except:
            self.t_SPSIFT = None  

        try: 
            self.t_SPCLUST= df_POM_parT.t_SPCLUST.iloc[0]  
        except:
            self.t_SPCLUST = None

# time to put one chunk in output buffer of FFBC    
#    time_chunk_in_output=3   
        try: 
            self.t_FFBC_i= df_POM_parT.t_FFBC_i.iloc[0]  
        except:
            self.t_FFBC_i= None
        
# time to write out [modified] observation [all chunks] data + other output    
#    time_obs_in_output=10   
        try: 
            self.t_CDOS_all= df_POM_parT.t_CDOS_all.iloc[0]  
        except:
            self.t_CDOS_all= None  

        try: 
            self.t_CDOS_sps= df_POM_parT.t_CDOS_sps.iloc[0]  
        except:
            self.t_CDOS_sps= None
        
# memory usage or parameters to calculate it
        #for SPS DDTR and SPDT
        # change later to read in from Cheetah config/POM config
        try: 
            self.dedisp_samples = df_POM_parT.dedisp_samples.iloc[0]
        except:
            self.dedisp_samples =  None
            
# should be later read rom config file            
        self.DM1 = [0,100,0.1]
        self.DM2 = [100,300,0.2]
        self.DM3 = [300,700,0.4]
        self.DM4 = [700,1500,0.8]
        self.DM5 = [1500,3100,1.6]
        self.DM6 = [3100,6300,3.2]
        logger.info('/glob/ --- Note that the DM-plan is currently hard-coded in class glob---')

        
 # number of chunks going into one buffer (later to be deduced from the config)
        self.chunks_in_ddbc = df_POM_parT.chunks_in_ddbc.iloc[0]
  #      self.chunks_in_ddbc = int(np.ceil( ((self.dedisp_samples * self.time_resolution / self.chunk_buffer_length)) ))
  # can be different from Ag_Na+Ag_Nb, hence reset for sps 
        try: 
            self.highest_freq = (df_POM_parT.highest_freq.iloc[0]) 
        except:
            self.highest_freq =  None
            
        try: 
            self.lowest_freq =self.highest_freq - (df_POM_parT.freq_bw.iloc[0] ) 
        except:
            self.lowest_freq = None

        try: 
            self.chan_in_sample = (df_POM_parT.chan_in_sample.iloc[0]) 
        except:
            self.chan_in_sample = 3700 # CBF mid value [22/1/25]
         
# get POM config parameters, e.g., M_ processing memory, D_ data memory
        try: 
            self.M_RCPT =df_POM_parT.M_RCPT.iloc[0]
        except:
            self.M_RCPT = None
        try: 
            self.D_RCPT =df_POM_parT.D_RCPT.iloc[0]
        except:
            self.D_RCPT = None

        try: 
            self.M_DDBC =df_POM_parT.M_DDBC.iloc[0]
        except:
            self.M_DDBC = None

        try: 
            self.D_DDBC =df_POM_parT.D_DDBC.iloc[0]
        except:
            self.D_DDBC = None

        try: 
            self.M_RFIM =df_POM_parT.M_RFIM.iloc[0]
        except:
            self.M_RFIM = None

        try: 
            self.D_RFIM =df_POM_parT.D_RFIM.iloc[0]
        except:
            self.D_RFIM = None

        try: 
            self.M_DDTR =df_POM_parT.M_DDTR.iloc[0]
        except:
            self.M_DDTR = None

        try: 
            self.D_DDTR =df_POM_parT.D_DDTR.iloc[0]
        except:
            self.D_DDTR = None

        try: 
            self.M_SPDT =df_POM_parT.M_SPDT.iloc[0]
        except:
            self.M_SPDT = None

        try: 
            self.D_SPDT =df_POM_parT.D_SPDT.iloc[0]
        except:
            self.D_SPDT = None

        try: 
            self.M_SPSIFT =df_POM_parT.M_SPSIFT.iloc[0]
        except:
            self.M_SPSIFT = None

        try: 
            self.D_SPSIFT =df_POM_parT.D_SPSIFT.iloc[0]
        except:
            self.D_SPSIFT = None

        try: 
            self.M_SPCLUST =df_POM_parT.M_SPCLUST.iloc[0]
        except:
            self.M_SPCLUST = None

        try: 
            self.D_SPCLUST =df_POM_parT.D_SPCLUST.iloc[0]
        except:
            self.D_SPCLUST = None

        try: 
            self.M_CDOS_sps =df_POM_parT.M_CDOS_sps.iloc[0]
        except:
            self.M_CDOS_sps = None

        try: 
            self.D_CDOS_sps =df_POM_parT.D_CDOS_sps.iloc[0]
        except:
            self.D_CDOS_sps = None

        self.M_FFBC =df_POM_parT.M_FFBC.iloc[0]
        self.D_FFBC =df_POM_parT.D_FFBC.iloc[0]
        self.M_CDOS =df_POM_parT.M_CDOS.iloc[0]
        self.D_CDOSa =df_POM_parT.D_CDOSa.iloc[0]

# cpu usage
        try: 
            self.cpu_RCPT =df_POM_parT.cpu_RCPT.iloc[0]
        except:
            self.cpu_RCPT = None

        try: 
            self.cpu_DDBC =df_POM_parT.cpu_DDBC.iloc[0]
        except:
            self.cpu_DDBC = None

        try: 
            self.cpu_FFBC =df_POM_parT.cpu_FFBC.iloc[0]
        except:
            self.cpu_FFBC = None

        try: 
            self.cpu_RFIM =df_POM_parT.cpu_RFIM.iloc[0]
        except:
            self.cpu_RFIM = None

        try: 
            self.cpu_DDTR =df_POM_parT.cpu_DDTR.iloc[0]
        except:
            self.cpu_DDTR = None

        try: 
            self.cpu_SPDT =df_POM_parT.cpu_SPDT.iloc[0]
        except:
            self.cpu_SPDT = None

        try: 
            self.cpu_SPSIFT =df_POM_parT.cpu_SPSIFT.iloc[0]
        except:
            self.cpu_SPSIFT = None

        try: 
            self.cpu_SPCLUST =df_POM_parT.cpu_SPCLUST.iloc[0]
        except:
            self.cpu_SPCLUST = None
            
        try:
            self.cluster_algorithm = df_POM_parT.cluster_algorithm.iloc[0]
        except:    
            self.cluster_algorithm = 1 # set to HDBSCAN if issue
            
        # only FoF=0 or HDBSCAN=1 allowed   
        if ((self.cluster_algorithm != 0) & (self.cluster_algorithm != 1)):
            self.cluster_algorithm = 1

        try: 
            self.cpu_CDOS =df_POM_parT.cpu_CDOS.iloc[0]
        except:
            self.cpu_CDOS = None

        try: 
            self.cpu_CDOS_sps =df_POM_parT.cpu_CDOS_sps.iloc[0]
        except:
            self.cpu_CDOS_sps = None

# gpu usage
        self.gpu_RFIM =df_POM_parT.gpu_RFIM.iloc[0]

    
# available resources    
        self.available_cpu_ram = df_POM_parT.available_cpu_ram.iloc[0] # needs to be integer
        self.available_cpu_cores=(df_POM_parT.available_cpu_cores.iloc[0] )# needs to be integer
        self.available_gpus=df_POM_parT.available_gpus.iloc[0] # needs to be integer
        self.available_mem_pergpu=df_POM_parT.available_mem_pergpu.iloc[0] # needs to be integer  

        # should be the same as in the DDBC buffer
        try: 
            self.chunks_in_rfim = self.chunks_in_ddbc
        except:
            self.chunks_in_rfim =  None
        
# number of SPS candidates per minute AFTER simple thresholding
        try: 
            self.number_of_SPSIFTcandidates_per_min = df_POM_parT.number_of_SPSIFTcandidates_per_min.iloc[0]
        except:
            self.number_of_SPSIFTcandidates_per_min =  None
# reduction factor for candidate list after SPSIFT  
# - we will multiply by this number to get the initial/raw number of SPS candidates       
        try: 
            self.reduce_factor_SPSIFT = df_POM_parT.reduce_factor_SPSIFT.iloc[0]
        except:
            self.reduce_factor_SPSIFT =  None
# reduction factor for candidate list after SPS_clustering
# - we will divide by this to get the reduced number
        try: 
            self.reduce_factor_SPCLUST = df_POM_parT.reduce_factor_SPCLUST.iloc[0]
        except:
            self.reduce_factor_SPCLUST =  None

###############################################################################
#  for realistic SPS pipeline setup
###############################################################################

def DM_time_object_size(g, agg_samples_a):
    """
    calculates the size of one DM-time object in bytes

    Parameters
    ----------
    param g : global class with DM plan
    param agg_samples_a : number of samples in the aggregation buffer 

    Returns
    -------
    DMtime_size_inbyte : size of one DM-time object in bytes

    """
    
    samp = agg_samples_a
    DMstart= [g.DM1[0],g.DM2[0],g.DM3[0],g.DM4[0],g.DM5[0],g.DM6[0]]   
    DMend=   [g.DM1[1],g.DM2[1],g.DM3[1],g.DM4[1],g.DM5[1],g.DM6[1]]   
    DMstep=  [g.DM1[2],g.DM2[2],g.DM3[2],g.DM4[2],g.DM5[2],g.DM6[2]]   
    
    size= samp * (DMend[0]-DMstart[0])/DMstep[0]
    for i in range(1,6):
        size = size + (samp / (i*2)  * (DMend[i]-DMstart[i])/DMstep[i] )

    # 32 is for floats, /8 because it is in byte , not bits
    DMtime_size_inbyte = size * 32 / 8 # these are floats
    return(DMtime_size_inbyte)

def DDTR_SPSprep(g):
    """
    calculates the overlap region in the SPS DDTR aggregation buffer  based on 
    maximum DM in the DM-plan and the number of samples in the buffer (config parameters)

    Parameters
    ----------
    g : global class with the config parameters

    Returns
    -------
    AB_Na : number of RCPT chunks in aggregation buffer NOT in the overlap region
    AB_Nb : number of RCPT chunks in overlap region of aggregation buffer
    chunks_in_ddbcN : AB_Na + AB_Nb (old value overwritten to account for any rounding issues [integer chunks])
    AB_size_inbyte : memory usage of aggregation buffer (TO BE UPDATED once more Stokes are used)
    DMtime_size_inbyte : memory usage for the DM-time object

    """
    # aggregation buffer has length of dedisp_samples, but part of it is overlap
    # passed-on-part is a, overlap region is b
    agg_samples = g.dedisp_samples
    # get the maximum DM
    maxDM=np.max([g.DM1[1],g.DM2[1],g.DM3[1],g.DM4[1],g.DM5[1],g.DM6[1]])
    # Delta_t = 4.15e6 ms * DM (1/f1^2 - 1/f2^2)  
    max_sweap_inms = 4.15e6 * maxDM * (1.0/g.lowest_freq/g.lowest_freq - 1.0/g.highest_freq/g.highest_freq)
    # time resolution is in mus => 1000 in 1 ms
    max_sweap_insamples = int(np.ceil(max_sweap_inms * 1000 / g.time_resolution))
    #print('agg_samples,maxDM,max_sweap_inms,max_sweap_insamples: ',agg_samples,maxDM,max_sweap_inms,max_sweap_insamples)

    agg_samples_b = max_sweap_insamples # overlap region in samples
    agg_samples_a = agg_samples - max_sweap_insamples # "usable" part of aggregation buffer for max sweap
    if (agg_samples_a <= 0): 
        logger.info('THIS DOES NOT WORK - Aggregation buffer size smaller than overlap region')
    
    #in number of chunks
    AB_Na = int(np.ceil(agg_samples_a * g.time_resolution / g.chunk_buffer_length))
    AB_Nb = int(np.ceil(agg_samples_b * g.time_resolution / g.chunk_buffer_length))
    
    #FormallY:
    #chunks_in_ddbc =int(np.ceil( ((g.dedisp_samples * g.time_resolution / g.chunk_buffer_length)) ))
    # but can be different than AB_Na+AB_Nb because of rounding, hence:
    chunks_in_ddbcN = AB_Na + AB_Nb    
    
    
    # similar to rcpt_setup: 8 bit, 4096 channels per sample, only Stokes-I for now
    # for CBF mid 3700 , replace with number of frequency channels in config file
    #AB_size_inbites = g.dedisp_samples * 8 * 4096
    AB_size_inbyte = g.dedisp_samples * g.chan_in_sample
    
    # The DM-time object is only setup for the time coverage of the aggregation 
    # buffer that is NOT in the overlap region [confirmed by Arun]
    DMtime_size_inbyte = DM_time_object_size(g, agg_samples_a)

    logstr='DDTR_prep AB_Na, AB_Nb,AB_size_inbyte,DMtime_size_inbyte: ' +\
            str(AB_Na) +','+str(AB_Nb) +','+str(AB_size_inbyte) +','+str(DMtime_size_inbyte) 
    logger.info(logstr)        
#    print('DDTR_prep AB_Na, AB_Nb,AB_size_inbyte,DMtime_size_inbyte:',AB_Na, AB_Nb,AB_size_inbyte,DMtime_size_inbyte)
    return(AB_Na, AB_Nb,chunks_in_ddbcN,AB_size_inbyte,DMtime_size_inbyte)



def sps_setup(g):
    """
    calculates the parameters for the SPS  

    Parameters
    ----------
    g : global class with config parameters

    Returns
    -------
    chunks_in_ddbcN :  number of RCPT chunks go into one SPS aggregation buffer
    AB_Nb :  number of RCPT chunks in overlap region of aggregation buffer
    D_DDBCaggB_per_chunk : size of DDBC SPS aggregation buffer per RCPT chunk
    D_DDTR_per_AggB : size of DM-t object per SPS aggregation buffer
    t_DDTR_allthreads : processing time of DDTR per aggregation buffer, accounting for all threads
    D_SPDTaggB : required data memory for SPSPDT result (raw candidate list) per aggregation buffer
    t_SPDTmod : processing time of SPDT per DM-time object (i.e. per aggregation buffer)
    D_SPSIFTaggB : required data memory for SPSIFT result (thresholded candidate list) per aggregation buffer
    t_SPSCLUSTaggB_allthreads, : processing time of clustering per aggregation buffer, accounting for all threads
    M_SPSCLUSTaggB : required processing memory for SPSclustering per aggregation buffer
    D_SPSCLUSTaggB : required  data memory for SPSclustering (final candidate list) result per aggregation buffer

    """
    
    min_in_mus = 60 * 1e6
    
    D_per_candidate = 40 # bytes; required memory per candidate in candidate list 
    #from candidate class; sp_candidate.h in cheetah>data
    #[6 values: DM:float, width:double, start time:double, end time:double, S/N:float, candidateID: long(= 8 bytes)]
    
    #  -------------  for SPS: DDBC, DDTR and SPDT
    # a+b = whole agg buffer, b = overlap region in aggregation buffer
    # NumOfChunks_AggB_a,NumOfChunks_AggB_b, total size AggB, size of DMt object per AggB 
    AB_Na, AB_Nb,chunks_in_ddbcN,AB_size_inbyte,DMtime_size_inbyte = DDTR_SPSprep(g)
    time_in_AggB = chunks_in_ddbcN * g.chunk_buffer_length #in mus
    
    # only candidates in this portion of the aggregation buffer will be used because of DM consideration
    usable_time_in_AggB = AB_Na * g.chunk_buffer_length #in mus
    
    
    istr1='/sps_setup/ aggregation buffer Na chunks, Nb(overlap), total-in-AggB chunks ' + \
          str(AB_Na)+' , '+ str(AB_Nb)+' , '+str(chunks_in_ddbcN)
    istr2='/sps_setup/ aggregation buffer in Byte, DM-time object in Byte: ' +\
           str(AB_size_inbyte)+' , '+str(DMtime_size_inbyte)
    logger.info(istr1)
    logger.info(istr2)
    
    #  -------------  for SPS: DDBC
    D_DDBCaggB_per_chunk = int(np.ceil(AB_size_inbyte / chunks_in_ddbcN  ))
    # M_DDBC, t_DDBC [just CPU copy] from config , should be very small    
    istr3='/sps_setup/ D_DDBC aggregation buffer size per chunk: ' +\
           str(D_DDBCaggB_per_chunk)
    logger.info(istr3)
    
    #  -------------  for SPS: DDTR (after DDBC all per aggregation buffer) 
    D_DDTR_per_AggB = DMtime_size_inbyte
    #M_DDTR from config, should be very small
    istr4='/sps_setup/ D_DDTR DM-time object size per aggregation buffer: ' +\
           str(D_DDTR_per_AggB)
    logger.info(istr4)

    minimum_threads_DDTR = int(np.ceil( 1.0 / g.r_to_p_DDTR ) ) # minimum for real-time-processing
    if (minimum_threads_DDTR > g.cpu_DDTR):
        errstr='DDTR setup -- too few threads, for real-time, number of threads set to: '+str(g.cpu_DDTR)+ \
                ', but need at least '+str(minimum_threads_DDTR)                
        logger.error(errstr )
    
    # scale run time with number of threads
    # use *total* time of buffer because all is processed in DDTR
    t_DDTR_allthreads = int(np.ceil(( 1.0/ g.r_to_p_DDTR * time_in_AggB ) / g.cpu_DDTR)) # round to mus    
    
    
    
    #  -------------  for SPS: SPDT
    # only 1 thread done/planned
    # use only time part in DM-time object (i.e. time in aggregation buffer NOT in overlap)
    t_SPDTmod = int(np.ceil(( 1.0/ g.r_to_p_SPDT * usable_time_in_AggB ) )) # round to mus
    
    # do D_SPDT below because we need raw-number-of candidates
    # M_SPDT in config should include the extra buffer used in SPDT to estimate width of puls at the end of aggB
    

    istr5='/sps_setup/ : time_in_AggB, t_DDTR_allthreads (factor to time_in_AggB), time_in_DMtobject (=usable_time_in_AggB), t_SPDTmod (factor to usable_time_in_AggB): ' +\
           str(time_in_AggB)+' , '+str(t_DDTR_allthreads)+' , ('+str(t_DDTR_allthreads/time_in_AggB)+'),'+ \
           str(usable_time_in_AggB) +', ' +  str(t_SPDTmod)+' , ('+str(t_SPDTmod/usable_time_in_AggB)+'),'
    logger.info(istr5)
   
     
    # -------------  for SPSIFT
    # get number BEFORE SPSIFT 
    N_raw_candis_per_min = g.number_of_SPSIFTcandidates_per_min * g.reduce_factor_SPSIFT
    N_raw_candis_per_chunk = N_raw_candis_per_min / min_in_mus * g.chunk_buffer_length
    # all the raw candidates discovered/expected in the DM-t object (i.e. only NOT from the overlap region in AggB)
    N_raw_candis_per_aggBuffer = int(np.ceil( N_raw_candis_per_chunk * AB_Na ))
    # perhaps add scaling for M_SIFT, t_SIFT later here
    
    #  -------------  for SPS: SPDT
    D_SPDTaggB =  N_raw_candis_per_aggBuffer * D_per_candidate

    
    # -------------  for SPSIFT
    # number of candidates in (usable part of) Aggregation buffer/ DM-time object AFTER SIFT
    N_candis_per_aggBuffer = int(np.ceil( ( g.number_of_SPSIFTcandidates_per_min 
                                 / min_in_mus * g.chunk_buffer_length
                                 * AB_Na) ))

    D_SPSIFTaggB = N_candis_per_aggBuffer * D_per_candidate + g.D_SPSIFT
                                
    # ------------- for SPSclustering
    # get number AFTER SPSclustering
    N_final_candis_per_min = g.number_of_SPSIFTcandidates_per_min / g.reduce_factor_SPCLUST
    N_final_candis_per_chunk = N_final_candis_per_min / min_in_mus * g.chunk_buffer_length
    # in DM-time object, i.e. "A" part of aggregation buffer
    N_final_candis_per_aggBuffer = int(np.ceil( N_final_candis_per_chunk * AB_Na ))
 
    # for HDBSCAN algorithm
    if (g.cluster_algorithm == 1):
    # calculate t_SPSCLUSTaggB, assume N^2 dependecy for HDBSCAN
    # N in aggregation buffer is different to N_per_min, per CPU adds another scaling 
        N_final_candis_per_cpu = N_final_candis_per_aggBuffer / g.cpu_SPCLUST 
        # this number is scaled with N**2
        N_CLUST_scale = ( N_final_candis_per_cpu / N_final_candis_per_min) * \
                        ( N_final_candis_per_cpu / N_final_candis_per_min)
    # for Friends-of-friends algorithm
    if (g.cluster_algorithm == 0):
        # assume just simple scaling with N
        N_CLUST_scale = N_final_candis_per_aggBuffer / N_final_candis_per_min / g.cpu_SPCLUST
    
    
    
    #t_clustersift = 0 - add later when implemented
    # scale with number of threads (11/24:only 1)
    t_SPSCLUSTaggB_allthreads = int(np.ceil((g.t_SPCLUST * N_CLUST_scale)  ) ) # round to mus
    # will overestimate time for more than one CPU, would need to adapt to the changed N because of N^2 (for HDBSCAN)
 
    
    # calculate M_SPSCLUSTaggB,  for HDBSCAN, (M_SPSCLUST must be per candidate)
    M_SPSCLUSTaggB = N_candis_per_aggBuffer * g.M_SPCLUST
    D_SPSCLUSTaggB = N_final_candis_per_aggBuffer * D_per_candidate

    istr6a='/sps_setup/ N_candidates after SPDT, SPSIFT, SPSclustering (per DM-time object from one aggregation buffer): ' +\
           str(N_raw_candis_per_aggBuffer)+' , '+str(N_candis_per_aggBuffer)+' , '+str(N_final_candis_per_aggBuffer)
    logger.info(istr6a)

    istr6='/sps_setup/ D_candidates after SPDT, SPSIFT, SPSclustering (per  DM-time object from one aggregation buffer): ' +\
           str(D_SPDTaggB)+' , '+str(D_SPSIFTaggB)+' , '+str(D_SPSCLUSTaggB)
    logger.info(istr6)

    istr7='/sps_setup/ SPSclustering processing memory (all SIFT candidates per DM-time object from one aggregation buffer): ' +\
           str(M_SPSCLUSTaggB)
    logger.info(istr7)

    istr8='/sps_setup/ SPSclustering threads, processing time per aggregation buffer for all threads (factor with respect to time_in_AggB): ' +\
           str(g.cpu_SPCLUST)+' , '+str(t_SPSCLUSTaggB_allthreads) +\
               '  ('+str(t_SPSCLUSTaggB_allthreads/time_in_AggB)+'),'
    logger.info(istr8)

    return(chunks_in_ddbcN, # how many total chunks go into one aggregation buffer (with overlap)
           AB_Nb, # number of chunks in overlap region of aggregation buffer
           D_DDBCaggB_per_chunk, # size of DDBC aggregation buffer per chunk
           D_DDTR_per_AggB, # size of DM-t object per aggregation buffer
           t_DDTR_allthreads,  # processing time of DDTR per aggregation buffer for all threads
           D_SPDTaggB,# data of SPSPDT result (raw candidates) per DM-time object from one aggregation buffer
           t_SPDTmod,  # processing time of SPDT per aggregation buffer or DM-time object
           D_SPSIFTaggB, # data of SPSIFT result (sifted candidates) per  DM-time object from one aggregation buffer
           t_SPSCLUSTaggB_allthreads, # processing time of clustering per DM-time object from one aggregation buffer, all threads
           M_SPSCLUSTaggB, # processing memory for SPS clustering per DM-time object from one aggregation buffer
           D_SPSCLUSTaggB # data of SPSclustering (final candidates) result per aggregation buffer
           )

###############################################################################
#       helper routines
###############################################################################
    
def print_memerror(modulename,needm,availm,attime):
    """
    RAM/memory error message function

    Parameters
    ----------
    modulename : name of module where error was raised
    needm : the requested RAM/memory
    availm : the available RAM/memory
    attime : simulation time when it happened
    
    Returns
    -------
    Log messages

    """
    ramstr = ('==================== RAM/Memory error  ========================== \n'+ 
              '=== in:        '+ str(modulename) + ' at time ' + str(attime) +'\n'+
              '=== need:      '+ str(needm) +'\n'+    
              '=== available: '+str(availm)+'\n'+    
              '==================================================================')
    logger.warning(ramstr)


def print_cpuerror(modulename,needcpu,availcpu,attime):
    """
    CPU/thread number error message function

    Parameters
    ----------
    modulename : name of module where error was raised
    needcpu : the requested CPUs/threads
    availcpu : the available CPUs/threads
    attime : simulation time when it happened
    
    Returns
    -------
    Log messages

    """
    cpustr=('==================== CPU/thread  error  ========================== \n'+ 
            '=== in:        '+str(modulename) + ' at time ' + str(attime) + '\n'+
            '=== need:      '+str(needcpu) +'\n'+    
            '=== available: '+str(availcpu)+'\n'+
            '==================================================================')
    logger.warning(cpustr)

def print_gpuerror(modulename,needgpu,availgpu,attime):
    """
    GPU number error message function

    Parameters
    ----------
    modulename : name of module where error was raised
    needcpu : the requested GPU units
    availcpu : the available GPU units
    attime : simulation time when it happened
    
    Returns
    -------
    Log messages

    """
    cpustr=('==================== GPU request error  ========================== \n'+ 
            '=== in:        '+str(modulename) + ' at time ' + str(attime) + '\n'+
            '=== need:      '+str(needgpu) +'\n'+    
            '=== available: '+str(availgpu)+'\n'+
            '==================================================================')
    logger.warning(cpustr)


def print_backtonormal(what,errflag,time):
    """
    Indicates when resources have become available after a previous failed resource request
    Only prints something if there was a failed request before.

    Parameters
    ----------
    what : name of module
    errflag : used to identify if there was an error before
    time : time in simulation

    Returns
    -------
    Log messages, an updated errflag

    """
    if (errflag != 0): 
        logger.info('===== PROCEED ==== enough '+str(what)+' is available at '+ str(time))
        errflagnew=0
    if (errflag==0): 
        errflagnew=errflag        
    return(errflagnew)



def get_decimal_year():
    """
    get decimal year for "now"

    Returns
    -------
    "now" in .6f format
    """
    from datetime import datetime
    now = datetime.now()
    year_start = datetime(now.year, 1, 1)
    year_end = datetime(now.year + 1, 1, 1)
    total_seconds_in_year = (year_end - year_start).total_seconds()
    elapsed_seconds = (now - year_start).total_seconds()
    decimal_year = now.year + elapsed_seconds / total_seconds_in_year
    return "{:.6f}".format(decimal_year)


def check_if_first_chunk_inbuffer(chunkid,useXchunks,totalchunks,overlapchunks):
    """
    check if a chunk is the first chunk AFTER overlap region in a (DDBC) buffer

    Parameters
    ----------
    chunkid : int
        chunk ID 
    useXchunks : int
        how many chunks are in one buffer
    totalchunks : int
        number of total chunks in an observation
    overlapchunks : int
        number of chunks that are in the overlap region (i.e., in two buffers)

    Returns
    -------
    False : if first chunk in observation
            if not the first chunk in region after overlap in buffers 2+x
    True : if first chunk after overlap region in buffer 2+x       

    """
    n_c= int(np.ceil( (totalchunks-useXchunks)/(useXchunks-overlapchunks) ))
    first_of_chunks_inbuffer = False
    if (chunkid > useXchunks) & (overlapchunks < useXchunks):
        # this will be skipped for the very first chunk of an observation
        for i in range(1,n_c+1):
            if (chunkid == (useXchunks + ( (i-1)*(useXchunks - overlapchunks)) +1)):
                first_of_chunks_inbuffer = True
    return(first_of_chunks_inbuffer)
    
    


###############################################################################

class Observation:
    """
    Class Observation defines one observation in one beam
    (with observation ID)
    
    """
    
    def __init__(self,obs_id_in):
        self.obs_id=obs_id_in
#        print('obs {}'.format(self.obs_id))
           
class Chunk:
    """
    Class Chunk represents one data chunk of an observation.
    
    A data chunk defines the smallest data unit that is read from 
    a buffer in Panda/Cheetah. 
    Currently, it is represented by the memory it requires and 
    the time it covers in the observation. 
    
    A data chunk actually consist of several data packages 
    whose definition include parameters such as number of time samples,
    bit per sample, number of frequency channels (represented, for instance, by "number_of_channels"
    in the Cheetah config .xml file > beam > source>). 
    The realistic parametrisation of a data chunk still needs to be implemented.    
    """
    
    def __init__(self,obs_id_in,chunk_id_in):
        self.obs_id=obs_id_in
        self.chunk_id=chunk_id_in
#        logger.info('--- obs {}'.format(self.obs_id) +
#                    ' chunk {}'.format(self.chunk_id)+ ' created')


    


###############################################################################
#       output/monitor data frame handling
###############################################################################

class out_monitor(object):
    """
    A class to setup, read, and modify two output data frames.
    It is used for coordination within the simulation and 
    as the simulation monitoring output. 
    
    for the intermediate data, this class can also release cpu_ram resource
    """
    # Note:  requesting [simpy] resources would require a different call structure 
    #        because this requires yield and env.process() setup, 
    #        returning a resource is OK
    
    def __init__(self,env):
        """
        Here, the first row of the data frames are set up. 

        Parameters
        ----------
        env : a simpy environment

        Returns
        -------
        Updated output data frames.
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation
        inter_data : a pandas DataFrame which monitors intermediate data and buffers

        """
        self.env = env
        self.out_data=pd.DataFrame({'obs':[0],     #obsID
                               'chunk':[0],   #chunk ID
                               'time_rcpt':[0],  #time when chunk has been read in 
                               'time_ddbc':[0],
                               'time_rfim':[0],  
                               'time_ddtr':[0],
                               'time_spdt':[0],
                               'time_spss':[0], #spssift
                               'time_spsc':[0], #SPS_clustering
                               'time_cdosS':[0], #CDOS sps
                               'time_ffbc':[0],  
                               #time when [later modified] chunk has been collected for obsID
                               'time_cdosA':[0],
                               #time when [modified] obsID is written out
                               })
#        print(self.out_data)

        # time vs resource use
        self.out_resources=pd.DataFrame({'time_now':[0],
                                    'obs':[0],     #obsID
                                    'chunk':[0],   #chunk ID
                                    'module_caller':['None'],
                                    'cpu_ram_free':[self.env.cpu_ram.level],
                                    'cpu_ram_used':[0],
                                    'DIF_cpu_ram_used':[0],
                                    'cpu_cores_free':[self.env.cpu_cores.level], #is float in dataframe only
                                    'cpu_cores_used':[0],
                                    'gpus':[self.env.gpus.level],
                                    'mem_in_gpu':[self.env.mempergpu.level]
                                    })#
        logger.info(self.out_resources)

        
        # intermediate data: buffers and newly created data 
        self.inter_data=pd.DataFrame({'obs':[0],     #obsID
                                      #'chunk':[0],   #chunk ID
                                      # Name part 1: name of buffer (B_...) or intermediate data (D_...) 
                                      'data_name_p1':['name_p1'],
                                      # Name part 2: ID identifying obs X, chunks A-B: oXcAtoB 
                                      'ID_name_p2':['IDname_p2'],
                                      'time_created':[0],
                                      'time_last_access':[0],
                                      'time_deleted':[0],  
                                      'size_in_ram':[0],
                                      'last_used_by':['None']
                                      })

    def interd_get_list_ID_nameparts(self,obsid,useXchunks,totalchunks):
        n_c= int(np.ceil(totalchunks/useXchunks))
        
        for i in range(1,n_c+1):
            if i < n_c:
                nameIDi = 'o'+str(obsid)+'c'+str(int(((i-1)*useXchunks)+1))+'to'+str(int(i*useXchunks))
            if i == n_c:
                nameIDi = 'o'+str(obsid)+'c'+str(int(((i-1)*useXchunks)+1))+'to'+str(int(totalchunks))
            
            
            if i == 1:
                nameID=pd.DataFrame({'ID':[nameIDi]})
            if i>1 :
                nameID.loc[len(nameID)] = [nameIDi]

        return(nameID)    
        
    def interd_get_list_ID_nameparts_overlap(self,obsid,useXchunks,totalchunks,overlapchunks):
        n_c= int(np.ceil( (totalchunks-useXchunks)/(useXchunks-overlapchunks) ))
        
        for i in range(1,n_c+1):
             
            
            if ((i <= n_c) & (i>1)):
 #               nameIDi = 'o'+str(obsid)+'c'+str(int( ((i-1)*useXchunks)+1 - (i-1)*overlapchunks)) +'to'+str(int(i*useXchunks  - (i-1)*overlapchunks))
                nameIDi = 'o'+str(obsid)+'c'+str(int( useXchunks+(i-1)*(useXchunks-overlapchunks) -overlapchunks +1)) +'to'+str(int(useXchunks + (i) * (useXchunks-overlapchunks)))
                if ( (useXchunks + (i) * (useXchunks-overlapchunks)) >= totalchunks):
                    nameIDi = 'o'+str(obsid)+'c'+str(int( useXchunks+(i-1)*(useXchunks-overlapchunks) -overlapchunks +1)) +'to'+str(int(totalchunks))
    
            if i == 1:
                nameIDi= 'o'+str(obsid)+'c1'+'to'+str(int(useXchunks))
                nameID=pd.DataFrame({'ID':[nameIDi]})
            if i>1 :
                nameID.loc[len(nameID)] = [nameIDi]

        return(nameID)    


    
    def interd_get_ID_namepart2(self,obsid,chunkid,useXchunks,totalchunks):
        """
        get the ID to which a data chunk belongs (for intermediate data)

        Parameters
        ----------
        obsid :  obsid
        chunkid : chunkid
        useXchunks : how many chunks are used together
        totalchunks : totalnumber of chunks in observation

        Returns
        -------
        nameID: a string wiht oXcAtoB (and chunkid is in interval [A,B] )

        """
        n_c= int(np.ceil(totalchunks/useXchunks))
        nameID='None'
        first_of_chunks = False
        last_of_chunks = False
        for i in range(1,n_c+1):
            if (chunkid == (((i-1)*useXchunks)+1)):
                first_of_chunks = True

            if ((chunkid == totalchunks) or (chunkid == (i*useXchunks) )):
                last_of_chunks = True
            
            if (((i-1)*useXchunks < chunkid) and (chunkid <= i * useXchunks)):
                nameID = 'o'+str(obsid)+'c'+str(int(((i-1)*useXchunks)+1))+'to'+str(int(i*useXchunks))
                if ((chunkid == totalchunks) or (i*useXchunks > totalchunks)):
                    nameID = 'o'+str(obsid)+'c'+str(int(((i-1)*useXchunks)+1))+'to'+str(int(totalchunks))
                    
        return(nameID,first_of_chunks,last_of_chunks)    


    def interd_get_ID_namepart2_overlap(self,obsid,chunkid,useXchunks,totalchunks,overlapchunks):
        """
        get the ID to which a data chunk belongs (for intermediate data), 
        account for overlap region at beginning in retrospect (same chunks used twice because of overlap)

        Parameters
        ----------
        obsid :  obsid
        chunkid : chunkid
        useXchunks : how many chunks are used together
        totalchunks : total number of chunks in observation
        overlapchunks: number of chunks in overlap region (that are already processed) 

        Returns
        -------
        nameID: a string wiht oXcAtoB (and chunkid is in interval [A,B] )

        """
        n_c= int(np.ceil( (totalchunks-useXchunks)/(useXchunks-overlapchunks) ))
        nameID='None'
        first_of_chunks = False
        last_of_chunks = False
        
        # only for the first chunks 
        if ((chunkid <= useXchunks) ):
            nameID = 'o'+str(obsid)+'c1'+'to'+str(int(useXchunks))
#            print(obsid,chunkid, nameID)
            
            if ((useXchunks > totalchunks)):
                    nameID = 'o'+str(obsid)+'c1'+'to'+str(int(totalchunks))
            
            if (chunkid == 1):
                first_of_chunks = True

#            if ((chunkid == totalchunks) or (chunkid == (useXchunks) )):
            if ((chunkid == totalchunks) or (chunkid == (useXchunks) )):
                last_of_chunks = True
            
            
        # if there is overlap region (already read in) account for those in the new name 
        if (chunkid > useXchunks) & (overlapchunks < useXchunks):
            for i in range(1,n_c+1):
                
                if (chunkid == (useXchunks + ( (i-1)*(useXchunks - overlapchunks)) +1)):
                    first_of_chunks = True

                if ((chunkid == totalchunks) or 
                    (chunkid == (useXchunks+ (i)*(useXchunks - overlapchunks) ))):
                    last_of_chunks = True
            
                if (( (useXchunks+(i-1)*(useXchunks-overlapchunks) ) < chunkid) and 
                    (chunkid <= ( useXchunks + (i) * (useXchunks-overlapchunks) )) and
                    (chunkid <= totalchunks)):
                    # the name includes the preceding chunks that are in the overlap region
                    nameID = 'o'+str(obsid)+'c'+str(int( useXchunks+(i-1)*(useXchunks-overlapchunks) -overlapchunks +1)) +'to'+str(int(useXchunks + (i) * (useXchunks-overlapchunks)))
                    #print('x',obsid,chunkid, nameID)

                    if ((chunkid == totalchunks) or ((useXchunks + (i) * (useXchunks-overlapchunks)) > totalchunks)):
                        nameID = 'o'+str(obsid)+'c'+str(int( useXchunks+(i-1)*(useXchunks-overlapchunks)-overlapchunks +1))+'to'+str(int(totalchunks))
                        #print('y',obsid,chunkid, nameID)
                    
#                    print(i,obsid,chunkid, nameID)

        return(nameID,first_of_chunks,last_of_chunks)    




    def interd_existornot(self,obsid,namep1,idname):
        """
        row_exists,sizevalue,last_module = interd_existornot(obsid,namep1,idname)
        
        checks if a row with the requested ID already exists
        if yes, returns what size does it have and which module used it last

        Parameters
        ----------
        obsid : obsID
        namep1 : name part 1, e.g. B_DDBC...
        idname : name part 2,e.g. 'oXcAtoB''

        Returns
        -------
        row_exists : Boolean
        sizevalue : if yes, size in inter_data
        last_module : if yes, the name of then last using module (entry of 'last_used_by')

        """
        maskc= (self.inter_data.obs == obsid) & (self.inter_data.data_name_p1 == namep1) & (self.inter_data.ID_name_p2 == idname)
        test=self.inter_data.loc[maskc]
        if (len(test) == 0) : 
            row_exists = False
            sizevalue= 0
            last_module = 'None'
            
        if (len(test) != 0) :
            row_exists = True
            sizedf=self.inter_data['size_in_ram'].loc[maskc]
            sizedf.reset_index(inplace=True,drop=True)
            sizevalue=sizedf[0]
            
            mod_df =self.inter_data['last_used_by'].loc[maskc]
            mod_df.reset_index(inplace=True,drop=True)
            last_module = mod_df[0]
        
        return(row_exists,sizevalue,last_module)
    
         
    def interd_create_or_access_data(self,obsid,namep1,idname,first_of_chunks,modulename,size_to_add):
        """
        The data frame self.inter_data is modified.
        
        Parameters
        ----------
        obsid : obsID
        namep1 : name part 1, e.g. B_DDBC...
        idname : name part 2,e.g. 'oXcAtoB''
        first_of_chunks : Boolean (if chunkID = A then True)
        last_of_chunks 
        modulename : accessing module's name'
        size_to_add : add size to existing size_in_ram value; 
                      (if we reduce candidate lists later on, we  need negative sizes)  
                      
    
        Returns
        -------
        old_s : old size value
        new_s : new size value
        
        Either a new line in self.inter_data, or it modifies an existing one [except removal of data]
        Note: an existing line with the same obsid, namep1, idname will be modified
        """
        
        old_sizevalue = 0
        new_size = 0
        
        mask= (self.inter_data.obs == obsid) & (self.inter_data.data_name_p1 == namep1) & (self.inter_data.ID_name_p2 == idname)
        test=self.inter_data.loc[mask]

        if (len(test) == 0) : 
            self.inter_data.loc[len(self.inter_data)]= [obsid,
                                                        namep1,
                                                        idname,
                                                        round(self.env.now),
                                                        round(self.env.now),
                                                         -1,
                                                        size_to_add,
                                                        modulename]
            if (size_to_add >= 0):
                new_size = size_to_add
            if (size_to_add < 0):
                new_size = 0  # initial size cannot be negative 


        if (len(test) != 0) :
            old_sizedf=self.inter_data['size_in_ram'].loc[mask]
            old_sizedf.reset_index(inplace=True,drop=True)
            old_sizevalue=old_sizedf[0]

            if (size_to_add >= 0):
                new_size = size_to_add + old_sizevalue

            if ((size_to_add < 0) and (np.abs(size_to_add) <= old_sizevalue)):
                new_size = size_to_add + old_sizevalue

            if ((size_to_add < 0) and (np.abs(size_to_add) > old_sizevalue)):
                
                new_size =0  #  size cannot be negative 

            
            
            self.inter_data.loc[mask,'size_in_ram']=new_size
            self.inter_data.loc[mask,'time_last_access'] = round(self.env.now)
            self.inter_data.loc[mask,'last_used_by'] = modulename


#             # request resource for intermediate data has to be done outside
        
        return(old_sizevalue, new_size)     
           
    def interd_delete_data(self,obsid,namep1,idname,last_of_chunks,modulename,size_to_remove,free_RAMresource):
        """
        (1) 'deletes' intermediate data in the inter_data dataFrame, logs times,
        but only if it is the last data chunk in chain-of-chunks that are processed
        
        optional: (2) returns simpy cpu_ram resources of the same size
        BEWARE: For 'normal' processing modules, this is currently done in the pipeline itself

        Parameters
        ----------
        obsid : ObsID
        namep1 : name part 1, e.g. B_DDBC...
        idname : name part 2, e.g. 'oXcAtoB''
        last_of_chunks : Boolean; only if True, something is done
        modulename : caller module
        size_to_remove : 
            if < 0 : all the size_in_ram is set to 0
            This should be the default
            
            if > 0 : the value will be subtracted from current size_in_ram
            BEWARE: Partial subtraction, or oversubtraction can be possible
            This is implemented for possible future use.
                
        free_RAMresource : Boolean, if True, returns cpu_ram with respective value
        Default is False
        BEWARE: Potential for releasing resources twice
        
        Returns
        -------
        old size value, new size value
        a changed inter_data
        

        """
        old_sizevalue = -99
        new_size = -99
        

        if (last_of_chunks == True):
            # this means all the size_in_ram should be 'deleted' (should be default)
            if (size_to_remove < 0):
                mask= (self.inter_data.obs == obsid) & (self.inter_data.data_name_p1 == namep1) & (self.inter_data.ID_name_p2 == idname)
                old_sizedf=self.inter_data['size_in_ram'].loc[mask]
                old_sizedf.reset_index(inplace=True,drop=True)
                old_sizevalue=old_sizedf[0]
                
                putback = old_sizevalue
                new_size =  0 
             
                self.inter_data.loc[mask,'size_in_ram']=new_size
                self.inter_data.loc[mask,'time_last_access'] = round(self.env.now)
                self.inter_data.loc[mask,'time_deleted'] = round(self.env.now)
                self.inter_data.loc[mask,'last_used_by'] = modulename
 
            
            # this allows partial deletion (might be useful for later)
            if (size_to_remove > 0):
                mask= (self.inter_data.obs == obsid) & (self.inter_data.data_name_p1 == namep1) & (self.inter_data.ID_name_p2 == idname)
                old_sizedf=self.inter_data['size_in_ram'].loc[mask]
                old_sizedf.reset_index(inplace=True,drop=True)
                old_sizevalue=old_sizedf[0]

                new_size =  old_sizevalue - size_to_remove 
                putback = size_to_remove 
                if (new_size < 0):
                    errmes='MORE CPU RAM resources returned that exist in inter_data --' + modulename
                    logger.error(errmes)
                    

            
                self.inter_data.loc[mask,'size_in_ram']=new_size
                self.inter_data.loc[mask,'time_last_access'] = round(self.env.now)
                self.inter_data.loc[mask,'time_deleted'] = round(self.env.now)
                self.inter_data.loc[mask,'last_used_by'] = modulename
                
            
            if (free_RAMresource == True):
#                print('---1----',self.env.cpu_ram.level,self.env.now,putback,namep1)
                #puts amount into the container. The event is triggered as soon as there’s enough space in the container.
                self.env.cpu_ram.put(float(putback))
                
#                print('---2----',self.env.cpu_ram.level,self.env.now)
     #           retmes='CPU RAM resource returned : '+ str(putback)+' at sim time '+str(self.env.now)+' by '+modulename
     #           logger.info(retmes)
                
        return(old_sizevalue,new_size)
        

    def init_od_newrow(self,obsid,chunkid):
        """
        initialize each row in output data frame

        Parameters
        ----------
        obsid : ID of observation
        chunkid : ID of data chunk
        
        Returns
        -------
        Add a new row with 0 time values in out_data

        """
        # initialize each row in output data frame
        self.out_data.loc[len(self.out_data)]= [obsid,
                                                chunkid,
                                                0,0,0,0,0,
                                                0,0,0,0,0]

        
    def mask_outd_obs_chunk_flag_setval(self,obsid,chunkid,column:str,colvalue):
        """
        sets a value in out_data

        Parameters
        ----------
        obsid : ID of observation  [defines row]
        chunkid : ID of data chunk [defines row]
        column : column name to be modified 
        colvalue : values to be put 

        Returns
        -------
        a modified out_data

        """
        
        list_of_columns = ['time_rcpt','time_ddbc','time_rfim','time_ffbc',
                           'time_cdosA',
                           'time_ddtr','time_spdt','time_spss','time_spsc',
                           'time_cdosS']
        flagC = any(x == column for x in list_of_columns)
        
        if flagC :
            mask= (self.out_data.obs == obsid) & (self.out_data.chunk == chunkid)
#            self.out_data[column].loc[mask]=colvalue  # note for futur pandas update issue
            self.out_data.loc[mask,column]=colvalue
#            print(self.out_data)

        else:
            logger.error('=== mask_obsdata_chunk_flag_setval: Column name does not exist.')
            
    def outres_obs_chunk_setvals(self,obsid,chunkid,caller):
        """
        creates an entry in out_resources

        Parameters
        ----------
        obsid : ID of observation
        chunkid : ID of data chunk
        caller : Name of calling module
            
        Returns
        -------
        A new entry in out_resources with time, obsid,chunkid, and resource availability 

        """
        
        cpuramatzero = self.out_resources.iloc[0]['cpu_ram_free']
        cpuramat_minus1 = self.out_resources.iloc[len(self.out_resources)-1]['cpu_ram_free']
        cpucoresatzero = self.out_resources.iloc[0]['cpu_cores_free']
        
        
        self.out_resources.loc[len(self.out_resources)]= [round(self.env.now),
                                                          obsid,
                                                          chunkid,
                                                          caller,
                                                          self.env.cpu_ram.level,
                                                          cpuramatzero-self.env.cpu_ram.level,
                                                          -cpuramat_minus1+self.env.cpu_ram.level,
                                                          self.env.cpu_cores.level,
                                                          cpucoresatzero-self.env.cpu_cores.level,
                                                          self.env.gpus.level,
                                                          self.env.mempergpu.level
                                                          ]
            
    def mask_outd_obs_chunk_flag_getval(self,obsid,chunkid,column:str):
        """
        query for the value in a column

        Parameters
        ----------
        obsid : ID of observation  [defines row]
        chunkid : ID of data chunk [defines row]
        column : column name to be queried 

        Returns
        -------
        value in column for obsid,chunkid
        [this should be only one, in any case, only the first value of an array is returned]

        """
        list_of_columns = ['time_rcpt','time_ddbc','time_rfim','time_ffbc',
                           'time_cdosA',
                           'time_ddtr','time_spdt','time_spss','time_spsc',
                           'time_cdosS']
        flagC = any(x == column for x in list_of_columns)
        
        if flagC :   
            mask= (self.out_data.obs == obsid) & (self.out_data.chunk == chunkid)
            colvaluedf=self.out_data[column].loc[mask]
            colvaluedf.reset_index(inplace=True,drop=True)
            colvalue=colvaluedf[0]

#            print(self.out_data)
            return(colvalue)

        else:
            logger.error('=== mask_obsdata_chunk_flag_setval: Column name does not exist.')
            return(331)



###############################################################################
#       module setup routines
###############################################################################
    
def rcpt_setup(g,df_cheetah_in, dfcheetahflags_in, df_POM_parT_in):
    """
    This sets up the receptor module.
    This function will change once the respective Cheetah config parameters are available.
    
    At the moment, the required memory per RCPT-produced data chunk is hard coded 
    and the assumption of keeping just Stokes-I is made. 
    
    
    Parameters
    ----------
    g : global class with parameters [for future: consider to modify]
    
    df_cheetah_in : cheetah config parameters
    dfcheetahflags_in : Cheetah run flags
    df_POM_parT_in : POM config 

    Returns
    -------
    runtime : how long the RCPT will be active for one chunk (basically the observing time going into one chunk)
    use_resource_name : CPU is used a resource
    use_resource_num : number of threads
    use_resource_mem : required processing memory + memory for data
    return_resource_num : released number of threads after RCPT finished
    return_resource_mem : released memory after RCPT finished
    gmod : a  modified global class [not used at the moment]

    """
    gmod=g
    
    # time of RCP module to process one chunk of data    
    # 
    runtime = df_POM_parT_in.t_RCP_i.iloc[0] + g.chunk_buffer_length

    #must be CPU
    use_resource_name = 'CPU'
    use_resource_num = df_POM_parT_in.cpu_RCPT.iloc[0]

    # 128 samples, each 64 mus
    # 8 bit, 4 polarisations
    # 4096 frequency channels
    # about 20 000 samples per s  [64mus time resolution : 15625 samples/s]
    
#    memperchunk = 128.0 [samples] * 8.0 * 4* 4096   #in bits
    # currently only Stokes-I
 #   memperchunk = 128.0* 8.0 * 4096
    memperchunk_inbyte = 128.0 * 4096



#    use_resource_mem = df_POM_parT_in.M_RCPT.iloc[0] + df_POM_parT_in.D_RCPT.iloc[0]
    use_resource_mem = df_POM_parT_in.M_RCPT.iloc[0] + memperchunk_inbyte

    return_resource_num =df_POM_parT_in.cpu_RCPT.iloc[0]
    
    return_resource_mem = df_POM_parT_in.M_RCPT.iloc[0]
    
#                               'NO_scan_scan_time':            [NO_scan_scan_time],
#                               'NO_scan_sub_array_id':         [NO_scan_sub_array_id],
#                               'NO_scan_time_resolution':      [NO_scan_time_resolution],
#                               'NO_scan_time_samples':         [NO_scan_time_samples],
 
    
   
    
    return(runtime, use_resource_name,use_resource_num,
           use_resource_mem,return_resource_num,return_resource_mem,gmod)



# ###############################################################################
class release_resource(object):
    """
    This is a helper class to return resources in an easy way.
    It should only be used if the the other classes' resource handling 
    is insufficient (e.g. return of only part of the resources).
    The runtime  is assumed to be 0.
    """
    def __init__(self,
                  env,
                  g,
                  name:str,
                  use_resource_name:str,
                  return_resource_num:int,
                  return_resource_mem:int,
                  obsid:int,
                  chunkid:int,
                  oo,
                  timecol_inoo_check1:str):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        g: the global class with POM parameters    
        name : str
            A short module description (e.g., RCPT_return) to be passed on in output
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
            

        Returns
        -------
        A modified oo (out_resources). 

        """
        self.env = env
        self.name = name
        self.use_resource_name = use_resource_name
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem
#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid
        self.oo = oo
        self.timecolcheck_inoo = timecol_inoo_check1
       

        self.keep_trying = 1
        
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.max_resource_mem = g.available_cpu_ram   
            self.have_resource_num= self.env.cpu_cores
            self.max_resource_num = g.available_cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_num= self.env.gpus
            self.max_resource_num = g.available_gpus   

            self.have_resource_mem= self.env.mempergpu
            self.max_resource_mem = g.available_mem_pergpu



    def condition(self):
        """
        Here used to emphasize type of process " try until all conditions met".
        
        This is the place to add any additional conditions for modules.

        Returns
        -------
        bool
        True: The process is tried to run again.
        False: The process is aborted.

        """
        if (self.keep_trying == 1):     
            return True
        else:
            return False

    def time_condition(self):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.
 
        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                            self.chunkid,
                                                            self.timecolcheck_inoo)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)
    
    
    def release(self,whichevent):
        """
        If condition for start (checked via timecol_inoo_check1 column in output_data)
        is true, and whichevent happened, then 
        return resources and update log and output DataFrames.
        
        If the conditions is not met, try again after 1 time unit.

        Returns
        -------
        updated DataFrame (out_resources) in oo

        """
        
        yield whichevent
#            datastr='/beam '+str(self.beamid)+'/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        errflag=0

        while self.condition():
            # checking resources makes only sense if process can be started
            if (self.time_condition()):
                if ((self.have_resource_num.level + self.return_resource_num) > self.max_resource_num):
                    errmes1a=datastr+'ERROR - tried to return too many resource numbers! have, return, max:'
                    errmes1b=str(self.have_resource_num.level) +',' + str( self.return_resource_num)+','+str(self.max_resource_num)
                    logger.error(errmes1a)
                    logger.error(errmes1b)
                    
                if (((self.have_resource_num.level + self.return_resource_num) <= self.max_resource_num) and 
                      (self.return_resource_num > 0)):
                    self.have_resource_num.put(self.return_resource_num)

                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_NRend')

                    self.keep_trying = 0
 
                
                if ((self.have_resource_mem.level + self.return_resource_mem) > self.max_resource_mem):
                    errmes2a=datastr+'ERROR - tried to return too many resource numbers! have, return, max:'
                    errmes2b=str(self.have_resource_mem.level) +',' + str( self.return_resource_mem)+','+str(self.max_resource_mem)
                    logger.error(errmes2a)
                    logger.error(errmes2b)
                    
                    
                if (((self.have_resource_mem.level + self.return_resource_mem) <= self.max_resource_mem) and
                     (self.return_resource_mem >0)) :
                    self.have_resource_mem.put(self.return_resource_mem)
 
                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_MRend')

                    self.keep_trying = 0
                    

            if (self.time_condition() == False):
                yield self.env.timeout(1)

# ###############################################################################

class proc_module(object):
    """
    This is a general processing class that keeps trying to start (and run) a process 
    even if starting conditions are not fulfilled at the beginning. 
    
    """
    def __init__(self,
                  env,
                  name:str,
                  runtime:int,
                  use_resource_name:str,
                  use_resource_num:int,
                  use_resource_mem:int,
                  return_resource_num:int,
                  return_resource_mem:int,
#                 beamid,
                  obsid:int,
                  chunkid:int,
                  oo,
                  timecol_inoo_check1:str,
                  timecol_inoo:str):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
            DESCRIPTION.
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
            

        Returns
        -------
        A modified oo (out_data, out_resources). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid

        self.oo = oo
        self.timecolcheck_inoo = timecol_inoo_check1
        self.timecol_inoo = timecol_inoo

        
        self.keep_trying = 1
        
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus

    def condition(self):
        """
        Here used to emphasize type of process " try until all conditions met".
        
        This is the place to add any additional conditions for modules.

        Returns
        -------
        bool
        True: The process is tried to run again.
        False: The process is aborted.

        """
        if (self.keep_trying == 1):     
            return True
        else:
            return False

    def time_condition(self):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.
 
        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                            self.chunkid,
                                                            self.timecolcheck_inoo)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)

    def resource_conditionF(self,errflag):
        """
        Checks if resources are available and returns Error messages if needed.

        Parameters
        ----------
        errflag : type of error as a number.
                  It is used to know whether there was an error in a previous try.

        Returns
        -------
        Boolean:
        True  (resources available)  
        False (at least one resource is not available)

        """
        if (self.have_resource_mem.level < self.use_resource_mem): 
            print_memerror(str(self.name),self.use_resource_mem,self.have_resource_mem.level,self.env.now)
            errflag=1
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                              self.chunkid,
                                              self.name+'_MEM_ERROR')

        #           check if enough CPUs/Threads or GPU units are available                
        if (self.have_resource_num.level < self.use_resource_num): 
            if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
                print_cpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=2
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_CPUnum_ERROR')
                
            if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
                print_gpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=3
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_GPUnum_ERROR')

        
        resource_condition_h =((self.have_resource_mem.level >= self.use_resource_mem) 
                              & (self.have_resource_num.level >= self.use_resource_num)) 
        
        return(resource_condition_h,errflag)

    
    
    def run(self):
        """
        If condition for start (checked via timecol_inoo_check1 column in output_data)
        is true, and if enough resources are available, wait for processing time
        (runtime), and update log and output DataFrames.
        
        If any of these conditions is not met, try again after 1 time unit.

        Returns
        -------
        updated DataFrames (out_data, out_resources) in oo

        """
#            datastr='/beam '+str(self.beamid)+'/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        errflag=0
        resource_condition = False # start value, updated if time condition is true

        while self.condition():
            # checking resources makes only sense if process can be started
            if (self.time_condition()):

            
#            logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [A] '+str(self.env.now))       
# 
 
                # check if enough resources are available, if not -> error messages + advance time    
                resource_condition, errflag = self.resource_conditionF(errflag)
            
            
                if (resource_condition and self.time_condition()):
                    start_module=self.env.now
#                    logger.info(datastr+': Module '+str(self.name)+' started at time : ' + str(start_module))

#                   ----- request resources
#                   *proceeds only when* enough memory  are available
                    M_module= yield self.have_resource_mem.get(self.use_resource_mem)
                    print_backtonormal(str(self.name)+'---processM RAM',errflag,self.env.now)

#                   *proceeds only when* enough CPUs/Threads or GPU units  are available
                    Ncpugpu_module= yield self.have_resource_num.get(self.use_resource_num)
                    print_backtonormal(str(self.name)+'---Processor/thread numbers',errflag,self.env.now)

                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_start')

                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        -1)


#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [B] '+str(self.env.now))       

                    #---- "processing" time
                    yield self.env.timeout(self.runtime)
            
                    end_module=self.env.now
 
    
#                    logger.info(datastr+': Module '+str(self.name)+' ended at time : ' + str(end_module))

    #              ----- return resources
                    self.have_resource_num.put(self.return_resource_num)
                    self.have_resource_mem.put(self.return_resource_mem)
 
                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                  self.chunkid,
                                                  self.name+'_end')

                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        round(self.env.now))

        
#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [C] '+str(self.env.now))       
                    self.keep_trying = 0
                if ((resource_condition == False) ):
                    #try condition again after 1 time unit
                    yield self.env.timeout(1)
            if (self.time_condition() == False):
                yield self.env.timeout(1)

###############################################################################

class proc_module_useevent(object):
    """
    This is a general processing class that keeps trying to start (and run) a process 
    even if resource starting conditions are not fulfilled at the beginning. 
    In contrast to proc_module, this class first waits for a successful event 
    (a previous process) before it even starts to query the resources
    
    """
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo_check1:str,
                 timecol_inoo:str):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
            DESCRIPTION.
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
            

        Returns
        -------
        A modified oo (out_data, out_resources). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid

        self.oo = oo
        self.timecolcheck_inoo = timecol_inoo_check1
        self.timecol_inoo = timecol_inoo

        
        self.keep_trying = 1
        
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus

    def condition(self):
        """
        Here used to emphasize type of process " try until all conditions met".
        
        This is the place to add any additional conditions for modules.

        Returns
        -------
        bool
        True: The process is tried to run again.
        False: The process is aborted.

        """
        if (self.keep_trying == 1):     
            return True
        else:
            return False

    def time_condition(self):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.
 
        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.chunkid,
                                                           self.timecolcheck_inoo)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)

    def resource_conditionF(self,errflag):
        """
        Checks if resources are available and returns Error messages if needed.

        Parameters
        ----------
        errflag : type of error as a number.
                  It is used to know whether there was an error in a previous try.

        Returns
        -------
        Boolean:
        True  (resources available)  
        False (at least one resource is not available)

        """
        if (self.have_resource_mem.level < self.use_resource_mem): 
            print_memerror(str(self.name),self.use_resource_mem,self.have_resource_mem.level,self.env.now)
            errflag=1
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+'_MEM_ERROR')

        #           check if enough CPUs/Threads or GPU units are available                
        if (self.have_resource_num.level < self.use_resource_num): 
            if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
                print_cpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=2
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_CPUnum_ERROR')
                
            if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
                print_gpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=3
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_GPUnum_ERROR')

        
        resource_condition_h = ((self.have_resource_mem.level >= self.use_resource_mem) 
                              & (self.have_resource_num.level >= self.use_resource_num))
        
        return(resource_condition_h,errflag)

    
    
    def run(self,whichevent):
        """
        If condition for start (checked via timecol_inoo_check1 column in output_data)
        is true, and if enough resources are available, wait for processing time
        (runtime), and update log and output DataFrames.
        
        If any of these conditions is not met, try again after 1 time unit.

        Returns
        -------
        updated DataFrames (out_data, out_resources) in oo

        """

        yield whichevent
#            datastr='/beam '+str(self.beamid)+'/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        errflag=0
        resource_condition = False # start value, updated if time condition is true

        while self.condition():
            # checking resources makes only sense if process can be started
            if (self.time_condition()):

            
#            logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [A] '+str(self.env.now))       
# 
 
                # check if enough resources are available, if not -> error messages + advance time    
                resource_condition, errflag = self.resource_conditionF(errflag)
            
            
                if (resource_condition and self.time_condition()):
                    start_module=self.env.now
#                    logger.info(datastr+': Module '+str(self.name)+' started at time : ' + str(start_module))

#                   ----- request resources
#                   *proceeds only when* enough memory  are available
                    M_module= yield self.have_resource_mem.get(self.use_resource_mem)
                    print_backtonormal(str(self.name)+'---processM RAM',errflag,self.env.now)

#                   *proceeds only when* enough CPUs/Threads or GPU units  are available
                    Ncpugpu_module= yield self.have_resource_num.get(self.use_resource_num)
                    print_backtonormal(str(self.name)+'---Processor/thread numbers',errflag,self.env.now)

                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_start')

                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        -1)


#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [B] '+str(self.env.now))       

                    #---- "processing" time
                    yield self.env.timeout(self.runtime)
            
                    end_module=self.env.now
 
    
#                    logger.info(datastr+': Module '+str(self.name)+' ended at time : ' + str(end_module))

    #              ----- return resources
                    self.have_resource_num.put(self.return_resource_num)
                    self.have_resource_mem.put(self.return_resource_mem)
 
                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_end')

                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        round(self.env.now))

        
#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [C] '+str(self.env.now))       
                    self.keep_trying = 0
                if ((resource_condition == False) ):
                    #try condition again after 1 time unit
                    yield self.env.timeout(1)
            if (self.time_condition() == False):
                yield self.env.timeout(1)

###############################################################################

class Proc_Module_Useevent_Idata(object):
    """
    This is a general processing class that keeps trying to start (and run) a process 
    even if resource starting conditions are not fulfilled at the beginning. 
    In contrast to proc_module, this class first waits for a successful event 
    (a previous process) before it even starts to query the resources
    
    this class (in contrast to proc_module_useevent) can handle intermediate data
    that is kept track of in the pandas dataframe oo.inter_data. 
    A row in that data frame .inter_data is created if not existent, otherwise it is modified.
    For new intermediate data, resources are requested.
    Resources can be also released if the intermediate data (from previous modules) 
    are no longer needed.
    
    """
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo_check1 : str,
                 timecol_inoo : str,
                 with_intermediate_data : str,
                 num_chunks_for_intermediate_data: int,
                 total_num_chunks : int,
                 req_ID1_inoointer: str,
                 req_last_used_by: str,
                 req_change_used_by:str,
                 mod_ID1_inoointer: str,
                 mod_size_interdata_perchunk: int,
                 mod_name_extra : str,
                 del_ID1_inoointer: str,
                 del_size_interdata_total: int,
                 del_resourceflag: bool
                 ):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
        [ One would expect: use_resource_mem = return_resource_mem, return_resource_num = use_resource_num]    
        [ However, there may be need for flexibility in future ]
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
        with_intermediate_data : 
            what should be done regarding intermediate data. The following options are possible
            [R] requires [old] intermediate data to proceed
            [M] creates or modifies intermediate data
            [D] deletes intermediate data
            [RM] is [R] and [M]
            [RD] is [R] and [D]
            [RMD] is [R],[M] and [D]
            [MD] is [M] and [D]
            all variations: ['R','r','M','m','D','d','RM','MR','rm','mr',
                          'RD','DR','rd','dr','MD','DM','md','dm',
                          'RMD','MDR','DRM','RDM','MRD','DMR',
                          'rmd','mdr','drm','rdm','mrd','dmr']
        num_chunks_for_intermediate_data : int
            how many data chunks are needed for processing 
            [current processing: one after the other]             
            to produce the intermediate data;
            R,M,D consider the same <=chunkID of obsid
            [currently manually set in global class, later from config]
        total_num_chunks
            total number of chunks in observation
        req_ID1_inoointer: str
            ID_part1 name of intermediate data that are >required< for this module
        req_last_used_by: str
            for the required intermediate data, there is also a required module/processing stage
            of the last access (e.g., 'DDBC_done')
        req_change_used_by: str
            name to which req_last_used_by is now changed, if 'None', nothing is replaced 
            which means there is no track record of the last access 
        mod_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >modified< (e.g., D_rfim)
        mod_size_interdata_perchunk: int,
            size in cpu_ram that is used/to be reserved in the intermediate data
            this size is requested and reserved from cpu_ram reserve
        mod_name_extra : str
            extra ID part to indicate what data (e.g., msk, cl)
            
            Note: Make sure, this name+done appears in the list of  
                  the >check_done_withorder< method of the parent class
            
        del_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >deleted<
            (size will be reduced in inter_data)
        del_size_interdata_total: int
            if < 0 : all the size_in_ram is set to 0
            This should be the default
            
            if > 0 : the value will be subtracted from current size_in_ram
            BEWARE: Partial subtraction, or oversubtraction can be possible
            This is implemented for possible future use.            
        del_resourceflag: bool
              Boolean, if True, returns cpu_ram with respective value
              Default is False  
              BEWARE: Potential for releasing resources twice

        Returns
        -------
        A modified oo (out_data, out_resources,inter_data). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid

        self.oo = oo
        self.timecolcheck_inoo = timecol_inoo_check1
        self.timecol_inoo = timecol_inoo

        
        self.keep_trying = 1
        
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus
        
       
        self.nchunks_idata = num_chunks_for_intermediate_data
        self.reqID1 =        req_ID1_inoointer
        self.req_last_used_by = req_last_used_by
        self.req_change_used_by =req_change_used_by
        self.modID1 =         mod_ID1_inoointer
        self.mod_size_idata =  mod_size_interdata_perchunk
        self.mod_name_extra = mod_name_extra
        self.delID1 =          del_ID1_inoointer
        self.del_size_idata =  del_size_interdata_total
        self.del_resourceflag = del_resourceflag
        
        self.total_num_chunks = total_num_chunks
        
        self.dataID='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
            
        self.w_idata = with_intermediate_data 
        w_idata_list=['R','r','M','m','D','d','RM','MR','rm','mr',
                      'RD','DR','rd','dr','MD','DM','md','dm',
                      'RMD','MDR','DRM','RDM','MRD','DMR',
                      'rmd','mdr','drm','rdm','mrd','dmr']
        
        if any(self.w_idata == w_element for w_element in w_idata_list):
            self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p = self.oo.interd_get_ID_namepart2(
                                                                obsid = self.obsid,
                                                                chunkid = self.chunkid,
                                                                useXchunks = self.nchunks_idata,
                                                                totalchunks = self.total_num_chunks)
            

        if all(self.w_idata != w_element for w_element in w_idata_list): 
            self.inchunkid_p  = 'None'
            self.first_of_chunks_p = 'None'
            self.last_of_chunks_p = 'None'
        
        

        
        # keep as comment - useful for tracking purposes
        #print('---',self.name, self.obsid,self.chunkid,self.w_idata,self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p)     

    def check_done_withorder(self,last_used_check_minimum: str, 
                             last_used_check_value:str):
        '''
        checks if last_used_check_value comes before or is equal to last_used_check_minimum
        in ordered list of modules_done strings

        Parameters
        ----------
        last_used_check_minimum : str
            This module should have been done 
            (but other modules may have modified the 'last_used_by' column of inter_data since then)
        last_used_check_value : str
            actual value (from 'last_used_by' column of inter_data)

        Returns
        -------
        Boolean
        True, if last_used_check_value comes before/is last_used_check_minimum
        False, otherwise
        
        Note: currently only used in child buffer... class, but not this class, 
              but this may need to change with more implemented processing modules
              [to be checked]
        '''
        
        
        lu_list=['DDBC_done',
                 'RFIM_req','RFIM_msk','RFIM_done','RFIM_mskdone',
                 'DDTR_req','DDTR_done',
                 'SPDT_done',
                 'SPSIFT_done',
                 'SPCLUST_done',
                 'CDOSs_req','CDOSsps_done','CDOSsps_MetaDdone',
                 'PSBC_req','PSBC_done','CXFT_req','CXFT_done',
                 'BRDZ_done','DRED_done','FDAS_done','FDAO_done','SIFT_done',
                 'FFBC_done','DDBC_idata_release','DDBC_idataO_release','DDBC_release',
                 'FLDO_done',
#                 'CDOSs_req','CDOSsps_done','CDOSsps_MetaDdone',
                 'CDOSa_req','CDOSa_done',
                 'CDOSa_listsdone',
                 'FFBC_release']
        lu_idx=0
        lu_min=0
        lu_actual = -1
        
        for lu_element in lu_list:
            
            lu_idx= lu_idx+1
            if (last_used_check_minimum ==  lu_element):
                lu_min = lu_idx
            if (last_used_check_value ==  lu_element):
                lu_actual = lu_idx
        #print('===',lu_min ,lu_actual)
        return(lu_min  <= lu_actual)
            
        
    
    def condition(self):
        """
        Here used to emphasize type of process " try until all conditions met".
        
        This is the place to add any additional conditions for modules.

        Returns
        -------
        bool
        True: The process is tried to run again.
        False: The process is aborted.

        """
        if (self.keep_trying == 1):     
            return True
        else:
            return False
        


    def time_condition(self):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.
 
        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.chunkid,
                                                           self.timecolcheck_inoo)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)

    def resource_conditionF(self,errflag):
        """
        Checks if resources are available and returns Error messages if needed.

        Parameters
        ----------
        errflag : type of error as a number.
                  It is used to know whether there was an error in a previous try.

        Returns
        -------
        Boolean:
        True  (resources available)  
        False (at least one resource is not available)

        """
        #print('Rcond:',self.use_resource_mem , self.mod_size_idata,self.use_resource_mem + self.mod_size_idata )

        
        if ((self.have_resource_mem.level < (self.use_resource_mem + np.abs(self.mod_size_idata))) and (self.mod_size_idata>=0)): 
            print_memerror(str(self.name),self.use_resource_mem,self.have_resource_mem.level,self.env.now)
            errflag=1
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+'_MEM_ERROR')

        if ( (self.mod_size_idata < 0) and (self.have_resource_mem.level < (self.use_resource_mem )) ): 
            print_memerror(str(self.name),self.use_resource_mem,self.have_resource_mem.level,self.env.now)
            errflag=1
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+'_MEM_ERROR')



        #           check if enough CPUs/Threads or GPU units are available                
        if (self.have_resource_num.level < self.use_resource_num): 
            if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
                print_cpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=2
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_CPUnum_ERROR')
                
            if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
                print_gpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=3
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_GPUnum_ERROR')

        
        if (self.mod_size_idata >= 0): 
            resource_condition_h = ((self.have_resource_mem.level >= (self.use_resource_mem + np.abs(self.mod_size_idata))) 
                              & (self.have_resource_num.level >= (self.use_resource_num)))
        
        if (self.mod_size_idata < 0): 
            resource_condition_h = ((self.have_resource_mem.level >= (self.use_resource_mem )) 
                              & (self.have_resource_num.level >= (self.use_resource_num)))

        return(resource_condition_h,errflag)


    
    def work_with_intermediate_data(self,do_what):
        #do_what = 'check_if_exit','new_or_modify','update_accessed_required',
        # 'update_accessed_required','release_intermediate_data_resource_done'
        
        # for log file (if uncommented, see below)
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        
        # set init return values
        intermediate_data_flag= 100
        old_s = -100
        new_s = -100
        
        # do nothing
        #if (self.inchunkid_p == 'None'):
        #    intermediate_data_flag= 50
            
        
      #======================================================================== 
      # note: Here, we currently do NOT check/consider where we are in the num_chunks_for_intermediate_data
      # currently, the size is NOT used [but could be later or alternatively with 'name_done']
        if ((self.inchunkid_p != 'None') and (do_what == 'check_if_exit')):
            
            row_exists,sizevalue,last_module = self.oo.interd_existornot(
                        obsid  = self.obsid,
                        namep1 = self.reqID1,
                        idname = self.inchunkid_p
                        )
            
            if (row_exists == True):
                # a requirement for the last-used-by-module was given and is fullfilled
                if ((self.req_last_used_by != 'None') and (self.req_last_used_by == last_module)):
                    intermediate_data_flag=0
                if ((self.req_last_used_by != 'None') and (self.req_last_used_by != last_module)):
                    intermediate_data_flag=1
                    # useful to find out if a name error was done, but it clogs up the logfile, hence commented out as default
                    #logger.info(datastr+' --- did not find row with '+last_module+' at SIMtime: '+str(self.env.now))

            if (row_exists == False):
                intermediate_data_flag = 50   
                
      #======================================================================== 
        # only  changes 'last_used_by' inter_datat (not size)
        if ((self.inchunkid_p != 'None') and (self.req_change_used_by != 'None') and 
            (do_what == 'update_accessed_required')):
            
            old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.reqID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.req_change_used_by, # the change
                                         size_to_add = 0))
            
            
            intermediate_data_flag=60            
               
       #======================================================================== 
         # only  changes 'last_used_by' inter_datat (not size)
        if ((self.inchunkid_p != 'None') and 
            (self.req_change_used_by != 'None') and 
            (do_what == 'update_accessed_required_done') and (self.last_of_chunks_p == True)):
            
            old_s,new_s=(self.oo.interd_create_or_access_data(
                                          obsid = self.obsid,
                                          namep1 = self.reqID1,
                                          idname = self.inchunkid_p,
                                          first_of_chunks = self.first_of_chunks_p, 
                                          modulename = self.name+'_done', # the change
                                          size_to_add = 0))
             
             
            intermediate_data_flag=70            
           

      #======================================================================== 
        if ((self.inchunkid_p != 'None') and (do_what == 'new_or_modify')):

            indicate_last_chunk = self.name+self.mod_name_extra+'done'
            
            if (self.last_of_chunks_p == False):   
                
                row_exists,sizevalue,last_module = self.oo.interd_existornot(
                        obsid  = self.obsid,
                        namep1 = self.modID1,
                        idname = self.inchunkid_p
                        )
                # the last chunk was already 'processed' [can happen since same-timme processes in simpy not necessarily in order]
                # in such a case we want to keep the entry in last-used-by
                if (row_exists == True) and (last_module == indicate_last_chunk):          
                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = indicate_last_chunk,
                                         size_to_add = (self.mod_size_idata)
#                                         size_to_add = np.abs(self.mod_size_idata)
                                         ) )
                
                
                # entry exists, but the last chunk was not processed
                if (row_exists == True) and (last_module != indicate_last_chunk):          

                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.name+self.mod_name_extra,
                                         # changed in comparison to processing class [because data for buffer already exists]
#                                         size_to_add = np.abs(self.mod_size_idata)
                                         size_to_add = (self.mod_size_idata)
                                         ) )
                #print(self.name+self.mod_name_extra)
                
                #--- no entry yet exists 
                if (row_exists == False) :          

                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.name+self.mod_name_extra,
                                         # changed in comparison to processing class [because data for buffer already exists]
#                                         size_to_add = np.abs(self.mod_size_idata) 
                                         size_to_add = (self.mod_size_idata) 
                                         ) )
                
                
                
            
            if (self.last_of_chunks_p == True):            
#                print('---B new_or_modify', self.name+self.mod_name_extra,self.obsid,self.chunkid)
                old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = indicate_last_chunk,                                         
#                                         size_to_add = np.abs(self.mod_size_idata)
                                         size_to_add = (self.mod_size_idata)
                                         ) )



            
            intermediate_data_flag=10            
        
      #======================================================================== 
        if ((self.inchunkid_p != 'None') and (do_what == 'release_intermediate_data_resource')):
            
            # this modifies the .inter_data data frame and (depending on flag) returns cpu_ram resource
            old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                       namep1 = self.delID1,
                                       idname = self.inchunkid_p ,
                                       last_of_chunks = self.last_of_chunks_p,
                                       modulename = self.name+self.mod_name_extra+'_freeP',
                                       size_to_remove = self.del_size_idata,
                                       free_RAMresource = self.del_resourceflag) 
            
            intermediate_data_flag=20
            
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+'_idata_freeP')
            
        
        return(intermediate_data_flag,old_s,new_s)

    def intermediate_data_condition(self):
        # check if requested intermediate data is available
        wh1_idata_list=['R','RM','r','MR','rm','mr',
                       'RD','DR','rd','dr',
                       'RMD','MDR','DRM','RDM','MRD','DMR',
                       'rmd','mdr','drm','rdm','mrd','dmr']
        # if no intermediate data need to be checked, still need this flag
        if all(self.w_idata != w_element for w_element in wh1_idata_list):
            IDr_flag = 0
        #if a check is requested
        if any(self.w_idata == w_element for w_element in wh1_idata_list):
            IDr_flag,os1,ns1 = self.work_with_intermediate_data('check_if_exit')
        
        ID_conditionh = (IDr_flag == 0) 
        
        return(ID_conditionh)

    def intermediate_data_modify(self):
        # when called, the resources have been successfully requested and the processing is done
        IDr_flag = 100
        
        # check if intermediate data should be modified/created
        wh2_idata_list=['M','m','RM','MR','rm','mr',
                        'MD','DM','md','dm',
                        'RMD','MDR','DRM','RDM','MRD','DMR',
                        'rmd','mdr','drm','rdm','mrd','dmr']
        
        #if intermediate data should be modified
        if any(self.w_idata == w_element for w_element in wh2_idata_list):
            
            IDr_flag,os2,ns2 = self.work_with_intermediate_data('new_or_modify')
                
        if (IDr_flag == 10):
#            mes1a= self.dataID +'/'+str(self.inchunkid_p)+'/'+'D_'+self.name+self.mod_name_extra+' --- was modified in inter_data'
            mes1a= self.dataID +'/'+str(self.inchunkid_p)+'/'+self.modID1+self.mod_name_extra+' --- was modified in inter_data'
#            logger.info(mes1a)
#            mes1b = self.dataID +'/'+str(self.inchunkid_p)+'/'+'D_'+self.name+self.mod_name_extra+' --- size '+str(os2)+' was changed to '+str(ns2)
            mes1b = self.dataID +'/'+str(self.inchunkid_p)+'/'+self.modID1+self.mod_name_extra+' --- size '+str(os2)+' was changed to '+str(ns2)
#            logger.info(mes1b)

        else:
            mes2= self.dataID +'/'+str(self.inchunkid_p)+'/'+'D_'+self.name+self.mod_name_extra+' --- was NOT modified in inter_data'
            logger.warning(mes2)
 
    def intermediate_data_delete(self):
        # when called, it deletes [older] intermediate data
        
        IDr_flag = 100
        
        # check if intermediate data should be deleted/resource released
        wh3_idata_list=['D','d',
                        'RD','DR','rd','dr','MD','DM','md','dm',
                        'RMD','MDR','DRM','RDM','MRD','DMR',
                        'rmd','mdr','drm','rdm','mrd','dmr']
        
        #if intermediate data should be deleted
        if any(self.w_idata == w_element for w_element in wh3_idata_list):
            IDr_flag,os3,ns3 = self.work_with_intermediate_data('release_intermediate_data_resource')
                
            if (IDr_flag == 20):
                mes1a= self.dataID +'/'+str(self.inchunkid_p)+'/'+self.name+self.mod_name_extra+'_freeP'+' --- (some or all) was >deleted< in inter_data'
#                logger.info(mes1a)
                mes1b = self.dataID +'/'+self.name+self.mod_name_extra+'_done'+' --- size '+str(os3)+' was changed to '+str(ns3)
#                logger.info(mes1b)
            
    
            if (self.del_resourceflag == True):
                mes1c = str(self.obsid)+'/'+str(self.inchunkid_p)+'/'+'RESOURCE '+ self.name+self.mod_name_extra+'_done'+' --- size '+str(os3-ns3)+' was released '
#                logger.info(mes1c)
                
                
            else:
                mes2= self.dataID+self.name+self.mod_name_extra+'_freeP'+' --- (some or all) was NOT  >deleted< in inter_data'
                logger.warning(mes2)
 
    def intermediate_data_update_access_for_required(self):
        # check if requested intermediate data is available
        wh1_idata_list=['R','RM','r','MR','rm','mr',
                       'RD','DR','rd','dr',
                       'RMD','MDR','DRM','RDM','MRD','DMR',
                       'rmd','mdr','drm','rdm','mrd','dmr']

        #if a check is requested
        if any(self.w_idata == w_element for w_element in wh1_idata_list):
            # only  changes 'last_used_by' inter_data (not size)
            
            IDr_flag,os4,ns4 = self.work_with_intermediate_data('update_accessed_required')
            if (IDr_flag ==60):
                mes3=self.dataID+'/'+str(self.inchunkid_p)+'/'+self.reqID1+' was modified to > '+self.req_change_used_by+'< at '+str(self.env.now)
#                logger.info(mes3)
            
            # for the last chunk in chain-of-chunks use _done in last_used_by
            if (self.last_of_chunks_p == True):
            # only  changes 'last_used_by' inter_datat (not size)
                IDr_flag,os5,ns5 = self.work_with_intermediate_data('update_accessed_required_done')
                
                if (IDr_flag ==70):
                    mes4=self.dataID+'/'+str(self.inchunkid_p)+'/'+self.reqID1+' was modified to > '+self.name+'_done'+'< at '+str(self.env.now)
#                    logger.info(mes4)
    
    #this allows to release the intermediate data (created by this module) at the end of the processing of e.g. an observation
    def release_intermediate_data_products(self, event_to_check, last_used_check_rb,only_after_total_obs):

            allnameIDs = self.oo.interd_get_list_ID_nameparts(obsid = self.obsid,
                                                              useXchunks = self.nchunks_idata,
                                                              totalchunks = self.total_num_chunks)


            # do only something if this is the last chunk of the data 
            if (only_after_total_obs ==True):
                if (self.chunkid == self.total_num_chunks):
                    total_condition = True
                if (self.chunkid != self.total_num_chunks):
                    total_condition = False

                    
            # do  this if it is called immediately 
            if ((self.last_of_chunks_p == True) and (only_after_total_obs == False) ):  
                
    #        print('called')
    #        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
                yield event_to_check
                
                #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
                rb_keep_trying = 1 
                while (rb_keep_trying == 1):
                    
                    row_exists,sizevalue,last_module = self.oo.interd_existornot(
                                                            obsid  = self.obsid,
                                                            namep1 = self.modID1,
                                                            idname = self.inchunkid_p
                                                            )
                 
                    if (row_exists == False):
                    # this should actually never happen if event_to_check is reasonable
                    # if this error, there is probably a naming issue in passed string variables
                        errmes=self.dataID+' (P)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' does NOT exist in inter_data at '+str(self.env.now)
                        logger.error(errmes)

                
                    if (row_exists == True):
                        #print('///',sizevalue,last_module,last_used_check_rb)
                        
                        was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
                                                                  last_used_check_value = last_module)
                        
                        #print('--------------',was_done_flag,self.modID1,self.inchunkid_p, last_used_check_rb, last_module) 
                        
                    
                        if (was_done_flag == True):
                            
                            # this is done to allow modification of inter_data (since for last_chunk) 
                            # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
                            yield self.env.timeout(0.1)  
                            
    #                        self.oo.outres_obs_chunk_setvals(self.obsid,
    #                                                         self.chunkid,
    #                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

                            old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                                   namep1 = self.modID1,
                                                   idname = self.inchunkid_p ,
                                                   last_of_chunks = self.last_of_chunks_p,
                                                   modulename = self.name+self.mod_name_extra+'release',
                                                   size_to_remove = -1, # set size to 0
                                                   free_RAMresource = True ) #release sizevalue
                            
                            rb_keep_trying = 0 # to stop repeating loop

                            self.oo.outres_obs_chunk_setvals(self.obsid,
                                                             self.chunkid,
                                                             self.name+self.mod_name_extra+'_idata_releaseE')
                            
                            mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' at '+str(self.env.now)         
                            mes2='Intermediate Data from CPU RAM resource was released (from size '+str(old_s)+' to '+ str(new_s)+')'
#                            logger.info(mes2+mes1)
                            
                        
                        if (was_done_flag == False):
                            yield self.env.timeout(1)
                    
   #----------------------------------------------------------------------------  
            # do  this for all previous parts of the observation
            if ((self.last_of_chunks_p == True) and (total_condition == True) ):  
                
    #        print('called')
    #        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
                yield event_to_check
                for data_partID in allnameIDs['ID']:
                
                #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
                    rb_keep_trying = 1 
                    while (rb_keep_trying == 1):
                    
                        row_exists,sizevalue,last_module = self.oo.interd_existornot(
                                                            obsid  = self.obsid,
                                                            namep1 = self.modID1,
                                                            idname = data_partID
                                                            )
                 
                        if (row_exists == False):
                    # this should actually never happen if event_to_check is reasonable
                    # if this error, there is probably a naming issue in passed string variables
                            errmes=self.dataID+' (P)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(data_partID)+' does NOT exist in inter_data at '+str(self.env.now)
                            logger.error(errmes)

                
                        if (row_exists == True):
                        #print('///',sizevalue,last_module,last_used_check_rb)
                        
                            was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
                                                                      last_used_check_value = last_module)
                        
                        #print('--------------',was_done_flag,self.modID1,self.inchunkid_p, last_used_check_rb, last_module) 
                        
                    
                            if (was_done_flag == True):
                            
                            # this is done to allow modification of inter_data (since for last_chunk) 
                            # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
                                yield self.env.timeout(0.1)  
                            
    #                        self.oo.outres_obs_chunk_setvals(self.obsid,
    #                                                         self.chunkid,
    #                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

                                old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                                   namep1 = self.modID1,
                                                   idname = data_partID ,
                                                   last_of_chunks = self.last_of_chunks_p,
                                                   modulename = self.name+self.mod_name_extra+'release',
                                                   size_to_remove = -1, # set size to 0
                                                   free_RAMresource = True ) #release sizevalue
                            
                                rb_keep_trying = 0 # to stop repeating loop

                                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                             self.chunkid,
                                                             self.name+self.mod_name_extra+'_idata_releaseEall')
                            
                                mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(data_partID)+' at '+str(self.env.now)         
                                mes2='Intermediate Data from CPU RAM resource was released (from size '+str(old_s)+' to '+ str(new_s)+')'
#                                logger.info(mes2+mes1)
                            
                        
                            if (was_done_flag == False):
                                yield self.env.timeout(1)


    
    def run(self,whichevent,run_only_one_process_for_all_chunks):
        """
        If condition for start (checked via timecol_inoo_check1 column in output_data)
        is true, and if enough resources are available, wait for processing time
        (runtime), and update log and output DataFrames.
        
        If any of these conditions is not met, try again after 1 time unit.

        Returns
        -------
        updated DataFrames (out_data, out_resources) in oo

        """
        #print('--- before event', self.name,self.obsid,self.chunkid)

        yield whichevent
        
#            datastr='/beam '+str(self.beamid)+'/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        errflag=0
        resource_condition = False # start value, updated if time condition is true

        #print('---C after event', self.name,self.obsid,self.chunkid)
        
        if run_only_one_process_for_all_chunks == True :
            condition2 = self.last_of_chunks_p

        if run_only_one_process_for_all_chunks == False :
            condition2 = True
            

        while (self.condition() and condition2):
            # checking resources makes only sense if process can be started
            if (self.time_condition()):

            
#            logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [A] '+str(self.env.now))       
# 
 
                # check if enough resources are available, if not -> error messages + advance time    
                resource_condition, errflag = self.resource_conditionF(errflag)
                
                # check intermediate_data (if not requested, will be true anyway)
                ID_condition = self.intermediate_data_condition()
            
                if (resource_condition and self.time_condition() and ID_condition):
                    start_module=self.env.now
#                    logger.info(datastr+': Module '+str(self.name)+' started at time : ' + str(start_module))

#                   ----- request resources
#                   *proceeds only when* enough memory  are available
                    if (self.use_resource_mem > 0):
                        M_module= yield self.have_resource_mem.get(self.use_resource_mem)
                        print_backtonormal(str(self.name)+'---processM RAM',errflag,self.env.now)
                    
#                   *proceeds only when* enough CPUs/Threads or GPU units  are available
                    Ncpugpu_module= yield self.have_resource_num.get(self.use_resource_num)
                    print_backtonormal(str(self.name)+'---Processor/thread numbers',errflag,self.env.now)

                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                     self.chunkid,
                                                     self.name+'_start')

                    #resource for intermediate data is requested
                    if (self.mod_size_idata > 0):
                        M_moduleb= yield self.have_resource_mem.get(self.mod_size_idata)
                        print_backtonormal(str(self.name)+'---intermediate data RAM',errflag,self.env.now)
                    
                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        -1)
                    
                    # be aware that if several processes write this at the same time 
                    #(e.g. several chunks request the resources above)
                    # the resource numbers can get mixed up. In such cases, the sums should be correct
                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                     self.chunkid,
                                                     self.name+'_idata')
#                                                     self.name+'_start_idata')
                    # update 'last_used_by' for 'required' in inter_data DataFrame 
                    


#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [B] '+str(self.env.now))       

                    #---- "processing" time
                    yield self.env.timeout(self.runtime)
                                
                    end_module=self.env.now

                    # create/modify intermediate data (only doing something if requested)
                    self.intermediate_data_modify()
                    self.intermediate_data_update_access_for_required()

                    
    
#                    logger.info(datastr+': Module '+str(self.name)+' ended at time : ' + str(end_module))

    #              ----- return resources
                    self.have_resource_num.put(self.return_resource_num)
                    self.have_resource_mem.put(self.return_resource_mem)
 
                    self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.name+'_end')

                    self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        self.chunkid,
                                                        self.timecol_inoo,
                                                        round(self.env.now))

                    # "delete" intermediate data and return resources (if requested)
                    self.intermediate_data_delete()
#                logger.info(datastr+': Module '+str(self.name)+'available '
#                        +str(self.use_resource_name)+' resources #:'
#                        +str(self.have_resource_num.level)+', Memory: '
#                        +str(self.have_resource_mem.level)+' at time [C] '+str(self.env.now))       
                    self.keep_trying = 0
                if ((resource_condition == False) ):
                    #try condition again after 1 time unit
                    yield self.env.timeout(1)
                if ((ID_condition == False) ):
                    #try condition again after 0.1 time unit - 
                    #together with round(), this is a "trick" to get the same-time processing at of multiple chunks 
                    # Reason: flags that allow processing need to be written + then read, small internal processing time offsets 
                    #         hence the same-time simpy processes may initially find false condition for some of the chunks
                    yield self.env.timeout(0.1)
#                    print(self.env.now)
            if (self.time_condition() == False):
                yield self.env.timeout(1)




###############################################################################

class Proc_Module_Useevent_Idata_wOverlap(Proc_Module_Useevent_Idata):
    """
    This is a general processing class that keeps trying to start (and run) a process 
    even if resource starting conditions are not fulfilled at the beginning. 
    In contrast to proc_module, this class first waits for a successful event 
    (a previous process) before it even starts to query the resources
    
    The difference to the parent class is consideration of an overlap region
    in the naming of chunk-ranges for the intermediate data
    
    """
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo_check1 : str,
                 timecol_inoo : str,
                 with_intermediate_data : str,
                 num_chunks_for_intermediate_data: int,
                 total_num_chunks : int,
                 req_ID1_inoointer: str,
                 req_last_used_by: str,
                 req_change_used_by:str,
                 mod_ID1_inoointer: str,
                 mod_size_interdata_perchunk: int,
                 mod_name_extra : str,
                 del_ID1_inoointer: str,
                 del_size_interdata_total: int,
                 del_resourceflag: bool,
                 overlap_chunks : int
                 ):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
        [ One would expect: use_resource_mem = return_resource_mem, return_resource_num = use_resource_num]    
        [ However, there may be need for flexibility in future ]
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
        with_intermediate_data : 
            what should be done regarding intermediate data. The following options are possible
            [R] requires [old] intermediate data to proceed
            [M] creates or modifies intermediate data
            [D] deletes intermediate data
            [RM] is [R] and [M]
            [RD] is [R] and [D]
            [RMD] is [R],[M] and [D]
            [MD] is [M] and [D]
            all variations: ['R','r','M','m','D','d','RM','MR','rm','mr',
                          'RD','DR','rd','dr','MD','DM','md','dm',
                          'RMD','MDR','DRM','RDM','MRD','DMR',
                          'rmd','mdr','drm','rdm','mrd','dmr']
        num_chunks_for_intermediate_data : int
            how many data chunks are needed for processing 
            [current processing: one after the other]             
            to produce the intermediate data;
            R,M,D consider the same <=chunkID of obsid
            [currently manually set in global class, later from config]
        total_num_chunks
            total number of chunks in observation
        req_ID1_inoointer: str
            ID_part1 name of intermediate data that are >required< for this module
        req_last_used_by: str
            for the required intermediate data, there is also a required module/processing stage
            of the last access (e.g., 'DDBC_done')
        req_change_used_by: str
            name to which req_last_used_by is now changed, if 'None', nothing is replaced 
            which means there is no track record of the last access 
        mod_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >modified< (e.g., D_rfim)
        mod_size_interdata_perchunk: int,
            size in cpu_ram that is used/to be reserved in the intermediate data
            this size is requested and reserved from cpu_ram reserve
        mod_name_extra : str
            extra ID part to indicate what data (e.g., msk, cl)
            
            Note: Make sure, this name+done appears in the list of  
                  the >check_done_withorder< method of the parent class
            
        del_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >deleted<
            (size will be reduced in inter_data)
        del_size_interdata_total: int
            if < 0 : all the size_in_ram is set to 0
            This should be the default
            
            if > 0 : the value will be subtracted from current size_in_ram
            BEWARE: Partial subtraction, or oversubtraction can be possible
            This is implemented for possible future use.            
        del_resourceflag: bool
              Boolean, if True, returns cpu_ram with respective value
              Default is False  
              BEWARE: Potential for releasing resources twice
        overlap_chunks : int
            The number of chunks in the overlap region (of the SPS aggregation buffer). 
            It is needed to give the correct ID name  
     

        Returns
        -------
        A modified oo (out_data, out_resources,inter_data). 

        """

        super().__init__(
                 env=env,
                 name = name,
                 runtime = runtime,
                 use_resource_name = 'CPU',
                 use_resource_num = use_resource_num,
                 use_resource_mem = use_resource_mem,
                 return_resource_num = return_resource_num,
                 return_resource_mem = return_resource_mem,
#                 beamid,
                 obsid = obsid,
                 chunkid = chunkid,
                 oo = oo,
                 timecol_inoo_check1  = timecol_inoo_check1,
                 timecol_inoo  = timecol_inoo,
                 with_intermediate_data  = with_intermediate_data,
                 num_chunks_for_intermediate_data = num_chunks_for_intermediate_data,
                 total_num_chunks  = total_num_chunks,
                 req_ID1_inoointer = req_ID1_inoointer,
                 req_last_used_by = req_last_used_by,
                 req_change_used_by = req_change_used_by,
                 mod_ID1_inoointer = mod_ID1_inoointer,
                 mod_size_interdata_perchunk = mod_size_interdata_perchunk,
                 mod_name_extra  = mod_name_extra,
                 del_ID1_inoointer = del_ID1_inoointer,
                 del_size_interdata_total = del_size_interdata_total,
                 del_resourceflag = del_resourceflag
                 )
        self.overlap_chunks = overlap_chunks
        
        # redo/overwrite the part of the grandparent class which accounts for the naming
        self.w_idata = with_intermediate_data 
        w_idata_list=['R','r','M','m','D','d','RM','MR','rm','mr',
                     'RD','DR','rd','dr','MD','DM','md','dm',
                     'RMD','MDR','DRM','RDM','MRD','DMR',
                     'rmd','mdr','drm','rdm','mrd','dmr']
       
        if any(self.w_idata == w_element for w_element in w_idata_list):
           self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p = self.oo.interd_get_ID_namepart2_overlap(
                                                               obsid = self.obsid,
                                                               chunkid = self.chunkid,
                                                               useXchunks = self.nchunks_idata,
                                                               totalchunks = self.total_num_chunks,
                                                               overlapchunks = self.overlap_chunks 
                                                               )
           #print(self.obsid,self.chunkid,self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p)                                                        

        if all(self.w_idata != w_element for w_element in w_idata_list): 
           self.inchunkid_p  = 'None'
           self.first_of_chunks_p = 'None'
           self.last_of_chunks_p = 'None'       
        
    def release_intermediate_data_products(self, event_to_check, last_used_check_rb):
              
            # in comparison to parent: self.oo.interd_get_list_ID_nameparts -> self.oo.interd_get_list_ID_nameparts_overlap
#      ---------- do not a list, but only the corresponding chunk package
#            allnameIDs = self.oo.interd_get_list_ID_nameparts_overlap(obsid = self.obsid,
#                                                              useXchunks = self.nchunks_idata,
#                                                              totalchunks = self.total_num_chunks,
#                                                              overlapchunks = self.overlap_chunks
#                                                              )
#
            self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p = self.oo.interd_get_ID_namepart2_overlap(
                                                               obsid = self.obsid,
                                                               chunkid = self.chunkid,
                                                               useXchunks = self.nchunks_idata,
                                                               totalchunks = self.total_num_chunks,
                                                               overlapchunks = self.overlap_chunks 
                                                               )
            allnameIDs = pd.DataFrame({'ID':[self.inchunkid_p]})

                    
            # do  this if it is called immediately 
            if ((self.last_of_chunks_p == True) ):  
            
    #        print('called')
    #        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
                yield event_to_check
                
                #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
                rb_keep_trying = 1 
                while (rb_keep_trying == 1):
                    
                    row_exists,sizevalue,last_module = self.oo.interd_existornot(
                                                            obsid  = self.obsid,
                                                            namep1 = self.modID1,
                                                            idname = self.inchunkid_p
                                                            )
                 
                    if (row_exists == False):
                    # this should actually never happen if event_to_check is reasonable
                    # if this error, there is probably a naming issue in passed string variables
                        errmes=self.dataID+' (P)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' does NOT exist in inter_data at '+str(self.env.now)
                        logger.error(errmes)

                
                    if (row_exists == True):
                        #print('///',sizevalue,last_module,last_used_check_rb)
                        
                        was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
                                                                  last_used_check_value = last_module)
                        
                        #print('--------------',was_done_flag,self.modID1,self.inchunkid_p, last_used_check_rb, last_module) 
                        
                    
                        if (was_done_flag == True):
                            
                            # this is done to allow modification of inter_data (since for last_chunk) 
                            # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
                            yield self.env.timeout(0.1)  
                            
    #                        self.oo.outres_obs_chunk_setvals(self.obsid,
    #                                                         self.chunkid,
    #                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

                            old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                                   namep1 = self.modID1,
                                                   idname = self.inchunkid_p ,
                                                   last_of_chunks = self.last_of_chunks_p,
                                                   modulename = self.name+self.mod_name_extra+'release',
                                                   size_to_remove = -1, # set size to 0
                                                   free_RAMresource = True ) #release sizevalue
                            
                            rb_keep_trying = 0 # to stop repeating loop

                            self.oo.outres_obs_chunk_setvals(self.obsid,
                                                             self.chunkid,
                                                             self.name+self.mod_name_extra+'_idata_releaseE')
                            
                            mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' at '+str(self.env.now)         
                            mes2='Intermediate Data from CPU RAM resource was released (from size '+str(old_s)+' to '+ str(new_s)+')'
#                            logger.info(mes2+mes1)
                            
                        
                        if (was_done_flag == False):
                            yield self.env.timeout(1)
                    
   # #----------------------------------------------------------------------------  
            
   #          if ((self.last_of_chunks_p == True) ):  
                
   #  #        print('called')
   #  #        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
   #              yield event_to_check
   #              for data_partID in allnameIDs['ID']:
                
   #              #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
   #                  rb_keep_trying = 1 
   #                  while (rb_keep_trying == 1):
                    
   #                      row_exists,sizevalue,last_module = self.oo.interd_existornot(
   #                                                          obsid  = self.obsid,
   #                                                          namep1 = self.modID1,
   #                                                          idname = data_partID
   #                                                          )
                 
   #                      if (row_exists == False):
   #                  # this should actually never happen if event_to_check is reasonable
   #                  # if this error, there is probably a naming issue in passed string variables
   #                          errmes=self.dataID+' (P)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(data_partID)+' does NOT exist in inter_data at '+str(self.env.now)
   #                          logger.error(errmes)

                
   #                      if (row_exists == True):
   #                      #print('///',sizevalue,last_module,last_used_check_rb)
                        
   #                          was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
   #                                                                    last_used_check_value = last_module)
                        
   #                      #print('--------------',was_done_flag,self.modID1,self.inchunkid_p, last_used_check_rb, last_module) 
                        
                    
   #                          if (was_done_flag == True):
                            
   #                          # this is done to allow modification of inter_data (since for last_chunk) 
   #                          # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
   #                              yield self.env.timeout(0.1)  
                            
   #  #                        self.oo.outres_obs_chunk_setvals(self.obsid,
   #  #                                                         self.chunkid,
   #  #                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

   #                              old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
   #                                                 namep1 = self.modID1,
   #                                                 idname = data_partID ,
   #                                                 last_of_chunks = self.last_of_chunks_p,
   #                                                 modulename = self.name+self.mod_name_extra+'release',
   #                                                 size_to_remove = -1, # set size to 0
   #                                                 free_RAMresource = True ) #release sizevalue
                            
   #                              rb_keep_trying = 0 # to stop repeating loop

   #                              self.oo.outres_obs_chunk_setvals(self.obsid,
   #                                                           self.chunkid,
   #                                                           self.name+self.mod_name_extra+'_idata_releaseEall')
                            
   #                              mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(data_partID)+' at '+str(self.env.now)         
   #                              mes2='Intermediate Data from CPU RAM resource was released (from size '+str(old_s)+' to '+ str(new_s)+')'
   #                              logger.info(mes2+mes1)
                            
                        
   #                          if (was_done_flag == False):
   #                              yield self.env.timeout(1)
 

###############################################################################

class receptor_mod(proc_module):
    """
    The Receptor Class, a child class of the general processing class.
    It does not need a time condition (data always coming) which is set to always True.
    """
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
#                 timecol_inoo_check1:str, # not needed here
                 timecol_inoo:str):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
            DESCRIPTION.
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
            

        Returns
        -------
        A modified oo (out_data, out_resources). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid

        self.oo = oo
#        self.timecolcheck_inoo = timecol_inoo_check1
        self.timecol_inoo = timecol_inoo

        
        self.keep_trying = 1
        
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus

    # no time condition needed here
    def time_condition(self):    
        """
        re-defines the time condition of the parent class as always True

        Returns
        -------
        Bollean:
            True

        """
        return(True)
    

###############################################################################

class Buffer(object):
    """
    The buffer class. 
    It creates a buffer DataFrame [for future use].
    """
    # this will be initialized each time anew when created
    def __init__(self,
                 buffername:str,
                 obs_id_in:int,
                 howmany_chunks:int,
                 chunkstartid:int,
                 total_memory:int,
                 runtimechunk:int,
                 resource_name:str, 
                 M_resource:int, 
                 use_resource:int,
                 env,
                 oo,
                 timecol_inoo:str,
                 timecolcheck_inoo:str):
        """
        Setup of Module

        Parameters
        ----------
        buffername : short module name (e.g., FFBC)
        obs_id_in : ID of observation
        howmany_chunks : how many data chunks will the buffer hold (for buffer DataFrame) 
        chunkstartid : ID of the first data chunk  (for buffer DataFrame)
        total_memory : final RAM/memory held in the buffer  (for buffer DataFrame)
        runtimechunk : processing time of the buffering
        resource_name : CPU or GPU
        M_resource : RAM/Memory requested
        use_resource : number of CPU/Threads/GPU units requested
        env : simpy environment
        oo : instance of output/coordination class 
        timecol_inoo : time column in out_data that will be modified AFTER buffering
        timecolcheck_inoo : time column in out_data to check time_condition

        Returns
        -------
        modified output DataFrames.
        
        Buffer.in_buffer gives information about the buffer.
        [currently overwritten, new columns could be added instead]
        """
        
        
        self.buffer = buffername
        self.obsid = obs_id_in
        self.howmanychunks = howmany_chunks
        self.chunkstartid = chunkstartid
        
        self.tot_mem = total_memory         # all data chunks
        
        self.runtime = runtimechunk         # for each data chunk
        self.use_resource_name = resource_name  # for each data chunk
        self.use_resource_num = use_resource    # for each data chunk
        self.use_resource_mem = M_resource        # for each data chunk
        
        self.env = env
        self.oo = oo
        self.timecol_inoo = timecol_inoo
        self.timecolcheck_inoo = timecolcheck_inoo
        
        self.keep_trying = 1
        
        if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores

        if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
            self.have_resource_mem= env.mempergpu
            self.have_resource_num= env.gpus
       
        
        # for future use
        # - add new lines for new obs later
        self.in_buffer=pd.DataFrame({'b_name':[buffername],
                                     'obs':[self.obsid],     #obsID
                                     'first_cID':[self.chunkstartid],
                                     'last_cID':[0],   # MOST RECENT processed chunk ID
                                     't_first_c_started':[0],
                                     't_last_c_done':[0], #time when MOST RECENT chunk has been put into buffer
                                   })
        # here we could later add columns 'processed_by_XY', time when done
    
    def condition(self):
        """
        Here used to emphasize type of process " try until all conditions met".
        
        Additional conditions for other buffer modules could be added.

        Returns
        -------
        bool
        True: The process is tried to run again.
        False: The process is aborted.

        """
        if (self.keep_trying == 1):     
            return True
        else:
            return False

         
    def time_condition(self,chunkid):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.

        Parameters
        ----------
        chunkid : ID of data chunk

        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           chunkid,
                                                           self.timecolcheck_inoo)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)

    def resource_conditionF(self,errflag):
        """
        Checks if resources are available and returns Error messages if needed.

        Parameters
        ----------
        errflag : type of error as a number.
                  It is used to know whether there was an error in a previous try.

        Returns
        -------
        Boolean:
        True  (resources available)  
        False (at least one resource is not available)

        """

        if (self.have_resource_mem.level < self.use_resource_mem): 
            print_memerror(str(self.name),self.use_resource_mem,self.have_resource_mem.level,self.env.now)
            errflag=1
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.buffer+'_MEM_ERROR')

        #           check if enough CPUs/Threads or GPU units are available                
        if (self.have_resource_num.level < self.use_resource_num): 
            if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
                print_cpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=2
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.buffer+'_CPUnum_ERROR')
            if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
                print_gpuerror(str(self.name),self.use_resource_num,self.have_resource_num.level,self.env.now)
                errflag=3
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                                 self.chunkid,
                                                 self.buffer+'_GPUnum_ERROR')
       
        resource_condition_h = ((self.have_resource_mem.level > self.use_resource_mem) 
                              & (self.have_resource_num.level > self.use_resource_num))
        
        return(resource_condition_h,errflag)


    def do_buffering(self,chunkid):
        """
        Do the buffering for a specific data chunk

        Parameters
        ----------
        chunkid : ID of data chunk

        Yields
        ------
         Modified output DataFrames.
         A modified buffer frame (planed to be used in future).
        """
        
        errflag=0
        
        while self.condition():
            # check data condition (e.g., data was successfully received)
            
            resource_condition,errflag = self.resource_conditionF(errflag)

            if (resource_condition & self.time_condition(chunkid) ):
                # get resources
                ncpu_module_i=yield self.have_resource_num.get(self.use_resource_num)
                if ((self.use_resource_name=='CPU') or (self.use_resource_name=='cpu')):
                    print_backtonormal(self.buffer +' CPU',errflag,self.env.now)
                if ((self.use_resource_name=='GPU') or (self.use_resource_name=='gpu')):
                    print_backtonormal(self.buffer +' GPU',errflag,self.env.now)

                M_module_ram= yield self.have_resource_mem.get(self.use_resource_mem)
                print_backtonormal(self.buffer +' -processM RAM',errflag,self.env.now)

                #start time
                time_begin_buffering = self.env.now

                self.oo.outres_obs_chunk_setvals(self.obsid,
                                            chunkid,
                                            self.buffer+'_begin')

                self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                        chunkid,
                                                        self.timecol_inoo,
                                                        -1)

#                logger.info('Buffering in  '+str(self.buffer)
#                            +' (obsID '+str(self.obsid)+', chunkID: '+str(chunkid) 
#                            +') started at time : ' 
#                            + str(time_begin_buffering))
                
                # fill buffer data frame
                self.in_buffer.first_cID.iat[0]=self.chunkstartid
        
                if (chunkid == self.chunkstartid):
                    time_first = time_begin_buffering
                
                self.in_buffer.last_cID.iat[0]=chunkid
 
                # "processing time"
                yield self.env.timeout(self.runtime)
                time_end_buffering = self.env.now   

                # fill buffer data frame
                if (chunkid > self.chunkstartid): 
                    time_firsth=self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                                        self.chunkstartid,
                                                                        self.timecol_inoo)
                    time_first=time_firsth-self.runtime
                    self.in_buffer.t_first_c_started.iat[0]=time_first
                    self.in_buffer.t_last_c_done.iat[0]=time_end_buffering
#               print(self.in_buffer)
        
        
                # free number resources
                self.have_resource_num.put(self.use_resource_num)
                # free processing memory 
                self.have_resource_mem.put(self.use_resource_mem)

                 # out_resource entry after
                self.oo.outres_obs_chunk_setvals(self.obsid,
                                            chunkid,
                                            self.buffer+'_end')
                 # out_data entry
                self.oo.mask_outd_obs_chunk_flag_setval(self.obsid,
                                                   chunkid,
                                                   self.timecol_inoo,
                                                   self.env.now)

#                logger.info('Buffering in  '+str(self.buffer)
#                            +' (obsID '+str(self.obsid)
#                            +', chunkID: '+str(chunkid) 
#                            +') ended at time : ' 
#                            + str(time_begin_buffering))

#                if (chunkid == (self.howmanychunks+self.chunkstartid-1)):
#                    logger.info('Buffering in  '+str(self.buffer)
#                                +' (obsID '+str(self.obsid)
#                                +') for ALL data chunks finished at time : ' 
#                                + str(time_end_buffering))
#

#  for later, could be output which increasing row numbers
#                logger.info(self.in_buffer)
                
                self.keep_trying = 0  
                
        if ((resource_condition == False) or (self.time_condition(chunkid) == False)):
             yield self.env.timeout(1)
            

###############################################################################
class buffer_mod(proc_module_useevent):
    """
    The buffer module class. 
    (in contrast to the Buffer class it does NOT create a buffer DataFrame )    
    """
    # this will be initialized each time anew when created
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid:int,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo:str,
                 timecolcheck_inoo:str):
           
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., FFBC) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo : str 
            time column in out_data that will be modified AFTER buffering
            This column MUST already exist in oo.out_data.
        timecolcheck_inoo : str
            time column in out_data to check time_condition
            This column MUST already exist in oo.out_data.
        Returns
        -------
        A modified oo (out_data, out_resources). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid
        
        self.oo = oo
        self.time_cd1 = timecolcheck_inoo
        self.timecol_inoo = timecol_inoo
        
                
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus

        self.keep_trying = 1

 
    def time_condition(self):
        """
        Checks if a time column in the output Data Frame is larger than 0.
        This means that the process corresponding to that column was successful.

        Parameters
        ----------
        chunkid : ID of data chunk

        Returns
        -------
        Boolean:
        True  (condition fulfilled)  
        False (condition not fulfilled)

        """
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.chunkid,
                                                           self.time_cd1)
        time_condition_h = ((time_c_1 > 0) )
        
        return(time_condition_h)

###############################################################################
class Buffer_Module_Useevent_Idata(Proc_Module_Useevent_Idata):
    """
    The buffer module class that also handles the (intermediate) data buffer          
    The parent proc_module_useevent_idata class needs the following

    Parameters
    ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU   --- currently assume CPU for all buffers
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
            DESCRIPTION.
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
        with_intermediate_data : 
            what should be done regarding intermediate data. The following options are possible
            [R] requires [old] intermediate data to proceed (from previous processes)
            [M] creates or modifies intermediate data
            [D] deletes intermediate data 
            === Important! this  for  OLD intermediate data, not the buffer created via M
            [RM] is [R] and [M]
            [RD] is [R] and [D]
            [RMD] is [R],[M] and [D]
            [MD] is [M] and [D]
            all variations: ['R','r','M','m','D','d','RM','MR','rm','mr',
            ----------------'RD','DR','rd','dr','MD','DM','md','dm',
            ----------------'RMD','MDR','DRM','RDM','MRD','DM'
            ----------------'rmd','mdr','drm','rdm','mrd','dmr']
        num_chunks_for_intermediate_data : int
            how many data chunks are needed for processing 
            [current processing: one after the other]             
            to produce the intermediate data;
            R,M,D consider the same <=chunkID of obsid
            [currently manually set in global class, later from config]
        total_num_chunks
            total number of chunks in observation
        req_ID1_inoointer: str
            ID_part1 name of intermediate data that are >required< for this module
        req_last_used_by: str
            for the required intermediate data, there is also a required module/processing stage
            of the last access (e.g., 'DDBC_done')
        req_change_used_by: str
            entry to which 'last_used_by' column of row req_ID1 will be changed (from req_last_used_by)
        mod_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >modified< (e.g., D_rfim)
        mod_size_interdata_perchunk: int,
            size in cpu_ram that is used/to be reserved in the intermediate data
            this size is requested and reserved from cpu_ram reserve
            ---
            Note: This is used *in addition* to D_already_reserved in inter_data
            ----- but only mod_size_interdata_perchunk is newly requested
            ---
        mod_name_extra : str
            extra ID part to indicate what data (e.g., msk, cl)
        del_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >deleted<
            (size will be reduced in inter_data)
        del_size_interdata_total: int
            if < 0 : all the size_in_ram is set to 0
            This should be the default
            -----
            if > 0 : the value will be subtracted from current size_in_ram
            BEWARE: Partial subtraction, or oversubtraction can be possible
            This is implemented for possible future use.            
        del_resourceflag: bool
            Boolean, if True, returns cpu_ram with respective value
            Default is False  
            BEWARE: Potential for releasing resources twice
            ----     
            New additional Parameter (in comparison to processing class)
            ---- 
        D_already_reserved : int
            per chunk, how much of the resource has already been reserved
            (e.g., the RCPT already uses D_RCPT, the same bits of memory are also used for the DDBC)
            --------- 
        sub_chunks : int  
            The number of (sub)chunks that went into "a chunk"
            Example: The DDBC already buffers self.g.chunks_in_ddbc chunks.
            -------  The (lower number of ) DDBC buffers are then buffered in the FFBC
            -------  this number allows to keep track of the number of the origignal raw data chunks 
       
    Returns
    -------
        A modified oo (out_data, out_resources,inter_data). 

        """
    # Note: in the old buffer_mod and Buffer class,  the order of 
    # timecol_inoo_check1 and timecol_inoo is different
    # these classes can be probably removed later anyway.
    
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo_check1 : str,
                 timecol_inoo : str,
                 with_intermediate_data : str,
                 num_chunks_for_intermediate_data: int,
                 total_num_chunks : int,
                 req_ID1_inoointer: str,
                 req_last_used_by: str,
                 req_change_used_by: str,
                 mod_ID1_inoointer: str,
                 mod_size_interdata_perchunk: int,
                 mod_name_extra : str,
                 del_ID1_inoointer: str,
                 del_size_interdata_total: int,
                 del_resourceflag: bool, 
                 D_already_reserved: int,
                 sub_chunks : int
                 ):
 
        super().__init__(
                 env=env,
                 name = name,
                 runtime = runtime,
                 use_resource_name = 'CPU',
                 use_resource_num = use_resource_num,
                 use_resource_mem = use_resource_mem,
                 return_resource_num = return_resource_num,
                 return_resource_mem = return_resource_mem,
#                 beamid,
                 obsid = obsid,
                 chunkid = chunkid,
                 oo = oo,
                 timecol_inoo_check1  = timecol_inoo_check1,
                 timecol_inoo  = timecol_inoo,
                 with_intermediate_data  = with_intermediate_data,
                 num_chunks_for_intermediate_data = num_chunks_for_intermediate_data,
                 total_num_chunks  = total_num_chunks,
                 req_ID1_inoointer = req_ID1_inoointer,
                 req_last_used_by = req_last_used_by,
                 req_change_used_by = req_change_used_by,
                 mod_ID1_inoointer = mod_ID1_inoointer,
                 mod_size_interdata_perchunk = mod_size_interdata_perchunk,
                 mod_name_extra  = mod_name_extra,
                 del_ID1_inoointer = del_ID1_inoointer,
                 del_size_interdata_total = del_size_interdata_total,
                 del_resourceflag = del_resourceflag
                 )
        self.D_already_reserved = D_already_reserved
        self.sub_chunks = sub_chunks
    
    
    # different from parent processing class because of consideration of  self.D_already_reserved   
    def work_with_intermediate_data(self,do_what):
        #do_what = 'Ncheck_if_exit','new_or_modify','release_intermediate_data_resource'
        
        # for log file tracking (if uncommented, see below) 
        datastr='/obID '+str(self.obsid)+'/data chunk '+str(self.chunkid)+'/ '
        
        # init return values (if nothing is/can be done)
        intermediate_data_flag= 100
        old_s = -100
        new_s = -100
        
        # do nothing
        #if (self.inchunkid_p == 'None'):
        #    intermediate_data_flag= 50
            
        
      #======================================================================== 
      # note: Here, we currently do NOT check/consider where we are in the num_chunks_for_intermediate_data
      # currently, the size is NOT used [but could be later or alterbatively with 'name_done']
        if ((self.inchunkid_p != 'None') and (do_what == 'check_if_exit')):
            
            row_exists,sizevalue,last_module = self.oo.interd_existornot(
                        obsid  = self.obsid,
                        namep1 = self.modID1,
                        idname = self.inchunkid_p
                        )
            
            if (row_exists == True):
                # a requirement for the last-used-by-module was given and is fullfilled
                if ((self.req_last_used_by != 'None') and (self.req_last_used_by == last_module)):
                    intermediate_data_flag=0
                if ((self.req_last_used_by != 'None') and (self.req_last_used_by != last_module)):
                    intermediate_data_flag=1
                    # useful to find out if a name error was done, but it clogs up the logfile, hence commented out as default
                    #logger.info(datastr+' --- did not find row with '+last_module+' at SIMtime: '+str(self.env.now))

            if (row_exists == False):
                intermediate_data_flag = 50   
                
               
      #======================================================================== 
        # only  changes 'last_used_by' inter_data (not size)
        if ((self.inchunkid_p != 'None') and (self.req_change_used_by != 'None') 
            and (do_what == 'update_accessed_required')):
            
            old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.reqID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.req_change_used_by, # the change
                                         size_to_add = 0))
            
            
            intermediate_data_flag=60            

       #======================================================================== 
         # only  changes 'last_used_by' inter_data (not size)
        if ((self.inchunkid_p != 'None') and 
            (self.req_change_used_by != 'None') and 
            (do_what == 'update_accessed_required_done') and (self.last_of_chunks_p == True)):
            
            old_s,new_s=(self.oo.interd_create_or_access_data(
                                          obsid = self.obsid,
                                          namep1 = self.reqID1,
                                          idname = self.inchunkid_p,
                                          first_of_chunks = self.first_of_chunks_p, 
                                          modulename = self.name+'_done', # the change
                                          size_to_add = 0))
             
             
            intermediate_data_flag=70            
           
            

      #======================================================================== 
        if ((self.inchunkid_p != 'None') and (do_what == 'new_or_modify')):

            
            indicate_last_chunk = self.name+self.mod_name_extra+'done'
            if (self.last_of_chunks_p == False):   
                
                row_exists,sizevalue,last_module = self.oo.interd_existornot(
                        obsid  = self.obsid,
                        namep1 = self.modID1,
                        idname = self.inchunkid_p
                        )
                # the last chunk was already 'processed' [can happen since same-timme processes in simpy not necessarily in order]
                if (row_exists == True) and (last_module == indicate_last_chunk):          
                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = indicate_last_chunk,
                                        # left with abs(), buffer unlikely to be reduced
                                         # changed in comparison to processing class [because data for buffer already exists]
                                         size_to_add = (np.abs(self.mod_size_idata) + np.abs(self.D_already_reserved))*self.sub_chunks
                                         ) )
                
                
                # entry exists, but the last chunk was not processed
                if (row_exists == True) and (last_module != indicate_last_chunk):          

                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.name+self.mod_name_extra,
                                         # changed in comparison to processing class [because data for buffer already exists]
                                        # left with abs(), buffer unlikely to be reduced
                                         size_to_add = (np.abs(self.mod_size_idata) + np.abs(self.D_already_reserved))*self.sub_chunks
                                         ) )
                #print(self.name+self.mod_name_extra)
                
                #--- no entry yet exists 
                if (row_exists == False) :          

                    old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = self.name+self.mod_name_extra,
                                         # changed in comparison to processing class [because data for buffer already exists]
                                        # left with abs(), buffer unlikely to be reduced
                                         size_to_add = (np.abs(self.mod_size_idata) + np.abs(self.D_already_reserved))*self.sub_chunks
                                         ) )
                
                
                
            # indicate that last chunk has been reached
            if (self.last_of_chunks_p == True):            
                old_s,new_s=(self.oo.interd_create_or_access_data(
                                         obsid = self.obsid,
                                         namep1 = self.modID1,
                                         idname = self.inchunkid_p,
                                         first_of_chunks = self.first_of_chunks_p, 
                                         modulename = indicate_last_chunk,
                                         # changed in comparison to processing class [because data for buffer already exists]
                                        # left with abs(), buffer unlikely to be reduced
                                         size_to_add = (np.abs(self.mod_size_idata) + np.abs(self.D_already_reserved))*self.sub_chunks
                                         ) )
            
            # no actual change in resource happens, only in inter_data, 
            # this is a reference entry for the out_resources
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+self.mod_name_extra+'_idata_Bmod')
            
            
            intermediate_data_flag=10 
            
            
        
      #======================================================================== 
        if ((self.inchunkid_p != 'None') and (do_what == 'release_intermediate_data_resource')):
            #this modifies the .inter_data data frame and (depending on flag) returns cpu_ram resource
            old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                       namep1 = self.delID1,
                                       idname = self.inchunkid_p ,
                                       last_of_chunks = self.last_of_chunks_p,
                                       modulename = self.name+self.mod_name_extra+'_freeB',
                                       size_to_remove = (self.del_size_idata)*self.sub_chunks,
                                       free_RAMresource = self.del_resourceflag) 
            
            intermediate_data_flag=20
            
            self.oo.outres_obs_chunk_setvals(self.obsid,
                                             self.chunkid,
                                             self.name+self.mod_name_extra+'_idata_freeB')
       
        return(intermediate_data_flag,old_s,new_s)

    
    # different messages in comparison to processing class
    def intermediate_data_modify(self):
        IDr_flag = 100
        
        # check if intermediate data should be modified/created
        wh2_idata_list=['M','m','RM','MR','rm','mr',
                        'MD','DM','md','dm',
                        'RMD','MDR','DRM','RDM','MRD','DMR',
                        'rmd','mdr','drm','rdm','mrd','dmr']
        # if no intermediate data need to be modified, all is fine
        #if all(self.w_idata != w_element for w_element in wh2_idata_list):
        #    IDr_flag = 0
        
        #if intermediate data should be modified
        if any(self.w_idata == w_element for w_element in wh2_idata_list):
            
            IDr_flag,os2,ns2 = self.work_with_intermediate_data('new_or_modify')
        # messages are changed in comparison to processing class        
        if (IDr_flag == 10):
            mes1a= self.dataID+ ' --- B_'+self.name+self.mod_name_extra+' --- was modified in inter_data'
#            logger.info(mes1a)
            mes1b =self.dataID+ '--- B_'+self.name+self.mod_name_extra+' --- size '+str(os2)+' was changed to '+str(ns2)
#            logger.info(mes1b)

        if (IDr_flag > 10 ):
            mes2= self.dataID+' --- B_'+self.name+self.mod_name_extra+' --- was NOT modified in inter_data'
            logger.warning(mes2)

   
    # alias for buffer .run 
    def reserve_buffer(self,whichevent,run_only_one_process_for_all_chunks):
        yield self.env.process(self.run(whichevent, run_only_one_process_for_all_chunks))


    def release_buffer_resource(self, event_to_check, last_used_check_rb,only_idata):
        """
        Releases the buffer in intermediate data and as a resource if requested.
        
        Parameters
        ----------

            event_to_check : 
                simpy event which needs to have happened
            last_used_check_rb : 
                entry in last_used_by column of respective buffer B row
            only_idata : boolean, 
                if True *only* the inter_data frame is modified (B... to 0)
                if False the simpy resource is also released
                -
                This switch allows to avoid double-releasing the [same] data linked in several buffer

        Returns
        -------
            A modified inter_data, returned resources if requested

        """
        
        # do only something if this is the last chunk of the data 
        if ((self.last_of_chunks_p == True) and (only_idata == False)):  
            
#        print('called')
#        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
            yield event_to_check
            
            #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
            rb_keep_trying = 1 
            while (rb_keep_trying == 1):
                
                row_exists,sizevalue,last_module = self.oo.interd_existornot(
                                                        obsid  = self.obsid,
                                                        namep1 = self.modID1,
                                                        idname = self.inchunkid_p
                                                        )
             
                if (row_exists == False):
                # this should actually never happen if event_to_check is reasonable
                # if this error, there is probably a naming issue in passed string variables
                    errmes=self.dataID+' (B)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' does NOT exist in inter_data at '+str(self.env.now)
                    logger.error(errmes)

            
                if (row_exists == True):
                    #print('///',sizevalue,last_module,last_used_check_rb)
                    
                    was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
                                                              last_used_check_value = last_module)
                
                    if (was_done_flag == True):
                        
                        # this is done to allow modification of inter_data (since for last_chunk) 
                        # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
                        yield self.env.timeout(0.1)  
                        
#                        self.oo.outres_obs_chunk_setvals(self.obsid,
#                                                         self.chunkid,
#                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

                        old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                               namep1 = self.modID1,
                                               idname = self.inchunkid_p ,
                                               last_of_chunks = self.last_of_chunks_p,
                                               modulename = self.name+self.mod_name_extra+'release',
                                               size_to_remove = -1, # set size to 0
                                               free_RAMresource = True ) #release sizevalue
                        
                        rb_keep_trying = 0 # to stop repeating loop
#                        yield self.env.timeout(0.01)
                        self.oo.outres_obs_chunk_setvals(self.obsid,
                                                         self.chunkid,
                                                         self.name+self.mod_name_extra+'_idata_BreleaseE')
                        
                        mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' at '+str(self.env.now)         
                        mes2='Buffer CPU RAM resource was released (from size '+str(old_s)+' to '+ str(new_s)+')'
#                        logger.info(mes2+mes1)
                        
                    
                    if (was_done_flag == False):
                        yield self.env.timeout(1)
                
 
        if ((self.last_of_chunks_p == True) and (only_idata == True)):  
            
#        print('called')
#        print(self.obsid,self.modID1,self.inchunkid_p,last_used_check_rb)
            yield event_to_check
            
            #print('---proceed',self.obsid,self.modID1,self.inchunkid_p )
            rb_keep_trying = 1 
            while (rb_keep_trying == 1):
                
                row_exists,sizevalue,last_module = self.oo.interd_existornot(
                                                        obsid  = self.obsid,
                                                        namep1 = self.modID1,
                                                        idname = self.inchunkid_p
                                                        )
             
                if (row_exists == False):
                # this should actually never happen if event_to_check is reasonable
                # if this error, there is probably a naming issue in passed string variables
                    errmes=self.dataID+' (B)--- Row with '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' does NOT exist in inter_data at '+str(self.env.now)
                    logger.error(errmes)

            
                if (row_exists == True):
                    #print('///',sizevalue,last_module,last_used_check_rb)
                    
                    was_done_flag = self.check_done_withorder(last_used_check_minimum = last_used_check_rb, 
                                                              last_used_check_value = last_module)
                
                    if (was_done_flag == True):
                        
                        # this is done to allow modification of inter_data (since for last_chunk) 
                        # to be the last in simpy order (for at the same time processing, simpy starts with the last chunk)
                        yield self.env.timeout(0.01)  
                        
#                        self.oo.outres_obs_chunk_setvals(self.obsid,
#                                                         self.chunkid,
#                                                         self.name+self.mod_name_extra+'_idata_BreleaseS')

                        old_s,new_s=self.oo.interd_delete_data(obsid = self.obsid,
                                               namep1 = self.modID1,
                                               idname = self.inchunkid_p ,
                                               last_of_chunks = self.last_of_chunks_p,
                                               modulename = self.name+self.mod_name_extra+'_idataO_release',
                                               size_to_remove = -1, # set size to 0
                                               free_RAMresource = False ) #release sizevalue
                        
                        rb_keep_trying = 0 # to stop repeating loop
#                        yield self.env.timeout(0.01)
                        self.oo.outres_obs_chunk_setvals(self.obsid,
                                                         self.chunkid,
                                                         self.name+self.mod_name_extra+'_idataO_BreleaseE')
                        
                        mes1=' for '+str(self.obsid)+','+str(self.modID1)+','+str(self.inchunkid_p)+' at '+str(self.env.now)         
                        mes2='Buffer was changed in inter_data (from size '+str(old_s)+' to '+ str(new_s)+')'
#                        logger.info(mes2+mes1)
                        
                    
                    if (was_done_flag == False):
                        yield self.env.timeout(1)
                
###############################################################################
class Buffer_Module_Useevent_Idata_wOverlap(Buffer_Module_Useevent_Idata):
    """
    The buffer module class that also handles the (intermediate) data buffer          
    The parent proc_module_useevent_idata class needs the following

    Parameters
    ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU   --- currently assume CPU for all buffers
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
            DESCRIPTION.
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        timecol_inoo_check1 : str
            The name of the time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
        with_intermediate_data : 
            what should be done regarding intermediate data. The following options are possible
            [R] requires [old] intermediate data to proceed (from previous processes)
            [M] creates or modifies intermediate data
            [D] deletes intermediate data 
            === Important! this  for  OLD intermediate data, not the buffer created via M
            [RM] is [R] and [M]
            [RD] is [R] and [D]
            [RMD] is [R],[M] and [D]
            [MD] is [M] and [D]
            all variations: ['R','r','M','m','D','d','RM','MR','rm','mr',
            ----------------'RD','DR','rd','dr','MD','DM','md','dm',
            ----------------'RMD','MDR','DRM','RDM','MRD','DM'
            ----------------'rmd','mdr','drm','rdm','mrd','dmr']
        num_chunks_for_intermediate_data : int
            how many data chunks are needed for processing 
            [current processing: one after the other]             
            to produce the intermediate data;
            R,M,D consider the same <=chunkID of obsid
            [currently manually set in global class, later from config]
        total_num_chunks
            total number of chunks in observation
        req_ID1_inoointer: str
            ID_part1 name of intermediate data that are >required< for this module
        req_last_used_by: str
            for the required intermediate data, there is also a required module/processing stage
            of the last access (e.g., 'DDBC_done')
        req_change_used_by: str
            entry to which 'last_used_by' column of row req_ID1 will be changed (from req_last_used_by)
        mod_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >modified< (e.g., D_rfim)
        mod_size_interdata_perchunk: int,
            size in cpu_ram that is used/to be reserved in the intermediate data
            this size is requested and reserved from cpu_ram reserve
            ---
            Note: This is used *in addition* to D_already_reserved in inter_data
            ----- but only mod_size_interdata_perchunk is newly requested
            ---
        mod_name_extra : str
            extra ID part to indicate what data (e.g., msk, cl)
        del_ID1_inoointer: str
            ID_part1 name for the intermediate data to be >deleted<
            (size will be reduced in inter_data)
        del_size_interdata_total: int
            if < 0 : all the size_in_ram is set to 0
            This should be the default
            -----
            if > 0 : the value will be subtracted from current size_in_ram
            BEWARE: Partial subtraction, or oversubtraction can be possible
            This is implemented for possible future use.            
        del_resourceflag: bool
            Boolean, if True, returns cpu_ram with respective value
            Default is False  
            BEWARE: Potential for releasing resources twice
            ----     
            New additional Parameter (in comparison to processing class)
            ---- 
        D_already_reserved : int
            per chunk, how much of the resource has already been reserved
            (e.g., the RCPT already uses D_RCPT, the same bits of memory are also used for the DDBC)
            --------- 
        sub_chunks : int  
            The number of (sub)chunks that went into "a chunk"
            Example: The DDBC already buffers self.g.chunks_in_ddbc chunks.
            -------  The (lower number of ) DDBC buffers are then buffered in the FFBC
            -------  this number allows to keep track of the number of the origignal raw data chunks
        overlap_chunks : int
            The number of chunks in the overlap region (of the SPS aggregation buffer). 
            It is needed to give the correct ID name  
       
    Returns
    -------
        A modified oo (out_data, out_resources,inter_data). 

        """
    # Note: in the old buffer_mod and Buffer class,  the order of 
    # timecol_inoo_check1 and timecol_inoo is different
    # these classes can be probably removed later anyway.
    
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid,
                 obsid:int,
                 chunkid:int,
                 oo,
                 timecol_inoo_check1 : str,
                 timecol_inoo : str,
                 with_intermediate_data : str,
                 num_chunks_for_intermediate_data: int,
                 total_num_chunks : int,
                 req_ID1_inoointer: str,
                 req_last_used_by: str,
                 req_change_used_by: str,
                 mod_ID1_inoointer: str,
                 mod_size_interdata_perchunk: int,
                 mod_name_extra : str,
                 del_ID1_inoointer: str,
                 del_size_interdata_total: int,
                 del_resourceflag: bool, 
                 D_already_reserved: int,
                 sub_chunks : int,
                 overlap_chunks : int
                 ):
         
        super().__init__(
                 env=env,
                 name = name,
                 runtime = runtime,
                 use_resource_name = 'CPU',
                 use_resource_num = use_resource_num,
                 use_resource_mem = use_resource_mem,
                 return_resource_num = return_resource_num,
                 return_resource_mem = return_resource_mem,
#                 beamid,
                 obsid = obsid,
                 chunkid = chunkid,
                 oo = oo,
                 timecol_inoo_check1  = timecol_inoo_check1,
                 timecol_inoo  = timecol_inoo,
                 with_intermediate_data  = with_intermediate_data,
                 num_chunks_for_intermediate_data = num_chunks_for_intermediate_data,
                 total_num_chunks  = total_num_chunks,
                 req_ID1_inoointer = req_ID1_inoointer,
                 req_last_used_by = req_last_used_by,
                 req_change_used_by = req_change_used_by,
                 mod_ID1_inoointer = mod_ID1_inoointer,
                 mod_size_interdata_perchunk = mod_size_interdata_perchunk,
                 mod_name_extra  = mod_name_extra,
                 del_ID1_inoointer = del_ID1_inoointer,
                 del_size_interdata_total = del_size_interdata_total,
                 del_resourceflag = del_resourceflag,
                 D_already_reserved = D_already_reserved,
                 sub_chunks = sub_chunks      
                 )
        self.overlap_chunks = overlap_chunks
        
        # redo/overwrite the part of the grandparent class which accounts for the naming
        self.w_idata = with_intermediate_data 
        w_idata_list=['R','r','M','m','D','d','RM','MR','rm','mr',
                     'RD','DR','rd','dr','MD','DM','md','dm',
                     'RMD','MDR','DRM','RDM','MRD','DMR',
                     'rmd','mdr','drm','rdm','mrd','dmr']
       
        if any(self.w_idata == w_element for w_element in w_idata_list):
           self.inchunkid_p, self.first_of_chunks_p,self.last_of_chunks_p = self.oo.interd_get_ID_namepart2_overlap(
                                                               obsid = self.obsid,
                                                               chunkid = self.chunkid,
                                                               useXchunks = self.nchunks_idata,
                                                               totalchunks = self.total_num_chunks,
                                                               overlapchunks = self.overlap_chunks 
                                                               )
                                                               

        if all(self.w_idata != w_element for w_element in w_idata_list): 
           self.inchunkid_p  = 'None'
           self.first_of_chunks_p = 'None'
           self.last_of_chunks_p = 'None'       

    
###############################################################################

            
class output_mod(proc_module_useevent):
    """
    The Output Class, a child class of the general processing class.
    It needs a more complicated time condition (here two processed must have been completed)
    """
    def __init__(self,
                 env,
                 name:str,
                 runtime:int,
                 use_resource_name:str,
                 use_resource_num:int,
                 use_resource_mem:int,
                 return_resource_num:int,
                 return_resource_mem:int,
#                 beamid:int,
                 obsid:int,
                 chunkid:int,
                 oo,
                 time_cond_col1:str,
                 time_cond_col2:str,
                 timecol_inoo:str,
                 max_chunk):
        """
        This class needs the following

        Parameters
        ----------
        env : a simpy environment
            This needs to be the same environment the whole simulation is run in.
        name : str
            A short module description (e.g., RCPT) to be passed on in outputs
        runtime : int
            The time, this module runs. (from POM config parameters)
        use_resource_name : str
            CPU or GPU 
        use_resource_num : int
            How many CPU/threads or GPU units are needed ?
        use_resource_mem : int
            How much RAM/memory is needed ?
        return_resource_num : int
            How many CPU/threads or GPU units are released AFTER processing ?
        return_resource_mem : int
            How much RAM/memory is released AFTER processing
        obsid : int
            ID of the observation
        chunkid : int
            ID of the data chunk
        oo : the output/coordination instance of the output_monitor class
            has routines and the output DataFrames for coordination
        time_cond_col1 : str
            The name of the FIRST time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        time_cond_col2 : str
            The name of the SECOND time column in oo.out_data that is used for the time condition
            This column MUST already exist in oo.out_data.
        timecol_inoo : str
            The name of the time column in oo.out_data that is belonging to this process.
            It is modified.
            This column MUST already exist in oo.out_data.
        max_chunk : int   
            The data chunk number after which the output process can be run.
            (e.g., for CDOS of the folded profile, max_chunk is the last data chunk of the observation)
        Returns
        -------
        A modified oo (out_data, out_resources). 

        """

        self.env = env
        self.name = name
        self.runtime = runtime
        
        self.use_resource_name = use_resource_name
        self.use_resource_num = use_resource_num
        self.use_resource_mem = use_resource_mem
        self.return_resource_num = return_resource_num
        self.return_resource_mem = return_resource_mem

#        self.beamid = beamid
        self.obsid = obsid
        self.chunkid = chunkid
        
        self.oo = oo
        self.time_cd1 = time_cond_col1
        self.time_cd2 = time_cond_col2
        self.timecol_inoo = timecol_inoo
        
        self.max_chunk = max_chunk
        
                
        if ((use_resource_name=='CPU') or (use_resource_name=='cpu')):
            self.have_resource_mem= self.env.cpu_ram
            self.have_resource_num= self.env.cpu_cores
        
        if ((use_resource_name=='GPU') or (use_resource_name=='gpu')):
            self.have_resource_mem= self.env.mempergpu
            self.have_resource_num= self.env.gpus

        self.keep_trying = 1

 
    def time_condition(self):
        time_c_1 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.max_chunk,
                                                           self.time_cd1)

        time_c_2 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.max_chunk,
                                                           self.time_cd2)

        time_c_3 = self.oo.mask_outd_obs_chunk_flag_getval(self.obsid,
                                                           self.max_chunk,
                                                           self.timecol_inoo)

        time_condition_h = ((time_c_1 > 0) & (time_c_2 > 0) & (time_c_3 == 0) )
        
        return(time_condition_h)


             
            

###############################################################################
###############################################################################


class sps_pipe_realistic1(object):
    """
    The sps pipeline class where the receptor, buffer, processing, and output processes are called
    - This is a SPS-ONLY pipeline -
    All the (not-in-overlap) resources are returned at the end of the processing of one aggregation buffer.
         
    """
    def __init__(self,env,g,df_POM_parT,df_cheetahflags,df_cheetah,name,oo_init):
        """
        gets the setup of the simulation

        Parameters
        ----------
        env : a simpy simulation environment 
              The same simpy env is used for the whole simulation (of one beam at the moment)
        g : the class with general parameters of the simulation 
            It was compiled before and is passed on.
        df_POM_run_flags : a pandas DataFrame with Flags of the POM
            It is used to run the pipeline with the config parameters (e.g., Cheetah modules on or off)
        name : Name of the pipeline 
            When multiple pipelines (beams) are run, this will help to keep track

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        self.env = env
        self.name= name
        self.simtime = g.sim_duration
        self.N_chunks = g.N_chunks
          
        self.g =g
        self.df_cheetah_in = df_cheetah
        self.df_cheetahflags_in = df_cheetahflags
        self.df_POM_parT_in =df_POM_parT


        #------------------------------------------------------------------------------
        # define flags which modules currently exist in POM; 
        #        1: exists 0: does not exist
        # TDAS ignored since not planned to do
        #------------------------------------------------------------------------------
        pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                                      'ddbc':       [1],
                                      'rfim':       [0],
                                      'ddtr':       [1],
                                      'spdt':       [1],
                                      'spsift':     [1],
                                      'spscluster': [1],
                                      'ffbc':       [0], # for unmodified obs
                                      'psbc':       [0],
                                      'cxft':       [0],
                                      'brdz':       [0],
                                      'dred':       [0],
                                      'fdas':       [0],
                                      'sift':       [0],
                                      'fldo':       [0],
                                      'cdos':       [1]
                                      })


        logger.info(pom_exist_flags)

        logger.info('------------- Check existing POM modules versus the wanted ones------------- ')
        
        import packages.POM_readin as pr
        self.df_POM_run_flags=pr.wanted_vs_exist(pom_exist_flags, df_POM_parT,df_cheetahflags,df_cheetah)
        
        if (self.df_POM_run_flags.run_rcpt.loc[0] == 0):
            warnings.warn('--- Running the POM without incoming data does not make sense \n'+
                          '--- The code will set RCPT to run')
            self.df_POM_run_flags.run_rcpt =1
    
            logger.info(self.df_POM_run_flags)


        #self.df_POM_run_flags = df_POM_run_flags
        self.obs_counter = 0
        self.chunk_counter = 0
        
#        self.oo = out_monitor(self.env)
        self.oo = oo_init
        

       

    def run(self):
        """
        runs the pipeline until end time of simulation is reached

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        # later to be extended to 3 beams
        #b_id=1
#        self.env.process(self.do(b_id))

        self.env.process(self.do())
        self.env.run(until=self.simtime)
        logger.info(str(self.name)+' time end: '+ str(self.env.now)) 
        return(self.oo.out_data,self.oo.out_resources, self.oo.inter_data,self.df_POM_run_flags)
      
 
    def do(self):
        """
        A realistic SPS  pipeline with individual module calls.

        Returns
        -------
        a running pipeline

        """
#    def do(self,b_id):
#        print(self.oo.out_data)
#        print(self.oo.out_resources)
        
        # setup the RCPT module  
        rcpt_runtime, \
        rcpt_use_resource_name,\
        rcpt_use_resource_num, \
        rcpt_use_resource_mem,\
        rcpt_return_resource_num,\
        rcpt_return_resource_mem, gmod =  rcpt_setup(self.g,
                                                     self.df_cheetah_in, 
                                                     self.df_cheetahflags_in, 
                                                     self.df_POM_parT_in)
        
        data_per_chunk_in_rcpt = rcpt_use_resource_mem - rcpt_return_resource_mem
        
 
        # setup of the SPS pipeline parameters  
        chunks_in_ddbcN,  \
        AB_Nb, \
        D_DDBCaggB_per_chunk, \
        D_DDTR_per_AggB, \
        t_DDTR_allthreads, \
        D_SPDTaggB, \
        t_SPDTmod, \
        D_SPSIFTaggB, \
        t_SPSCLUSTaggB_allthreads, \
        M_SPSCLUSTaggB, \
        D_SPSCLUSTaggB = sps_setup(self.g)  
        
        # start simulation time
        sps_pipe1_start=self.env.now
        # --- Create/get new observation
        # --- Increment the obs counter
        infstr1=' ------------- SPS started at: '+ str(sps_pipe1_start)
        logger.info(infstr1)

        while True:
            
            self.obs_counter += 1
            time_obsID_started = self.env.now
#            logger.info('- obs counter: '+str(self.obs_counter)+
#                        ', started at time '+str(time_obsID_started))
            
            obs = Observation(self.obs_counter)
            # need to set 0 for each new observation
            self.chunk_counter = 0
            
            
            # ------ keeps generating data_chunks for one observation,  
            #        counting, until number-of-chunks is reached
            while self.chunk_counter < (self.N_chunks):
                #logger.info('### chunk counter, N_chunks: ' + 
#                            str(self.chunk_counter)+','+str(self.N_chunks)) 
                self.chunk_counter += 1
                errflag=0
                  
                chunk = Chunk(obs.obs_id,self.chunk_counter)
#                  print(chunk.obs_id,chunk.chunk_id)
                self.oo.init_od_newrow(obs.obs_id,self.chunk_counter)
                  #print(self.oo.out_data)
                                       
                time_read_start=self.env.now 



#-------------- Receptor module
                rec1=receptor_mod(self.env,
                                       '* RCPT',
                                       rcpt_runtime,
                                       'CPU',
                                       rcpt_use_resource_num,
                                       rcpt_use_resource_mem,
                                       rcpt_return_resource_num,
                                       rcpt_return_resource_mem,
                                  #     b_id,
                                       chunk.obs_id,
                                       chunk.chunk_id,
                                       self.oo,
                                       'time_rcpt')

                  # run data receptor 
                rec1_proc =self.env.process(rec1.run())
                yield rec1_proc    

##############################   
#-------------- DDBC module

                if (self.df_POM_run_flags.run_ddbc.loc[0] == 1):
                    
                    #------------- DDBC setup for SPS, checks for individual chunks
                    overlapchunks=AB_Nb
                    
                    # --- check if first chunk AFTER overlap region in buffer 2+x (x>=0)
                    first_chunk_inbuffer = check_if_first_chunk_inbuffer(chunkid = self.chunk_counter,
#                                                  useXchunks = self.g.chunks_in_ddbc,
                                                  useXchunks = chunks_in_ddbcN,
                                                  totalchunks = self.g.N_chunks,
                                                  overlapchunks = overlapchunks)
                    
                    # memory size for the chunks in first buffer and not-the-first chunks in subsequent buffers
                    D_DDBCmod = D_DDBCaggB_per_chunk
                    #  link/check later with data_per_chunk_in_rcpt (for 128 samples with 64mus OK)
                    if first_chunk_inbuffer: # reminder: Flag is False for first chunk in a observation
                        # for buffers >=2 the overlap region is copied again
                        D_DDBCmod = D_DDBCaggB_per_chunk + D_DDBCaggB_per_chunk * overlapchunks
                                            
                    # --- check if a chunk is the last chunk in a DDBC buffer
                    # number of buffers to consider    
                    n_c= int(np.ceil(self.g.N_chunks - chunks_in_ddbcN) /(chunks_in_ddbcN - overlapchunks) )
                    last_of_chunks_inAggBuffer = False
                    for i in range(0,n_c+1):
                        if ((self.chunk_counter == self.g.N_chunks) or 
                            (self.chunk_counter == (chunks_in_ddbcN+ (i)*(chunks_in_ddbcN - overlapchunks) ))):
                            last_of_chunks_inAggBuffer = True
    
                    
    
                        
                    ddbc = Buffer_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'DDBC',
                                   runtime = self.g.t_DDBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_DDBC,
                                   use_resource_mem = self.g.M_DDBC,
                                   return_resource_num = self.g.cpu_DDBC,
                                   return_resource_mem = self.g.M_DDBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rcpt',
                                   timecol_inoo  = 'time_ddbc',
                                   with_intermediate_data = 'M',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_DDBC',
# [A] --- removed for now since it deletes the whole raw data, but some of it may be still needed later (overlap)
# 
#                                   # some data was reserved for the receptor, now  + aggregation buffer copy
#                                   # doing it this way, will free up the resource of both after CDOS
#                                   mod_size_interdata_perchunk = (D_DDBCmod + data_per_chunk_in_rcpt), 

                                   mod_size_interdata_perchunk = (D_DDBCmod), 
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
# [A]                                  D_already_reserved = data_per_chunk_in_rcpt,
                                   D_already_reserved = 0,
                                   sub_chunks = 1,
                                   overlap_chunks = overlapchunks)

                    ddbc_proc = self.env.process(ddbc.reserve_buffer(rec1_proc,False))
                     

                      
                if (self.df_POM_run_flags.run_ddbc.loc[0] == 0):
                    logger.info('- NO DDBC done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))

##############################                      
#-------------- DDTR module

                if (self.df_POM_run_flags.run_ddtr.loc[0] == 1):

                    ddtr = Proc_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'DDTR',
                                   runtime = t_DDTR_allthreads,
#                                   runtime = self.g.t_DDTR,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_DDTR,
                                   use_resource_mem = self.g.M_DDTR,
                                   return_resource_num = self.g.cpu_DDTR,
                                   return_resource_mem = self.g.M_DDTR,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ddbc',
                                   timecol_inoo  = 'time_ddtr',
                                   with_intermediate_data = 'RM',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_DDBC',
                                   req_last_used_by ='DDBC_done',
                                   req_change_used_by = 'DDTR_req',
                                   mod_ID1_inoointer = 'D_DDTR',
#                                   mod_size_interdata_perchunk = self.g.D_DDTR,
                                   mod_size_interdata_perchunk = D_DDTR_per_AggB,
                                   mod_name_extra = '_DMtime',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   overlap_chunks = overlapchunks
                                   )

                    ddtr_proc = self.env.process(ddtr.run(ddbc_proc, True))

                if (self.df_POM_run_flags.run_ddtr.loc[0] == 0):
                      logger.info('- NO DDTR done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))

##############################                      
#-------------- SPDT module

                if (self.df_POM_run_flags.run_spdt.loc[0] == 1):
                # Note: processing per aggregation buffer, i.e. M_SPDT is per aggregation buffer
                    spdt = Proc_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'SPDT',
#                                   runtime = self.g.t_SPDT, # only single thread expected
                                   runtime = t_SPDTmod,                                    
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_SPDT,
                                   use_resource_mem = self.g.M_SPDT,   
                                   return_resource_num = self.g.cpu_SPDT,
                                   return_resource_mem = self.g.M_SPDT,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ddtr',
                                   timecol_inoo  = 'time_spdt',
                                   with_intermediate_data = 'RM',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'D_DDTR',
                                   req_last_used_by ='DDTR_DMtimedone',
                                   req_change_used_by = 'SPDT_req',
                                   mod_ID1_inoointer = 'D_SPDT',
                                   mod_size_interdata_perchunk = D_SPDTaggB,
#                                   mod_size_interdata_perchunk = self.g.D_SPDT,
                                   mod_name_extra = '_Scl',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   overlap_chunks = overlapchunks
                                   )

                    spdt_proc = self.env.process(spdt.run(ddtr_proc, True))

                if (self.df_POM_run_flags.run_spdt.loc[0] == 0):
                      logger.info('- NO SPDT done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))

##############################                      
#-------------- SPSIFT module
 
                if (self.df_POM_run_flags.run_spsift.loc[0] == 1):
                     
                    # is processed per candidate list from SPDT, i.e., per aggregation buffer
                    spsift = Proc_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'SPSIFT',
                                   runtime = self.g.t_SPSIFT, # only single thread expected
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_SPSIFT,
                                   use_resource_mem = self.g.M_SPSIFT,
                                   return_resource_num = self.g.cpu_SPSIFT,
                                   # not only processing memory returned, also the cache from the removed candidates
                                   # --- quick fix, correct, but not listed for idata 
                                   return_resource_mem = self.g.M_SPSIFT + (D_SPDTaggB - D_SPSIFTaggB),
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_spdt',
                                   timecol_inoo  = 'time_spss',
                                   with_intermediate_data = 'RM',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'D_SPDT',
                                   req_last_used_by ='SPDT_Scldone',
                                   req_change_used_by = 'SPSIFT_req',
                                   mod_ID1_inoointer = 'D_SPDT',
                                   mod_size_interdata_perchunk = (- D_SPDTaggB + D_SPSIFTaggB), # is negative, but D_SDPD should be positive afterwards
                                   mod_name_extra = '_SclSIFT',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   overlap_chunks = overlapchunks
                                   )

                    spsift_proc = self.env.process(spsift.run(spdt_proc, True))

                if (self.df_POM_run_flags.run_spsift.loc[0] == 0):
                      logger.info('- NO SPSIFT done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))


##############################                      
#-------------- SPScluster module

                if (self.df_POM_run_flags.run_spscluster.loc[0] == 1):
                    # is processed per candidate list from sift, i.e., per aggregation buffer
                    spcluster = Proc_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'SPCLUST',
                                   runtime = t_SPSCLUSTaggB_allthreads, #currently only single thread
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_SPCLUST,
                                   use_resource_mem = M_SPSCLUSTaggB,
                                   return_resource_num = self.g.cpu_SPCLUST,
                                   # not only processing memory returned, also the cache from the removed candidates
                                   # --- quick fix, correct, but not listed for idata 
                                   return_resource_mem = M_SPSCLUSTaggB + (D_SPSIFTaggB - D_SPSCLUSTaggB) ,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_spss',
                                   timecol_inoo  = 'time_spsc',
                                   with_intermediate_data = 'RM',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'D_SPDT',
                                   req_last_used_by ='SPSIFT_done',
                                   req_change_used_by = 'SPCLUST_req',
                                   mod_ID1_inoointer = 'D_SPDT',
                                   mod_size_interdata_perchunk = (- D_SPSIFTaggB + D_SPSCLUSTaggB ),
                                   mod_name_extra = '_SclClust',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   overlap_chunks = overlapchunks
                                   )


                    spcluster_proc = self.env.process(spcluster.run(spsift_proc, True))

                if (self.df_POM_run_flags.run_spscluster.loc[0] == 0):
                      logger.info('- NO SPS Clustering done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))

##############################                      
#-------------- CDOS-SPS module

                if (self.df_POM_run_flags.run_cdos.loc[0] == 1):

                    cdos = Proc_Module_Useevent_Idata_wOverlap(env =self.env,
                                   name = 'CDOSsps',
                                   # assume bad case - 90 percent of time in aggregation buffer  
                                   runtime = (chunks_in_ddbcN * self.g.chunk_buffer_length * 0.9) , 
#                                   runtime = self.g.t_CDOS_sps, # only single thread expected
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_CDOS_sps,
                                   use_resource_mem = self.g.M_CDOS_sps,
                                   return_resource_num = self.g.cpu_CDOS_sps,
                                   return_resource_mem = self.g.M_CDOS_sps,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_spsc',
                                   timecol_inoo  = 'time_cdosS',
                                   with_intermediate_data = 'RM',
#                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   num_chunks_for_intermediate_data = chunks_in_ddbcN,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'D_SPDT',
                                   req_last_used_by ='SPCLUST_done',
                                   req_change_used_by = 'CDOSs_req',
                                   mod_ID1_inoointer = 'D_CDOS',
                                   mod_size_interdata_perchunk = self.g.D_CDOS_sps,
                                   mod_name_extra = '_MetaD',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   overlap_chunks = overlapchunks
                                   )

                    cdos_proc = self.env.process(cdos.run(spcluster_proc, True))

#------------- return resources from intermediate data, raw data [if in first part of aggregation buffer]

                    # return resources if last chunk in aggregation buffer
                    if last_of_chunks_inAggBuffer:
                        
                        # while aggregation buffer COPIES are done for a SPS *ONLY* run,
                        # we need to 'delete' manually the raw data 
                        # because the DDBC buffer release considers only one set/copy of the data 
                        
                        if (self.chunk_counter != self.g.N_chunks) :
                            can_del_part_of_aggb = (chunks_in_ddbcN - overlapchunks) * data_per_chunk_in_rcpt
                            
                        # if in last DDBC buffer, all the buffer can be deleted
                        if (self.chunk_counter == self.g.N_chunks) :
                            mem_in_last_ddbc  = data_per_chunk_in_rcpt * (self.g.N_chunks - 
                                                    (chunks_in_ddbcN + 
                                                     (n_c-1)*(chunks_in_ddbcN - overlapchunks)) )
                            can_del_part_of_aggb = mem_in_last_ddbc
                        
                        rawd_part_release = release_resource(env =self.env,
                                                g = self.g,
                                                name = 'raw_release_aggBpart',
                                                use_resource_name = 'CPU',
                                                return_resource_num = 0,
                                                return_resource_mem = can_del_part_of_aggb,
                                                #                 beamid:int,
                                                obsid             = chunk.obs_id,
                                                chunkid           =  self.chunk_counter,
                                                oo                 = self.oo,
                                                timecol_inoo_check1 = 'time_cdosS')

                            
                            
                            

                    #-------- final resource release after the processing is finished
                    # release RAM memory of  buffer False: not only inter_data, but also resource 
                        B_DDBC_release = self.env.process(ddbc.release_buffer_resource(cdos_proc, 'DDTR_done',False))
                    # release RAM memory of data products (RFIM msk, CDOS outut lists,...) 
                    #  '' entry must be listed in check_done_withorder method
                        D_DDTR_release = self.env.process(ddtr.release_intermediate_data_products(cdos_proc, 'SPDT_done'))
                        D_SPDT_release = self.env.process(spdt.release_intermediate_data_products(cdos_proc, 'CDOSsps_done'))
                        D_CDOS_sps_Metad_release = self.env.process(cdos.release_intermediate_data_products(B_DDBC_release, 'CDOSsps_MetaDdone'))
                    # !!! Revise when no longer copy of data or if PSS+SPS !!!
                    # release the RCPT data ( first part of aggregation buffer, this is outside overlap) 
                        DOrawd_part_release = self.env.process(rawd_part_release.release(cdos_proc))


        sps_pipr1_end=self.env.now
        
        
        
###############################################################################


class pipe4(object):
    """
     
    The pipeline class where the receptor, buffer, processing, and output processes are called

         
    """
    def __init__(self,env,g,df_POM_parT,df_cheetahflags,df_cheetah,name,oo_init):
        """
        gets the setup of the simulation

        Parameters
        ----------
        env : a simpy simulation environment 
              The same simpy env is used for the whole simulation (of one beam at the moment)
        g : the class with general parameters of the simulation 
            It was compiled before and is passed on.
        df_POM_run_flags : a pandas DataFrame with Flags of the POM
            It is used to run the pipeline with the config parameters (e.g., Cheetah modules on or off)
        name : Name of the pipeline 
            When multiple pipelines (beams) are run, this will help to keep track

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        self.env = env
        self.name= name
        self.simtime = g.sim_duration
        self.N_chunks = g.N_chunks
          
        self.g =g

        #------------------------------------------------------------------------------
        # define flags which modules currently exist in POM; 
        #        1: exists 0: does not exist
        # TDAS ignored since not planned to do
        #------------------------------------------------------------------------------
        pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                              'ddbc':       [1],
                              'rfim':       [1],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [1], # for unmodified obs
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [1]
                              })
        logger.info(pom_exist_flags)

        logger.info('------------- Check existing POM modules versus the wanted ones------------- ')
        import packages.POM_readin as pr
        self.df_POM_run_flags=pr.wanted_vs_exist(pom_exist_flags, df_POM_parT,df_cheetahflags,df_cheetah)
        
        if (self.df_POM_run_flags.run_rcpt.loc[0] == 0):
            warnings.warn('--- Running the POM without incoming data does not make sense \n'+
                          '--- The code will set RCPT to run')
            self.df_POM_run_flags.run_rcpt =1
    
        logger.info(self.df_POM_run_flags)


#        self.df_POM_run_flags = df_POM_run_flags
        self.obs_counter = 0
        self.chunk_counter = 0
        
#        self.oo = out_monitor(self.env)
        self.oo = oo_init
       

    def run(self):
        """
        runs the pipeline until end time of simulation is reached

        Returns
        -------
        out_data : a pandas DataFrame to see which processes have been completed for each data chunk
        out_resources : a pandas DataFrame with resource monitoring over the time of the simulation

        """
        # later to be extended to 3 beams
        #b_id=1
#        self.env.process(self.do(b_id))

        self.env.process(self.do())
        self.env.run(until=self.simtime)
        logger.info(str(self.name)+' time end: '+ str(self.env.now)) 
        return(self.oo.out_data,self.oo.out_resources, self.oo.inter_data,self.df_POM_run_flags)
      
 
    def do(self):
        """
        The pipeline with individual module calls.

        Returns
        -------
        None.

        """
#    def do(self,b_id):
#        print(self.oo.out_data)
#        print(self.oo.out_resources)
        
        while True:
            pip4_start=self.env.now
            # --- Create/get new observation
            # --- Increment the obs counter
            
            
            self.obs_counter += 1
            time_obsID_started = self.env.now
#            logger.info('- obs counter: '+str(self.obs_counter)+
#                        ', started at time '+str(time_obsID_started))
            
            obs = Observation(self.obs_counter)
            # need to set 0 for each new observation
            self.chunk_counter = 0
            
            
            # ------ keeps generating data_chunks for one observation,  
            #        counting, until number-of-chunks is reached
            while self.chunk_counter < (self.N_chunks):
                

                  self.chunk_counter += 1
                  errflag=0
                  
                  chunk = Chunk(obs.obs_id,self.chunk_counter)
#                  print(chunk.obs_id,chunk.chunk_id)
                  self.oo.init_od_newrow(obs.obs_id,self.chunk_counter)
                  #print(self.oo.out_data)
                                       
                  time_read_start=self.env.now 

                  readin_iters=(self.g.t_RCPT_i + self.g.chunk_buffer_length)

                  rusemem=self.g.D_RCPT+self.g.M_RCPT
                  rreturnmem=self.g.M_RCPT



                  rec1=receptor_mod(self.env,
                                       '* RCPT',
                                       readin_iters,
                                       'CPU',
                                       self.g.cpu_RCPT,
                                       rusemem,
                                       self.g.cpu_RCPT,
                                       rreturnmem,
                                  #     b_id,
                                       chunk.obs_id,
                                       chunk.chunk_id,
                                       self.oo,
                                       'time_rcpt')

                  # run data receptor 
                  rec1_proc =self.env.process(rec1.run())
                  yield rec1_proc    

##############################                      


                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 1):


                      ddbc = Buffer_Module_Useevent_Idata(env =self.env,
                                   name = 'DDBC',
                                   runtime = self.g.t_DDBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_DDBC,
                                   use_resource_mem = self.g.M_DDBC,
                                   return_resource_num = self.g.cpu_DDBC,
                                   return_resource_mem = self.g.M_DDBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rcpt',
                                   timecol_inoo  = 'time_ddbc',
                                   with_intermediate_data = 'M',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_ddbc,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_DDBC',
                                   mod_size_interdata_perchunk = self.g.D_DDBC,
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   D_already_reserved= self.g.D_RCPT,
                                   sub_chunks = 1)

                      ddbcproc = self.env.process(ddbc.reserve_buffer(rec1_proc,False))
                     

                      
                  if (self.df_POM_run_flags.run_ddbc.loc[0] == 0):
                      logger.info('- NO DDBC done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))
##############################                      

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 1):


                      rfim = Proc_Module_Useevent_Idata(env =self.env,
                                   name = 'RFIM',
                                   runtime = self.g.t_RFIM,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_RFIM,
                                   use_resource_mem = self.g.M_RFIM,
                                   return_resource_num = self.g.cpu_RFIM,
                                   return_resource_mem = self.g.M_RFIM,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ddbc',
                                   timecol_inoo  = 'time_rfim',
                                   with_intermediate_data = 'RM',
                                   num_chunks_for_intermediate_data = self.g.chunks_in_rfim,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_DDBC',
                                   req_last_used_by ='DDBC_done',
                                   req_change_used_by = 'RFIM_req',
                                   mod_ID1_inoointer = 'D_RFIM',
                                   mod_size_interdata_perchunk = self.g.D_RFIM,
                                   mod_name_extra = '_msk',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False)



                      rfim_proc = self.env.process(rfim.run(ddbcproc, True))

                  if (self.df_POM_run_flags.run_rfim.loc[0] == 0):
                      logger.info('- NO RFIM done for obsID/chunk'+ str(chunk.obs_id)+
                                  '/'+str(self.chunk_counter))




########################################
#12345678912345678
                  if (self.df_POM_run_flags.run_ffbc.loc[0] == 1):
                      
                      ffbc = Buffer_Module_Useevent_Idata(env =self.env,
                                   name = 'FFBC',
                                   runtime = self.g.t_FFBC_i,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_FFBC,
                                   use_resource_mem = self.g.M_FFBC,
                                   return_resource_num = self.g.cpu_FFBC,
                                   return_resource_mem = self.g.M_FFBC,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_rfim',
                                   timecol_inoo  = 'time_ffbc',
                                   with_intermediate_data = 'M',
                                   num_chunks_for_intermediate_data = self.g.N_chunks,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'None',
                                   req_last_used_by ='None',
                                   req_change_used_by ='None',
                                   mod_ID1_inoointer = 'B_FFBC',
                                   mod_size_interdata_perchunk = self.g.D_FFBC,
                                   mod_name_extra = '_',
                                   del_ID1_inoointer = 'None1',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False,
                                   D_already_reserved= self.g.D_RCPT, # reminder:  per data chunk
                                   sub_chunks = self.g.chunks_in_ddbc
                                   )

                      ffbc_proc = self.env.process(ffbc.reserve_buffer(rfim_proc,False))

##############################                       
#  DDBC no longer needed in THIS pipeline (but data is in the FFBC )
#                     the first argument is the event after which this is run 
#                     the second is the earliest entry in "last_used_by" for the last module that modified the row of B_DDBC (FFBC does not!)
#                     True - only inter_data is changed 
                      ddbcrelease = self.env.process(ddbc.release_buffer_resource(ffbc_proc, 'RFIM_req',True))   
##############################                      

                      
                  if (self.df_POM_run_flags.run_ffbc.loc[0] == 0):
                       logger.info('- NO FFBC done for obsID/chunk'+ str(chunk.obs_id)+
                                   '/'+str(self.chunk_counter))

            
            # # --- CDOS crucial for releasing resources, keep user informed if unreasonable flags
                  if ((self.df_POM_run_flags.run_cdos.loc[0] == 1) 
                      and (self.df_POM_run_flags.run_ffbc.loc[0] == 0)):
                       warnings.warn('- CDOS cannot work without FFBC \n'+
                                         '- run_CDOS=0, no CDOS for obsID ' 
                                         + str(self.obs_counter) + '\n'+
                                       '!!! Buffer used by Obs *not* out of scope, thus RAM stays occupied')
                
                  if (self.df_POM_run_flags.run_cdos.loc[0] == 0):
                           warnings.warn('- run_CDOS=0, no CDOS for obsID ' 
                                         +str(self.obs_counter) +'\n'+
                                       '!!! Buffer used by Obs *not* out of scope, thus RAM stays occupied')
            

                  if ((self.df_POM_run_flags.run_cdos.loc[0] == 1) 
                        and (self.df_POM_run_flags.run_ffbc.loc[0] == 1)):
                
#123456789123456789123                      
                      cdosA = Proc_Module_Useevent_Idata(env =self.env,
                                   name = 'CDOSa',
                                   runtime = self.g.t_CDOS_all,
                                   use_resource_name = 'CPU',
                                   use_resource_num = self.g.cpu_CDOS,
                                   use_resource_mem = self.g.M_CDOS,
                                   return_resource_num = self.g.cpu_CDOS,
                                   return_resource_mem = self.g.M_CDOS,
                #                 beamid:int,
                                   obsid             = chunk.obs_id,
                                   chunkid           = self.chunk_counter,
                                   oo                 = self.oo,
                                   timecol_inoo_check1 = 'time_ffbc',
                                   timecol_inoo  = 'time_cdosA',
                                   with_intermediate_data = 'RM',
                                   num_chunks_for_intermediate_data = self.g.N_chunks,
                                   total_num_chunks = self.g.N_chunks,
                                   req_ID1_inoointer = 'B_FFBC',
                                   req_last_used_by ='FFBC_done',
                                   req_change_used_by = 'CDOS_req',
                                   mod_ID1_inoointer = 'D_CDOSa',
                                   mod_size_interdata_perchunk = self.g.D_CDOSa,
                                   mod_name_extra = '_lists',
                                   del_ID1_inoointer = 'None',
                                   del_size_interdata_total = -1,
                                   del_resourceflag = False)


                      cdosA_proc = self.env.process(cdosA.run(ffbc_proc, True))
                      
#123456789123456789123

                   #-------- final resource release after the processing of one observation is finished
                      # release RAM memory of FFBC buffer False: not only inter_data, but also resource 
                      B_FFBC_release = self.env.process(ffbc.release_buffer_resource(cdosA_proc, 'CDOS_done',False))
                      # release RAM memory of data products (RFIM msk, CDOS outut lists,...) 
                      #  '' entry must be listed in check_done_withorder method
                      D_RFIM_msk_release = self.env.process(rfim.release_intermediate_data_products(cdosA_proc, 'RFIM_mskdone',True))
                      D_CDOSa_lists_release = self.env.process(cdosA.release_intermediate_data_products(cdosA_proc, 'CDOSa_listsdone',True))

        pip4_end=self.env.now













#------------------------------------------------------------------------------
#  
#------------------------------------------------------------------------------











