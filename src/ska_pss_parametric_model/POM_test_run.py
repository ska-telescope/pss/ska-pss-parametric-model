#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
test program for the PSS Parametric Operational Model

"""
#    **************************************************************************
#    |       test program to run the PSS Parametric Operational Model
#    **************************************************************************
#    | Author: B. Posselt
#    **************************************************************************
#    | Description:
#    | (1) reads test config files and runs chosen POM test pipeline
#    | (2) compares output files (.csv) with reference test output files                                                        
#    | 
#    | Usage on shell: [put in one line]
#    |  python POM_test_run.py test1 path-to-installation (above /src)
#    |      
#    |  1st argument: which test
#    |  2nd argument: path  
#    |
#    | There are currently 4 tests:
#           1:  RCPT, DDBC, RFIM and DDBC buffer release after RFIM
#           2:  the same as 1 but with top-like visualisation in the terminal
#           3:  RCPT, DDBC, RFIM , FFBC and CDOSa 
#               and buffer + intermediate data (masks, lists) resource releases
#           4:  the same as 3 but with more realistic many-chunk long observation
#
#     Config files of the tests are in  tests/testconfigs/
#     The test output files (used for comparison) are in tests/testoutputs  
#
#    If the syntax of the used classes/methods changes in further development, 
#    the tests in POM_testpipes.py need to be updated.
#    **************************************************************************

import packages.POM_readin as pr
import packages.POM_modules as pm
import packages.POM_visualisation as vis

import packages.POM_testpipes as pt

import logging
from packages.logging_config import configure_loggingFile, get_default_formatter
import warnings

import simpy
import pandas as pd
import numpy as np

import argparse

from time import sleep

from rich.console import Console


pt.do_at("started at ")


#print(pd.__version__)

#------------------------------------------------------------------------------
# read arguments
#------------------------------------------------------------------------------
parser=argparse.ArgumentParser()
parser.add_argument("test_number",type=int,help="run test number")
parser.add_argument("path_to_maindir",type=str,help="main directory (above src)")
args = parser.parse_args()

path_to_add = args.path_to_maindir
#------------------------------------------------------------------------------
# set up logging behaviour  = as in main pipeline
#------------------------------------------------------------------------------
nowindecimal=pm.get_decimal_year()
#logfilename='../output/pomlog_'+str(nowindecimal)+'.log' - to give unique name
# where the logfile is written to  [will be overwritten in next run]
logfilename=path_to_add+'tests/testoutputs/pomlog_mostrecentTests.txt'

configure_loggingFile(logging.DEBUG,filen=logfilename)
logger = logging.getLogger("pomlog")
logger.info("====================  Logging started for POM TEST ====================  ")
logging.captureWarnings(True) 

warnings.simplefilter("always")  # enable always warning
warnings.warn('test warning')

#------------------------------------------------------------------------------
# where to write the output/monitoring data frames to [will be overwritten in next run]
#------------------------------------------------------------------------------
outdata_filename=path_to_add+'tests/testoutputs/out_data_mostrecent.csv'
outres_filename=path_to_add+'tests/testoutputs/out_resources_mostrecent.csv'
interdata_filename=path_to_add+'tests/testoutputs/inter_data_mostrecent.csv'
# for unique names
#outdata_filename='../../tests/testoutputs/out_data_'+str(nowindecimal)+'.csv'
#outres_filename='../../tests/testoutputs/out_resources_'+str(nowindecimal)+'.csv'
#interdata_filename='../../tests/testoutputs/inter_data_'+str(nowindecimal)+'.csv'


#==============================================================================
# Test 1
#==============================================================================
if args.test_number == 1:
    logger.info("====================  POM TEST 1 ========================  ")
    
    cheetah_config_file=path_to_add+'tests/testconfigs/protest_mid_single_beam.xml'
    pom_config_file=path_to_add+'tests/testconfigs/POM_config_test1.csv'
    
    comp_outd_file=path_to_add+'tests/testoutputs/out_data_Test1.csv'
    comp_outres_file=path_to_add+'tests/testoutputs/out_resources_Test1.csv'
    comp_interdata_file=path_to_add+'tests/testoutputs/inter_data_Test1.csv'
    
    print(' === Test 1: RCPT, DDBC, RFIM and DDBC buffer release after RFIM')
    print('     Note: the intermediate data produced by RFIM (msk) are NOT released')

#------------------------------------------------------------------------------
# define flags which modules currently exist in POM; 
#        1: exists 0: does not exist
#------------------------------------------------------------------------------
    pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                              'ddbc':       [1],
                              'rfim':       [1],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [1], # for unmodified obs
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [1]
                              })


#==============================================================================
# Test 2 - the pipeline is the same as Test 1 but with visualisation
# screenshot of the terminal at the end of the simulation in test directory 
#==============================================================================
if args.test_number == 2:
    logger.info("====================  POM TEST 2 ========================  ")
    
    cheetah_config_file=path_to_add+'tests/testconfigs/protest_mid_single_beam.xml'
    pom_config_file=path_to_add+'tests/testconfigs/POM_config_test1.csv'
    
    comp_outd_file=path_to_add+'tests/testoutputs/out_data_Test1.csv'
    comp_outres_file=path_to_add+'tests/testoutputs/out_resources_Test1.csv'
    comp_interdata_file=path_to_add+'tests/testoutputs/inter_data_Test1.csv'
    
    print(' === Test 2: RCPT, DDBC, RFIM and DDBC buffer release after RFIM')
    print(' ===  (=Test1) + visualisation')
    print('!!! Open to compare with output at the end of simulation: ')
    print('        '+ path_to_add+'tests/Test2_terminal_at_end.png')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    sleep(5)





#------------------------------------------------------------------------------
# define flags which modules currently exist in POM; 
#        1: exists 0: does not exist
#------------------------------------------------------------------------------
    pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                              'ddbc':       [1],
                              'rfim':       [1],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [1], # for unmodified obs
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [1]
                              })

#==============================================================================
#==============================================================================
# Test 3 - RCPT,DDBC,RFIM, FFBC and CODa in the pipeline, 
#          without freeing resources for final data products 
#==============================================================================
if args.test_number == 3:
    logger.info("====================  POM TEST 3 ========================  ")
    
    cheetah_config_file=path_to_add+'tests/testconfigs/protest_mid_single_beam.xml'
    pom_config_file=path_to_add+'tests/testconfigs/POM_config_test3.csv'
    
    comp_outd_file=path_to_add+'tests/testoutputs/out_data_Test3.csv'
    comp_outres_file=path_to_add+'tests/testoutputs/out_resources_Test3.csv'
    comp_interdata_file=path_to_add+'tests/testoutputs/inter_data_Test3.csv'
    
    print(' === Test 3: RCPT, DDBC, RFIM , FFBC and CDOSa')
    print(' === (note: DDBC and FFBC use same data, B_ size in inter_data for tracking, not to be counted)')

#------------------------------------------------------------------------------
# define flags which modules currently exist in POM; 
#        1: exists 0: does not exist
#------------------------------------------------------------------------------
    pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                              'ddbc':       [1],
                              'rfim':       [1],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [1], # for unmodified obs
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [1]
                              })

#==============================================================================

if (args.test_number not in [1,2,3]):
    cheetah_config_file='None'
    pom_config_file='None'
    
    comp_outd_file='None'
    comp_outres_file='None'
    comp_interdata_file='None'

    
    pom_exist_flags=pd.DataFrame({'rcpt':   [0],
                              'ddbc':       [0],
                              'rfim':       [0],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [0], 
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [0]
                              })

#==============================================================================
#==============================================================================
# Test 3 - RCPT,DDBC,RFIM, FFBC and CODa in the pipeline, 
#          without freeing resources for final data products 
#==============================================================================
if args.test_number == 4:
    logger.info("====================  POM TEST 4 ========================  ")
    
    cheetah_config_file=path_to_add+'tests/testconfigs/protest_mid_single_beam.xml'
    pom_config_file=path_to_add+'tests/testconfigs/POM_config_test4.csv'
    
    comp_outd_file=path_to_add+'tests/testoutputs/out_data_Test4.csv'
    comp_outres_file=path_to_add+'tests/testoutputs/out_resources_Test4.csv'
    comp_interdata_file=path_to_add+'tests/testoutputs/inter_data_Test4.csv'
    
    print(' === Test 4: RCPT, DDBC, RFIM , FFBC and CDOSa')
    print(' ===   similar to Test 3 but for long observation with many (1000) data chunks')

#------------------------------------------------------------------------------
# define flags which modules currently exist in POM; 
#        1: exists 0: does not exist
#------------------------------------------------------------------------------
    pom_exist_flags=pd.DataFrame({'rcpt':       [1],
                              'ddbc':       [1],
                              'rfim':       [1],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [1], # for unmodified obs
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [1]
                              })

#==============================================================================

if (args.test_number not in [1,2,3,4]):
    cheetah_config_file='None'
    pom_config_file='None'
    
    comp_outd_file='None'
    comp_outres_file='None'
    comp_interdata_file='None'

    
    pom_exist_flags=pd.DataFrame({'rcpt':   [0],
                              'ddbc':       [0],
                              'rfim':       [0],
                              'ddtr':       [0],
                              'spdt':       [0],
                              'spsift':     [0],
                              'ffbc':       [0], 
                              'psbc':       [0],
                              'cxft':       [0],
                              'brdz':       [0],
                              'dred':       [0],
                              'fdas':       [0],
                              'sift':       [0],
                              'fldo':       [0],
                              'cdos':       [0]
                              })



logger.info(str(cheetah_config_file))
logger.info(str(pom_config_file))
logger.info(pom_exist_flags)

# --- as in main routine
#------------------------------------------------------------------------------
# Read in configs, cross-check against existing capabilities
# log info
#------------------------------------------------------------------------------
logger.info('------------- Reading of cheetah config file ------------- ')
dict_cheetah,df_cheetah,df_cheetahflags=pr.read_cheetah_config(cheetah_config_file)
#print(' ')
logger.info('=== read (selected) Cheetah config - column header')
logger.info(df_cheetah.columns.values)
#print(' ')
logger.info('=== read (selected) Cheetah config parameter - full data frame')
logger.info(df_cheetah)
# config files might defer because entries were removed:
#print(' ')
logger.info('=== NO-flags data frame column header')
logger.info('=== indicating if (selected) Cheetah pars exist in config file')
logger.info(df_cheetahflags.columns.values)

#print(' ')
logger.info('------------- Reading of parametric model config file ------------- ')
df_POM_par,df_POM_parT=pr.read_POM_config(pom_config_file)
logger.info('=== transposed POM config column values:')
logger.info(df_POM_parT.columns.values)

logger.info('------------- Check existing POM modules versus the wanted ones------------- ')
df_POM_run_flags=pr.wanted_vs_exist(pom_exist_flags, df_POM_parT,df_cheetahflags,df_cheetah)

if (df_POM_run_flags.run_rcpt.loc[0] == 0):
    warnings.warn('--- Running the POM without incoming data does not make sense \n'+
                  '--- The code will set RCPT to run')
    df_POM_run_flags.run_rcpt =1
    
logger.info(df_POM_run_flags)


#------------------------------------------------------------------------------
# use configs to populate POM parameters, available globally
#------------------------------------------------------------------------------
g=pm.glob(df_POM_parT)

#==============================================================================
# visuallisation for Test 2
#==============================================================================
if args.test_number == 2:
    console = Console()

#------------------------------------------------------------------------------
# setup simpy environment
pom1 = simpy.Environment()
# setup resources 
pr.setup_resources(pom1,g)
# initialize output data frames
oo1 = pm.out_monitor(pom1)

#==============================================================================
# visuallisation for Test 2
#==============================================================================
if args.test_number == 2:
## initialize visualisation class
    vis1 = vis.top_vis(pom1,console,g,oo1)
## start top-like visualisation, slowed down via sleeptime
    pom1.process(vis1.run_top_vis(sleeptime=0.1)) # stops the program for a while


for run in range(g.number_of_runs):
    logger.info("Run " + str(run+1) + " of "+ str(g.number_of_runs))

    
#==============================================================================
# Test 1 or 2
#==============================================================================
    if (args.test_number == 1) or (args.test_number == 2):
  
        tpipe_1=pt.testpipe1(pom1,g,df_POM_run_flags,'test-pipeline 1',oo1)   
        out_data,out_resources,inter_data =(tpipe_1.run()) 
   
#==============================================================================
# Test 3 or 4
#==============================================================================
    if (args.test_number == 3)  or (args.test_number == 4):
  
        tpipe_3=pt.testpipe3(pom1,g,df_POM_run_flags,'test-pipeline 3',oo1)   
        out_data,out_resources,inter_data =(tpipe_3.run()) 


    
    
    if (args.test_number not in [1,2,3,4]) :

        print('NOTHING iS DONE')
    
    
    
    
    if (args.test_number > 0) and (args.test_number < 5):
   # save data frames
        out_data.to_csv(outdata_filename,index=False)
        out_resources.to_csv(outres_filename,index=False)
        inter_data.to_csv(interdata_filename,index=False)
        
       
        
        outdata_comp, outres_comp, inter_comp = pt.compare_dataframes(comp_outd_file,
                                                          comp_outres_file,
                                                          comp_interdata_file, 
                                                          outdata_filename,
                                                          outres_filename,
                                                          interdata_filename)
    
    
    
    


    logger.info('***** end time for run '+str(run)+' : '+ str(pom1.now))

    logger.info(out_resources)
    logger.info(out_data)
    
logger.info('end')

pt.do_at("ended at   ")

if (args.test_number == 3) :
    print('')
    print('For Test 3 we can double check if the resources at the end are as expected: ')
    print(' *out_data* : 3 x ffbc processes (each 4000) ')
    print(' *out_data* : 1 x rfim process (each 100 for processing and 2222 for reserved msk)  ')
    print(' *out_data* : 1 x rcp process with 100  ')
    print(' *out_data* : 19 data chunks with 10 000 are processed, they are each 1 times in RAM (either in a buffer or raw [rcpt reserved])')
    # Buffer B_ in inter_data list active buffers, several can point to the same data
    # for this reason it is better to simply count the number of not-finished data chunks
    # DDBC data is also in the ffbc
    # FFBC is showing 60000 for one observation (6 chunks) currently processed by CDOS 
    print(' *inter_data* : 5 x RFIM_msk with 2222')
    print( ' => sum of all  = 223,909')
    print('  the last line of *out_resources* has under index=616:')
    print('  => cpu_ram_used: 223,909')


if args.test_number == 2:
    sleep(10)



