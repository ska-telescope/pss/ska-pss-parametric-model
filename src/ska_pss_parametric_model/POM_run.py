#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
main program to run the PSS Parametric Operational Model

"""
#    **************************************************************************
#    |        main program to run the PSS Parametric Operational Model
#    **************************************************************************
#    | Author: B. Posselt
#    **************************************************************************
#    | Description:
#    |  (1) read config files and choose modules(and parameters) to run POM
#    |                                                        
#    | 
#    | Usage on shell: [put in one line]
#    |  python POM_run.py 
#    |  .../parametric_modelPSS/config_examples/protest_mid_single_beam.xml 
#    |  .../parametric_modelPSS/config_examples/POM_config.csv
#    |      
#    |  1st argument: Cheetah config file - .xml
#    |  2nd argument: POM config file - .csv    
#    **************************************************************************

import packages.POM_readin as pr
import packages.POM_modules as pm
import packages.POM_visualisation as vis

import logging
from packages.logging_config import configure_loggingFile, get_default_formatter
import warnings

import simpy
import pandas as pd
import numpy as np

import argparse

from rich.console import Console


#print(pd.__version__)

#------------------------------------------------------------------------------
# set up logging behaviour
#------------------------------------------------------------------------------
nowindecimal=pm.get_decimal_year()
#logfilename='../output/pomlog_'+str(nowindecimal)+'.log' - to give unique name
# where the logfile is written to  [will be overwritten in next run]
logfilename='../../output/pomlog_mostrecent.txt'

configure_loggingFile(logging.DEBUG,filen=logfilename)
logger = logging.getLogger("pomlog")
logger.info("====================  Logging started for POM ====================  ")
logging.captureWarnings(True) 

# ---  https://docs.python.org/3/howto/logging.html:
# warnings.warn() in library code if the issue is avoidable 
# and the client application should be modified to eliminate the warning
# * by default, same text in message: only one time warning 
#
# logging.warning() if there is nothing the client application can do 
# about the situation, but the event should still be noted
warnings.simplefilter("always")  # enable always warning

#logger.debug('debug message')
#logger.info('info message')/P
warnings.warn('test warning')
#logger.warning('warn message')
#logger.error('error message')
#logger.critical('critical message')

#------------------------------------------------------------------------------
# where to write the output/monitoring data frames to [will be overwritten in next run]
#------------------------------------------------------------------------------
outdata_filename='../../output/out_data_mostrecent.csv'
outres_filename='../../output/out_resources_mostrecent.csv'
interdata_filename='../../output/inter_data_mostrecent.csv'
# for unique names
#outdata_filename='../../output/out_data_'+str(nowindecimal)+'.csv'
#outres_filename='../../output/out_resources_'+str(nowindecimal)+'.csv'
#interdata_filename='../../output/inter_data_'+str(nowindecimal)+'.csv'
# this is needed to refine later the plot
runflags_filename = '../../output/run_modules_mostrecent.csv'

#------------------------------------------------------------------------------
# read arguments
#------------------------------------------------------------------------------
parser=argparse.ArgumentParser()
parser.add_argument("cheetah_config_file",type=str,help="path/CheetahConfigFile")
parser.add_argument("pom_config_file",type=str,help="path/POMConfigFile")

args = parser.parse_args()
cheetah_config_file=args.cheetah_config_file
pom_config_file=args.pom_config_file
logger.info(str(cheetah_config_file))
logger.info(str(pom_config_file))

#------------------------------------------------------------------------------
# Read in configs, 
# cross-check against existing capabilities is done in respective pipelines
# log info
#------------------------------------------------------------------------------
logger.info('------------- Reading of cheetah config file ------------- ')
dict_cheetah,df_cheetah,df_cheetahflags=pr.read_cheetah_config(cheetah_config_file)

logger.info('=== read (selected) Cheetah config - column header')
logger.info(df_cheetah.columns.values)

logger.info('=== read (selected) Cheetah config parameter - full data frame')
logger.info(df_cheetah)
# config files might differ because entries were removed:

logger.info('=== NO-flags data frame column header')
logger.info('=== indicating if (selected) Cheetah pars exist in config file')
logger.info(df_cheetahflags.columns.values)


#------------------------------------------------------------------------------
logger.info('------------- Reading of parametric model config file ------------- ')
df_POM_par,df_POM_parT=pr.read_POM_config(pom_config_file)
logger.info('=== transposed POM config column values:')
logger.info(df_POM_parT.columns.values)



#------------------------------------------------------------------------------
# use configs to populate POM parameters, initial version for resources
# cross-checked in respective pipelines 
# can be made available globally
#------------------------------------------------------------------------------
g=pm.glob(df_POM_parT)
# global g # only used if we want t0 do error tracking in spyder


#console = Console()
#------------------------------------------------------------------------------
#  run the POM for number of runs 
#------------------------------------------------------------------------------
# setup simpy environment
pom1 = simpy.Environment()
# setup resources 
pr.setup_resources(pom1,g)
# initialize output data frames
oo1 = pm.out_monitor(pom1)

## initialize visualisation class
#vis1 = vis.top_vis(pom1,console,g,oo1)
## start top-like visualisation, slowed down via sleeptime
#pom1.process(vis1.run_top_vis(sleeptime=0.0)) # stops the program for a while


for run in range(g.number_of_runs):
    logger.info("Run " + str(run+1) + " of "+ str(g.number_of_runs))

    
#    pipe_4=pm.pipe4(pom1,g,df_POM_parT,df_cheetahflags,df_cheetah,'test-pipeline ID 4',oo1)
#    out_data,out_resources,inter_data,df_POM_run_flags =(pipe_4.run())

    sps1=pm.sps_pipe_realistic1(pom1,g,df_POM_parT,df_cheetahflags,df_cheetah,'sps test1`',oo1)    
    out_data,out_resources,inter_data,df_POM_run_flags =(sps1.run())
 
    logger.info('***** end time for run '+str(run)+' : '+ str(pom1.now))

    # save data frames
    out_data.to_csv(outdata_filename,index=False)
    out_resources.to_csv(outres_filename,index=False)
    inter_data.to_csv(interdata_filename,index=False)
    df_POM_run_flags['simtime']=g.sim_duration
    df_POM_run_flags.to_csv(runflags_filename,index=False)
    
 #   logger.info('***** end time for run '+str(run)+' : '+ str(pom1.now))


    logger.info(out_resources)
    logger.info(out_data)
    
logger.info('end')
print('end')

vis.plot_output_frommain_more(simtime = df_POM_run_flags.simtime.iat[0],
                         df_POM_run_flags = df_POM_run_flags,
                         filedir = '../../output/',
                         fileres = outres_filename,
                         extraname = 'testP1')


