# PSS Parametric Occupancy Model (POM)

This repository includes the current python code of a parametric model describing the PSS pipeline and example configuration files to run it.


The POM is a discrete event simulation. In its initial version (0.5.0), it produces a pandas data frame of "event" times, where such  events mark different processing steps in the PSS modules (currently RCPT, FFBC, CDOS). 

Started with two input file names, the POM reads the two configuration files (one specific to the POM, one a Cheetah configuration file) and cross-checks which PSS modules are currently implemented and can actually be simulated. 



Install and run the code 

    (1) download source
    
        git init
        git remote add origin https://gitlab.com//ska-telescope/pss/ska-pss-parametric-model.git
        git pull origin >branch<
             ( e.g. git pull origin main )

    (2) activate poetry environment* and make sure all the the dependencies are installed

        poetry shell
        poetry install
    
    (3) create an "output" directory where the most recent log file and the csv files are kept 

    (4a) run one of the tests [Note: assume python = python3]:

        python src/ska_pss_parametric_model/POM_test_run.py 1 $(pwd)"/"
        (= python src/ska_pss_parametric_model/POM_test_run.py 1 path/to/directory/ )
    
    (4b) example run: 
        
        cd src/ska_pss_parametric_model
        python POM_run.py ../../config_examples/protest_mid_single_beam.xml ../../config_examples/POM_config.csv
        ls ../../output/

             should show:   inter_data_mostrecent.csv  
                            out_data_mostrecent.csv  
                            out_resources_mostrecent.csv  
                            pomlog_mostrecent.txt

        or 
        run within spyder: set up the use of the arguments via 
        Run Menu > Configuration per file > Command line option > add the full paths of both config files.  



* if poetry is not installed do first:
    curl -sSL https://install.python-poetry.org | python3 -    

